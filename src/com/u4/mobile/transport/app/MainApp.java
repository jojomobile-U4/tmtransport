package com.u4.mobile.transport.app;

import org.androidannotations.annotations.EApplication;

@EApplication
public class MainApp extends com.u4.mobile.base.app.MainApp {
	private static MainApp instance;

	public static MainApp getInstance() {
		return instance;
	}

	public MainApp() {
		instance = this;
	}
}
