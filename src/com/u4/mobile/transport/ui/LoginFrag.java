package com.u4.mobile.transport.ui;

import java.io.File;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.EditText;

import com.u4.mobile.base.app.AppSettings_;
import com.u4.mobile.base.helpers.NetworkHelper;
import com.u4.mobile.base.ui.AppSettingsActivity_;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.SessionInfo;
import com.u4.mobile.model.UpdateInfo;
import com.u4.mobile.scanner.Scanner.Scanable;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.app.MainApp;
import com.u4.mobile.transport.services.AppUpdateService_;
import com.u4.mobile.transport.services.RestHelper;
import com.u4.mobile.transport.ui.shippingorders.ShippingOrderStopsAct_;
import com.u4.mobile.transport.utils.SynchronizationHelper;
import com.u4.mobile.utils.FileHelper;
import com.u4.mobile.utils.LogHelper;

@OptionsMenu(resName = "activity_login_menu")
@EFragment(R.layout.activity_login)
public class LoginFrag extends FragmentBase implements Scanable {

	private UpdateInfo updateInfo;
	
	@ViewById(resName = "login__user_name_edit_view")
	EditText userNameEditText;

	@ViewById(resName = "login__password_edit_view")
	EditText passwordEditText;

	@Pref
	public AppSettings_ appSettings;

	@Bean
	public RestHelper restHelper;

	@Bean
	DatabaseHelper databaseHelper;

	private boolean hasChangesSettings = false;

	@OptionsItem(resName = "activity_login_menu__preferences__item")
	public void showPreferences() {
		startActivityForResult(new Intent(MainApp.getInstance().getApplicationContext(), AppSettingsActivity_.class), 0);
	}

	@AfterViews
	public void startUp() {
		if (appSettings.saveCredentials().get()) {
			userNameEditText.setText(appSettings.userName().get());
			passwordEditText.setText(appSettings.userPassword().get());
		}

		hasChangesSettings = appSettings.hasChangedSettings().get();
		setLoaderVisible(false);
	}

	public SessionInfo getOnlineSessionInfo() {
		SessionInfo sessionInfo = null;

		try {
			ResponseEntity<SessionInfo> responseEntity = restHelper.getRestService().getSessionInfo();
			if (responseEntity != null && responseEntity.hasBody() && responseEntity.getStatusCode() == HttpStatus.OK) {
				sessionInfo = responseEntity.getBody();
				
				if (sessionInfo == null) {
					return null;
				}
			} else {
				return null;
			}
		} catch (Exception exception) {
			showDialogInfo(NetworkHelper.translateMessage(exception));
		}

		return sessionInfo;
	}

	@Click(R.id.login__Login__button)
	@Background
	public void login() {
		try {
			setLoaderVisible(true);

			if (validateControl(userNameEditText) && validateControl(passwordEditText)) {
				MainApp.getInstance().setLogin(userNameEditText.getText().toString());
				MainApp.getInstance().setPassword(passwordEditText.getText().toString());

				if (appSettings.serviceUrl().get() == null) {
					showDialogInfo(getString(R.string.pref_services_url_missing));
					return;
				}

				if (hasChangesSettings || isChangeLoginPref()) {
					restHelper.reinitialize();
				}

				// check login offline
				if (isLastGoodConfiguration() && !hasChangesSettings) {
					restHelper.initialize();
					databaseHelper.init();

					if (MainApp.getInstance().getSessionInfo() != null) {
						afterDoLogin();

						return;
					}
				}

				// check login online
				restHelper.initialize();
				MainApp.getInstance().setSessionInfo(getOnlineSessionInfo());

				if (MainApp.getInstance().getSessionInfo() != null) {
					databaseHelper.init();
					savePref();
					saveLastConfigPref();
					hasChangesSettings = false;
					afterDoLogin();
				}
			}
		} catch (Exception exception) {
			showDialogInfo(NetworkHelper.translateMessage(exception));
		} finally {
			setLoaderVisible(false);
		}
	}

	@Background
	public void afterDoLogin(){
		AppUpdateService_.intent(MainApp.getInstance()).checkForAppUpdate().start();		
		showMenu();
	}
	
	@Background
	public void doAppUpdate(){
		try {
			File updateAppFile = FileHelper.saveAppUpdateFile(MainApp.getInstance().getApplicationContext(), restHelper.getRestService().getUpdate(updateInfo.getFileName()));

			if (updateAppFile != null && updateAppFile.exists()) {
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.fromFile(updateAppFile), "application/vnd.android.package-archive");
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				
				// Finish activity when do update application
				getActivity().finish();
				
				return;
			}		
		} catch (Exception e) {
			LogHelper.setErrorMessage(getClass().getName() + " - doAppUpdate", e);
		}
		
		showMenu();
	}
	
	@UiThread
	public void askForAppUpdate() {
		if (SynchronizationHelper.existsDataToSend()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(getString(R.string.alert_dialog__information__title));
			builder.setMessage(getString(R.string.login__update_app_exists_data_to_send_msg));
			builder.setPositiveButton(getString(com.u4.mobile.R.string.ok), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					showMenu();
				}
			});

			AlertDialog alert = builder.create();
			alert.show();
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(getString(R.string.alert_dialog__information__title));
			builder.setMessage(getString(R.string.login__update_app_enable_question));
			builder.setPositiveButton(getString(com.u4.mobile.R.string.yes), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					doAppUpdate();
				}
			});
			builder.setNegativeButton(getString(com.u4.mobile.R.string.no), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					showMenu();
				}
			});

			AlertDialog alert = builder.create();
			alert.show();
		}
	}
	
	@UiThread
	public void showMenu() {
		if (!databaseHelper.databaseExists() || !databaseHelper.isVersionConsistent()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(getString(R.string.alert_dialog__information__title));
			builder.setMessage(getString(R.string.login__check_database_info));
			builder.setPositiveButton(getString(com.u4.mobile.R.string.ok), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					
					final Intent intent = AdministrationAct_.intent(MainApp.getInstance().getApplicationContext()).mGoToStartActAfterDownloadDatabase(true).get();
					startActivity(intent);
				}
			});

			AlertDialog alert = builder.create();
			alert.show();
		} else {
			final Intent intent = new Intent(MainApp.getInstance().getApplicationContext(), ShippingOrderStopsAct_.class);
			startActivity(intent);
		}
	}

	private void savePref() {
		appSettings.userName().put(userNameEditText.getText().toString());
		appSettings.userPassword().put(passwordEditText.getText().toString());
	}

	private void saveLastConfigPref() {
		appSettings.lastUserName().put(userNameEditText.getText().toString());
		appSettings.lastUserPassword().put(passwordEditText.getText().toString());
		appSettings.hasChangedSettings().put(false);

	}

	private boolean isLastGoodConfiguration() {
		if (StringUtils.hasText(appSettings.lastUserName().get()) && appSettings.lastUserName().get().equals(userNameEditText.getText().toString())
				&& StringUtils.hasText(appSettings.lastUserPassword().get()) && appSettings.lastUserName().get().equals(passwordEditText.getText().toString())) {
			return true;
		}

		return false;
	}

	private boolean isChangeLoginPref() {
		if (StringUtils.hasText(appSettings.userName().get()) && appSettings.userName().get().equals(userNameEditText.getText().toString()) && StringUtils.hasText(appSettings.userPassword().get())
				&& appSettings.userName().get().equals(passwordEditText.getText().toString())) {
			return false;
		}

		return true;
	}

	private boolean validateControl(EditText control) {
		if (!StringUtils.hasLength(control.getText().toString())) {
			control.setError(getResources().getString(R.string.app_required_field));
			return false;
		}
		
		return true;
	}

	public void onBarcodeScan(String data) {
		if (getCurrentControl() != null) {
			getCurrentControl().setText(data);

			if (StringUtils.hasText(getCurrentControl().getText()) && getCurrentControl().getId() == R.id.login__password_edit_view) {
				if (StringUtils.hasLength(userNameEditText.getText())) {
					View nextControl = getCurrentControl().focusSearch(View.FOCUS_UP);
					if (nextControl != null) {
						nextControl.requestFocus();
					}
					login();
					return;
				}
			}

			View nextControl = getCurrentControl().focusSearch(View.FOCUS_DOWN);

			if (nextControl != null) {
				nextControl.requestFocus();
			}
		}
	}
}