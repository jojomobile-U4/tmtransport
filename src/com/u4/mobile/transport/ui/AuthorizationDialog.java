package com.u4.mobile.transport.ui;

import java.util.NoSuchElementException;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.u4.mobile.model.SessionInfo;
import com.u4.mobile.transport.services.RestHelper;

@EFragment
public class AuthorizationDialog extends com.u4.mobile.base.ui.AuthorizationDialog {

	@Bean
	public RestHelper restHelper;

	@Override
	public SessionInfo doAuthorize(String userName, String password) {
		SessionInfo sessionInfo = null;
		
		// TODO: For presentation, then it should be delete
		if("AT2".equalsIgnoreCase(userName) && "AT2".equalsIgnoreCase(password)){
			sessionInfo = new SessionInfo();
			sessionInfo.setUserId(39);
			sessionInfo.setUserName("AT2");
			sessionInfo.setOsobId(324);
			
			return sessionInfo;
		}

		try {
			// set new restHelper settings
			restHelper.reinitialize(userName, password);
			ResponseEntity<SessionInfo> responseEntity = restHelper.getRestService().getSessionInfo();

			if (responseEntity != null && responseEntity.hasBody() && responseEntity.getStatusCode() == HttpStatus.OK) {
				sessionInfo = responseEntity.getBody();
			} else {
				throw new NoSuchElementException();
			}
		} finally {
			// restore restHelper settings
			restHelper.reinitialize();
		}

		return sessionInfo;
	}

	public SessionInfo getSessionInfo() {
		SessionInfo sessionInfo = null;

		return sessionInfo;
	}

}
