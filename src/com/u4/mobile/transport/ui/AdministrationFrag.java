package com.u4.mobile.transport.ui;

import java.sql.SQLException;
import java.util.Locale;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import com.u4.mobile.R;
import com.u4.mobile.base.app.ApplicationEnums.SyncDirection;
import com.u4.mobile.base.app.ApplicationInterfaces.IMainMenuFrafments;
import com.u4.mobile.base.helpers.NetworkHelper;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.transport.app.MainApp;
import com.u4.mobile.transport.ui.shippingorders.ShippingOrderStopsAct_;
import com.u4.mobile.transport.utils.SynchronizationHelper;
import com.u4.mobile.utils.LogHelper;

@EFragment(R.layout.fragment_main_menu_administration)
public class AdministrationFrag extends FragmentBase implements IMainMenuFrafments {

	@FragmentArg(AdministrationAct.GO_TO_START_ACT_AFTER_DOWNLOAD_DATABASE__PARAM)
	Boolean mGoToStartActAfterDownloadDatabase;

	@Bean
	SynchronizationHelper mSynchronizationHelper;

	@Bean
	DatabaseHelper mDatabaseHelper;

	@UiThread
	public void afterInitDatabase() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(getString(R.string.alert_dialog__information__title));
		builder.setMessage(getString(R.string.sync__init_database_get_finish_ok_msg));
		builder.setPositiveButton(getString(com.u4.mobile.R.string.ok), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();

				if (mGoToStartActAfterDownloadDatabase != null && mGoToStartActAfterDownloadDatabase) {
					getActivity().finish();
					final Intent intent = ShippingOrderStopsAct_.intent(MainApp.getInstance().getApplicationContext()).get();
					startActivity(intent);
				}
			}
		});
		
		AlertDialog alert = builder.create();
		alert.show();
	}

	@Click(com.u4.mobile.R.id.adm_sync__init_database__button)
	public void getInitDatabase() {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(getString(R.string.adm_sync__init_database__title));
			builder.setMessage(getString(R.string.adm_sync__init_database__question));
			builder.setPositiveButton(getString(com.u4.mobile.R.string.yes), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

					setLoaderVisible(true, com.u4.mobile.R.string.sync__init_database_get_message);
					synchronize();
					dialog.dismiss();
				}
			});
			builder.setNegativeButton(getString(com.u4.mobile.R.string.no), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			
			AlertDialog alert = builder.create();
			alert.show();

		} catch (Exception e) {
			showDialogInfo(NetworkHelper.translateMessage(e, SyncDirection.Download));
		} finally {
			setLoaderVisible(false);
		}
	}

	@Override
	public String getPageTitle() {
		return MainApp.getInstance().getApplicationContext().getResources().getString(R.string.main_menu__administration__tab_title).toUpperCase(Locale.getDefault());
	}

	@Click(com.u4.mobile.R.id.adm_sync__send_data__button)
	@Background
	public void sendData() {
		try {
			setLoaderVisible(true, com.u4.mobile.R.string.adm_sync__sending_data__msg);
			mSynchronizationHelper.sendData();
			showDialogInfo(com.u4.mobile.R.string.adm_sync__send_data_finish_ok__message);
		} catch (Exception e) {
			showDialogInfo(NetworkHelper.translateMessage(e, SyncDirection.Upload));
		} finally {
			setLoaderVisible(false);
		}
	}

	@AfterViews
	void setupView() {
		setLoaderVisible(false);
	}

	@Background
	protected void synchronize() {
		try {
			if (!NetworkHelper.checkInternet()) {
				showDialogInfo(com.u4.mobile.R.string.network__no_connection_to_internet);

				return;
			}

			if (mDatabaseHelper.databaseExists() && mDatabaseHelper.isVersionConsistent() && SynchronizationHelper.existsDataToSend()) {
				showDialogInfo(R.string.sync__init_database_data_to_send_exists__msg);

				return;
			}

			setLoaderVisible(true, com.u4.mobile.R.string.sync__init_database_get_message);

			if (mSynchronizationHelper.getInitDatabase(false)) {
				afterInitDatabase();
			} else {
				showDialogInfo(getString(com.u4.mobile.R.string.adm_sync__sync_finish_bad__message));
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage("AdministrationFrag.synchronize()", e);
		} catch (Exception e) {
			showDialogInfo(NetworkHelper.translateMessage(e, SyncDirection.Download));
		} finally {
			setLoaderVisible(false);
		}
	}
}
