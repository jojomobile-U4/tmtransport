package com.u4.mobile.transport.ui.shippingorders;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.OrmLiteDao;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.ui.ListAdapterBase;
import com.u4.mobile.model.DaoHelper;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.transport.model.Stop;

@EBean
public class ShippingOrderStopsListAdapter extends ListAdapterBase<Stop> {

	@OrmLiteDao(helper = DatabaseHelper.class, model = Stop.class)
	static Dao<Stop, Long> stopsDao;

	private Long shorId;

	public ShippingOrderStopsListAdapter() {
		super();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder = null;

		try {
			if (convertView == null) {
				final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.shipping_order_stops_list_view_item, parent, false);

				holder = new ViewHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.populateFrom(getItem(position));
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	public void init(Long shorId) {

		this.shorId = shorId;
	}

	@Override
	public QueryBuilder<Stop, Long> prepareQuery() {
		return null;
	}

	@Override
	protected List<Stop> getData() throws SQLException {
		HashMap<String, Object> queryParam = new HashMap<String, Object>();
		queryParam.put("ShippingOrderLine", ShippingOrderLine.NAME);
		queryParam.put("StpsGuidLoading", ShippingOrderLine.STPS_GUID_LOADING_FIELD_NAME);
		queryParam.put("StpsGuidUnloading", ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME);
		queryParam.put("Canceled", ShippingOrderLine.CANCELED_FIELD_NAME);
		queryParam.put("Delivered", ShippingOrderLine.DELIVERED_FIELD_NAME);
		queryParam.put("Loaded", ShippingOrderLine.LOADED_FIELD_NAME);
		queryParam.put("Stop", Stop.NAME);
		queryParam.put("StopSequence", Stop.STOP_SEQUENCE_FIELD_NAME);
		queryParam.put("ShorId", Stop.SHOR_ID_FIELD_NAME);
		queryParam.put("ShorIdFilter", shorId != null ? shorId : -1);
		queryParam.put("LocaLocationType", Stop.LOCA_LOCATION_TYPE_FIELD_NAME);
		queryParam.put("LocaLocationName", Stop.LOCA_LOCATION_NAME_FIELD_NAME);
		queryParam.put("LocaLocationAddress", Stop.LOCA_LOCATION_ADDRESS_FIELD_NAME);
		queryParam.put("SourceDocType", ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME);
		queryParam.put("LIMIT", getPageSize());
		queryParam.put("OFFSET", getListOffset() * getPageSize());

		StringBuilder query = new StringBuilder();
		query.append("SELECT stps.Id, stps.Guid, stps.%(ShorId), stps.%(LocaLocationType), stps.%(LocaLocationName), stps.%(LocaLocationAddress), ");
		query.append("       (SELECT count(*) ");
		query.append("          FROM %(ShippingOrderLine) shoi ");
		query.append("  	   WHERE     (shoi.%(StpsGuidLoading) = stps.Guid OR shoi.%(StpsGuidUnloading) = stps.Guid)");
		query.append("               AND (   (stps.%(LocaLocationType) = 'HUB' AND shoi.%(SourceDocType) = 'NOSN' AND shoi.%(Canceled) = 0 AND shoi.%(Delivered) = 0 AND shoi.%(Loaded) = 0) ");
		query.append("                    OR (stps.%(LocaLocationType) <> 'HUB' AND shoi.%(Canceled) = 0 AND shoi.%(Delivered) = 0))) as Done ");
		query.append("  FROM %(Stop) stps");
		query.append(" WHERE stps.%(ShorId) = %(ShorIdFilter)");
		query.append(" ORDER BY stps.%(StopSequence)");
		query.append(" LIMIT %(LIMIT) OFFSET %(OFFSET)");

		final GenericRawResults<Stop> data = stopsDao.queryRaw(DaoHelper.formatQuery(query.toString(), queryParam), new RawRowMapper<Stop>() {

			@Override
			public Stop mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
				Stop result = new Stop();
				result.setId(Long.parseLong(resultColumns[0]));
				result.setGuid(resultColumns[1]);
				result.setShorId(Long.parseLong(resultColumns[2]));
				result.setLocaLocationType(resultColumns[3]);
				result.setLocaLocationName(resultColumns[4]);
				result.setLocaLocationAddress(resultColumns[5]);
				result.setDone(Integer.parseInt(resultColumns[6]) > 0 ? false : true);

				return result;
			}
		});

		return data.getResults();
	}

	class ViewHolder {
		TextView primaryLineTextView;
		TextView secondaryLineTextView;
		TextView indicator;

		ViewHolder(View row) {
			primaryLineTextView = (TextView) row.findViewById(R.id.shipping_order_stops__primary_line);
			secondaryLineTextView = (TextView) row.findViewById(R.id.shipping_order_stops__secondary_line);
			indicator = (TextView) row.findViewById(R.id.shipping_order_stops__indicator);
		}

		void populateFrom(Stop item) {
			indicator.setVisibility(View.VISIBLE);

			if (item.isDone()) {
				indicator.setBackgroundResource(R.drawable.list_item_indicator_green);
			} else {
				indicator.setBackgroundResource(R.drawable.list_item_indicator_red);
			}

			highlightText(primaryLineTextView, item.getLocaLocationName());
			highlightText(secondaryLineTextView, item.getLocaLocationAddress());
		}
	}

}
