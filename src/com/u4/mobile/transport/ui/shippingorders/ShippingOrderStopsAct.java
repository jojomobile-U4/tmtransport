package com.u4.mobile.transport.ui.shippingorders;

import org.androidannotations.annotations.EActivity;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;

import com.u4.mobile.base.ui.NavigationDrawerActivity;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.DeliveryRejectionReason;
import com.u4.mobile.transport.ui.incidents.IncidentAct;

@EActivity
public class ShippingOrderStopsAct extends NavigationDrawerActivity {

	private ShippingOrderStopsFrag shippingOrderStopsFrag;

	@Override
	public void poplutateView() {
		super.poplutateView();

		shippingOrderStopsFrag = ShippingOrderStopsFrag_.builder().build();
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.add(R.id.navigation_drawer_frame_container, shippingOrderStopsFrag).commit();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == IncidentAct.RESULT_NUM) {
			if (resultCode == RESULT_OK) {
				String text = data.getStringExtra(DeliveryRejectionReason.DESCRIPTION__FIELD_NAME);
				Long id = data.getLongExtra(DownloadTransferObjectBase.ID__FIELD_NAME, 0);

				if (text.length() == 0 || id == 0) {
					text = ":(";
					id = (long) -1;
				}
			}
		}
	}
}
