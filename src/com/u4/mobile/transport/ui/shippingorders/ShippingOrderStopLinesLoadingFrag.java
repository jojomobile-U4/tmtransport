package com.u4.mobile.transport.ui.shippingorders;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.springframework.util.StringUtils;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.app.ApplicationInterfaces.ITabFrafments;
import com.u4.mobile.base.ui.ListFragmentBase;
import com.u4.mobile.model.ApplicationParameter;
import com.u4.mobile.model.DaoManager;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.scanner.Scanner.Scanable;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.app.MainApp;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.transport.model.Stop;
import com.u4.mobile.transport.model.ShippingOrderLine.SourceDocType;
import com.u4.mobile.transport.ui.incidents.SelectIncidentDialog;
import com.u4.mobile.transport.ui.incidents.SelectIncidentDialog.OnSelectIncydentListener;
import com.u4.mobile.transport.ui.incidents.SelectIncidentDialog_;
import com.u4.mobile.transport.ui.shippingorders.ShippingOrderStopLinesListBaseAdapter.LineContextType;
import com.u4.mobile.utils.AppParametersHelper;
import com.u4.mobile.utils.LogHelper;

@OptionsMenu(R.menu.shipping_order_stop_lines_loading_menu)
@EFragment(R.layout.lista_view_quick_search)
public class ShippingOrderStopLinesLoadingFrag extends ListFragmentBase<ShippingOrderLine> implements ITabFrafments, Scanable {

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	public Dao<ShippingOrderLine, Long> shippingOrderLineDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = Stop.class)
	public Dao<Stop, Long> stopDao;

	private ActionMode actionMode;
	private SetLoadingStatusActionModeCallback actionModeCallback = new SetLoadingStatusActionModeCallback();

	@Bean
	ShippingOrderStopLinesListBaseAdapter adapter;

	@FragmentArg(ShippingOrderStopLinesAct.SHIPPING_ORDER_STOP_GUID__PARAM)
	String shippingOrderStopGuid;

	@ViewById(R.id.list__list_View)
	protected ListView listView;

	@ViewById(R.id.searchView)
	protected EditText searchView;

	@ViewById(R.id.list_view_empty_view)
	LinearLayout listViewEmptyView;

	private Stop stopObject = null;

	@Bean
	AppParametersHelper appParametersHelper;

	@Override
	public String getPageTitle() {
		return MainApp.getInstance().getApplicationContext().getResources().getString(R.string.shipping_order_stop_lines_loading__tab_title).toUpperCase(Locale.getDefault());
	}

	@Background
	public void loadData() {
		try {
			stopObject = DaoManager.queryForGuid(shippingOrderStopGuid, stopDao);
		} catch (SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		}

		populateView();
	}

	@Override
	public void onBarcodeScan(String data) {
		if (stopObject == null) {
			return;
		}

		try {
			final ShippingOrderLine carrierLine = shippingOrderLineDao
					.queryBuilder()
					.where()
					.raw(String.format("UPPER(%s) = '%s' AND %s = '%s'", ShippingOrderLine.SOURCE_DOC_NUMBER_FIELD_NAME, data.toUpperCase(Locale.getDefault()),
							ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, SourceDocType.Carrier.getValue())).queryForFirst();

			if (carrierLine == null) {
				showDialogInfo(String.format(getString(R.string.shipping_order_stop_lines__msg_carrier_not_found_by_barcode), data));

				return;
			}

			if (!stopObject.getShorId().equals(carrierLine.getShorId())) {
				showDialogInfo(String.format(getString(R.string.shipping_order_stop_lines__msg_carrier_loading_not_intended_for_location), carrierLine.getSourceDocNumber()));

				return;
			}

			if (carrierLine.isLoaded()) {
				showDialogInfo(String.format(getString(R.string.shipping_order_stop_lines__msg_carrier_already_confirmed), carrierLine.getSourceDocNumber()));

				return;
			}

			if (!carrierLine.getStopLoading().getGuid().equalsIgnoreCase(stopObject.getGuid())) {
				QueryBuilder<ShippingOrderLine, Long> queryBuilder = shippingOrderLineDao.queryBuilder();
				queryBuilder.where().eq(ShippingOrderLine.SHOI_GUID__FIELD_NAME, carrierLine.getGuid()).and().eq(ShippingOrderLine.STPS_GUID_LOADING_FIELD_NAME, stopObject.getGuid()).and()
						.eq(ShippingOrderLine.LOADED_FIELD_NAME, false);
				queryBuilder.groupBy(ShippingOrderLine.STPS_GUID_LOADING_FIELD_NAME);

				List<ShippingOrderLine> lines = queryBuilder.query();

				if (lines != null && lines.size() == 1 && lines.get(0).getStopLoading().getGuid().equalsIgnoreCase(stopObject.getGuid())) {
					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setTitle(getString(R.string.shipping_order_stop_lines__question_carrier_loading_not_intended_for_location_title));
					builder.setMessage(String.format(getString(R.string.shipping_order_stop_lines__question_carrier_loading_not_intended_for_location), carrierLine.getSourceDocNumber()));
					builder.setPositiveButton(getString(com.u4.mobile.R.string.yes), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							ShippingOrderLine.confirmSubCarrierLines(carrierLine, null, stopObject.getGuid(), ShippingOrderLine.LOADED_FIELD_NAME, true);
						}
					});
					builder.setNegativeButton(getString(com.u4.mobile.R.string.no), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});

					AlertDialog alert = builder.create();
					alert.show();
				} else {
					showDialogInfo(String.format(getString(R.string.shipping_order_stop_lines__msg_carrier_loading_not_intended_for_location), carrierLine.getSourceDocNumber()));
				}
			} else {
				carrierLine.confirmLoading(true);
			}

			reloadData(-1);
			listView.invalidateViews();
		} catch (SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		}
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);

		MenuItem setLoadedItem = menu.findItem(R.id.shipping_order_stop_lines_loading_context_menu__set_loaded);
		MenuItem unsetLoadedItem = menu.findItem(R.id.shipping_order_stop_lines_loading_context_menu__unset_loaded);

		ApplicationParameter param = appParametersHelper.getParameter(AppParametersHelper.CONFIRM_LOAD_MANUALLY);
		boolean visible = param != null && "T".equalsIgnoreCase(param.getValue());

		setLoadedItem.setVisible(visible);
		unsetLoadedItem.setVisible(visible);
	}

	@OptionsItem(R.id.shipping_order_stop_lines_loading_context_menu__incident)
	public boolean optionIncidentSelected() {
		adapter.setSelecting(true, String.format("shoi.%s IS NULL", ShippingOrderLine.PROD_ID_FIELD_NAME));
		reloadData(-1);
		listView.invalidateViews();

		if (actionMode == null) {
			actionModeCallback.setActionModeContext(ActionModeContext.SetIncydent);
			actionMode = getActivity().startActionMode(actionModeCallback);
		}

		return true;
	}

	@OptionsItem(R.id.shipping_order_stop_lines_loading_context_menu__set_loaded)
	public boolean optionSetLoadedSelected() {
		adapter.setSelecting(true, String.format("shoi.%s = 0", ShippingOrderLine.LOADED_FIELD_NAME));
		reloadData(-1);
		listView.invalidateViews();

		if (actionMode == null) {
			actionModeCallback.setActionModeContext(ActionModeContext.SetLoading);
			actionMode = getActivity().startActionMode(actionModeCallback);
		}

		return true;
	}

	@OptionsItem(R.id.shipping_order_stop_lines_loading_context_menu__unset_loaded)
	public boolean optionUnsetLoadedSelected() {
		adapter.setSelecting(true, String.format("shoi.%s = 1", ShippingOrderLine.LOADED_FIELD_NAME));
		reloadData(-1);
		listView.invalidateViews();

		if (actionMode == null) {
			actionModeCallback.setActionModeContext(ActionModeContext.SetUnloading);
			actionMode = getActivity().startActionMode(actionModeCallback);
		}

		return true;
	}

	@UiThread
	public void populateView() {
		adapter.setBaseFilter(String.format("shoi.%s = %s AND shoi.%s = '%s' AND shoi.%s = 'NOSN'", ShippingOrderLine.SHOR_ID_FIELD_NAME, stopObject.getShorId(),
				ShippingOrderLine.STPS_GUID_LOADING_FIELD_NAME, shippingOrderStopGuid, ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME));

		adapter.setBaseOrderBy(String.format("shoi.%s, stps_u.%s DESC, shoi_parent.%s, shoi.%s", ShippingOrderLine.DONE_FIELD_NAME, Stop.STOP_SEQUENCE_FIELD_NAME,
				ShippingOrderLine.SOURCE_DOC_NUMBER_FIELD_NAME, ShippingOrderLine.SOURCE_DOC_NUMBER_FIELD_NAME));

		super.getList();
	}

	@Background
	public void reportIncident(List<Long> selectedItems, Long reasonId, String reasonDescription) {
		setLoaderVisible(true);
		ShippingOrderLine.reportIncidentById(selectedItems, reasonId, reasonDescription);
		setLoaderVisible(false);
		reloadData(0);
		listView.invalidateViews();
	}

	@AfterViews
	void onCreateView() {
		adapter.setLineContextType(LineContextType.Loading);
		super.adapter = adapter;
		super.listView = listView;

		listView.setEmptyView(listViewEmptyView);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				if (adapter.isSelecting()) {
					adapter.setItemChecked(position, !adapter.isItemChecked(position));
				}
			}
		});

		searchView.addTextChangedListener(adapter.getTextWatcher());

		loadData();
	}

	private void finishSelectMode() {
		adapter.setSelecting(false);
		reloadData(0);
		listView.invalidateViews();

		if (actionMode != null) {
			actionMode.finish();
		}
	}

	public enum ActionModeContext {
		SetIncydent, SetLoading, SetUnloading
	}

	public class SetLoadingStatusActionModeCallback implements ActionMode.Callback {

		private ActionModeContext actionModeContext = ActionModeContext.SetLoading;

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
			case R.id.shipping_order_stop_lines_loading_context_menu__select_all:
				changeAllItemSelected();

				if (isSetAllSelected()) {
					item.setIcon(R.drawable.ab_select_all);
					item.setTitle(R.string.app_action__unselect_all);
				} else {
					item.setIcon(R.drawable.ab_unselect_all);
					item.setTitle(R.string.app_action__select_all);
				}

				return true;
			case R.id.shipping_order_stop_lines_loading_context_menu__set_loaded:
			case R.id.shipping_order_stop_lines_loading_context_menu__unset_loaded:
				final List<Long> selectedItems = adapter.getCheckedItemsIds();

				if (selectedItems.size() > 0) {
					ShippingOrderLine.confirmLoadingById(selectedItems, actionModeContext == ActionModeContext.SetLoading);
				}

				break;
			case R.id.shipping_order_stop_lines_loading_context_menu__set_incident:
				final List<Long> checkedItems = adapter.getCheckedItemsIds();

				if (checkedItems.size() > 0) {
					final SelectIncidentDialog dialog = SelectIncidentDialog_.builder().build();
					dialog.setListener(new OnSelectIncydentListener() {

						@Override
						public void onSelectIncydent(Long reasonId, String reasonDescription) {
							if (reasonId != null && StringUtils.hasText(reasonDescription)) {
								reportIncident(checkedItems, reasonId, reasonDescription);
							}
						}
					});

					dialog.show(getFragmentManager(), "selectIncidentDialog");
				}

				break;
			}

			mode.finish();

			return true;
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mode.getMenuInflater().inflate(R.menu.shipping_order_stop_lines_loading_context_menu, menu);

			MenuItem setDeliveredItem = menu.findItem(R.id.shipping_order_stop_lines_loading_context_menu__set_loaded);
			MenuItem unsetDeliveredItem = menu.findItem(R.id.shipping_order_stop_lines_loading_context_menu__unset_loaded);
			MenuItem setIncydentItem = menu.findItem(R.id.shipping_order_stop_lines_loading_context_menu__set_incident);

			setDeliveredItem.setVisible(false);
			unsetDeliveredItem.setVisible(false);
			setIncydentItem.setVisible(false);

			switch (actionModeContext) {
			case SetIncydent:
				setIncydentItem.setVisible(true);
				break;
			case SetLoading:
				setDeliveredItem.setVisible(true);
				break;
			case SetUnloading:
				unsetDeliveredItem.setVisible(true);
				break;
			default:
				break;
			}

			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			actionMode = null;
			finishSelectMode();
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		public void setActionModeContext(ActionModeContext actionModeContext) {
			this.actionModeContext = actionModeContext;
		}
	};

}
