package com.u4.mobile.transport.ui.shippingorders;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.u4.mobile.R;
import com.u4.mobile.base.ui.ActivityBase;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.base.ui.PageSectionsPagerAdapter;

@EActivity(com.u4.mobile.R.layout.fragment_tab)
public class ShippingOrderStopLinesAct extends ActivityBase {

	public static final String LOADING__PARAM = "loading";
	public static final String SHIPPING_ORDER_STOP_GUID__PARAM = "shippingOrderStopGuid";

	private PagerAdapter pagerAdapter;

	private String shippingOrderStopGuid;

	@ViewById(R.id.pager)
	ViewPager viewPager;

	private List<FragmentBase> getFragments() {
		List<FragmentBase> fragmentList = new ArrayList<FragmentBase>();
		ShippingOrderStopLinesUnLoadingFrag fragment1 = ShippingOrderStopLinesUnLoadingFrag_.builder().shippingOrderStopGuid(shippingOrderStopGuid).build();
		fragmentList.add(fragment1);

		ShippingOrderStopLinesLoadingFrag fragment2 = ShippingOrderStopLinesLoadingFrag_.builder().shippingOrderStopGuid(shippingOrderStopGuid).build();
		fragmentList.add(fragment2);

		return fragmentList;
	}

	@AfterViews
	public void onCreateView() {
		Bundle data = getIntent().getExtras();

		if (data != null && data.containsKey(SHIPPING_ORDER_STOP_GUID__PARAM)) {
			shippingOrderStopGuid = data.getString(SHIPPING_ORDER_STOP_GUID__PARAM);
		}

		pagerAdapter = new PageSectionsPagerAdapter(getSupportFragmentManager(), getFragments());
		viewPager.setAdapter(pagerAdapter);
		viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				invalidateOptionsMenu();
			}
		});

		if (data != null && data.containsKey(LOADING__PARAM) && data.getBoolean(LOADING__PARAM)) {
			viewPager.setCurrentItem(1, true);
		}
	}
}
