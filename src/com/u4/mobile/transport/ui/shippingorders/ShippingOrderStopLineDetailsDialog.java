package com.u4.mobile.transport.ui.shippingorders;

import java.sql.SQLException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.u4.mobile.base.ui.DialogFragmentBase;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.utils.LogHelper;

@EFragment(R.layout.shipping_order_stop_line_details__dialog)
public class ShippingOrderStopLineDetailsDialog extends DialogFragmentBase {

	public static final String SHIPPING_ORDER_LINE_ID_PARAM = "shippingOrderLineId";

	@FragmentArg(SHIPPING_ORDER_LINE_ID_PARAM)
	Long mShippingOrderLineId;

	@ViewById(R.id.shipping_order_stop_line_details__stop_header)
	TextView mStopHeader;

	@ViewById(R.id.shipping_order_stop_line_details__stop_address)
	TextView mStopAddress;

	@ViewById(R.id.shipping_order_stop_line_details__order)
	TextView mOrderSymbol;

	@ViewById(R.id.shipping_order_stop_line_details__invoice)
	TextView mInvoiceSymbol;
	
	@ViewById(R.id.shipping_order_stop_line_details__turnover_document)
	TextView mTurnoverDocument;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	public Dao<ShippingOrderLine, Long> shippingOrderLineDao;

	private ShippingOrderLine mLineObject;

	@Click(R.id.shipping_order_stop_line_details__ok_button)
	public void buttonOkClick() {
		dismiss();
	}

	@AfterViews
	@Background
	public void onCreateView() {
		try {
			mLineObject = shippingOrderLineDao.queryForId(mShippingOrderLineId);
		} catch (SQLException e) {
			LogHelper.setErrorMessage(getClass().getName(), e);
		} finally {
			populateView();
		}
	}

	@UiThread
	public void populateView() {
		getDialog().setTitle(R.string.shipping_order_stop_line_details__title);

		if (mLineObject == null) {
			mStopHeader.setText("");
			mStopAddress.setText("");
			mOrderSymbol.setText("");
			mInvoiceSymbol.setText("");
			mTurnoverDocument.setText("");

			return;
		}

		mOrderSymbol.setText(mLineObject.getSordSymbol());
		mInvoiceSymbol.setText(mLineObject.getSinvSymbol());
		mTurnoverDocument.setText(mLineObject.getDoobSymbol());
		
		if (mLineObject.getStopUnloading() != null) {
			mStopHeader.setText(mLineObject.getStopUnloading().getLocaLocationName());
			mStopAddress.setText(mLineObject.getStopUnloading().getLocaLocationAddress());
		}
	}
}
