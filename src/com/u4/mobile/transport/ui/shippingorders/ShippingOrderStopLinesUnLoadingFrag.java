package com.u4.mobile.transport.ui.shippingorders;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.app.ApplicationInterfaces.ITabFrafments;
import com.u4.mobile.base.ui.ListFragmentBase;
import com.u4.mobile.model.ApplicationParameter;
import com.u4.mobile.model.DaoManager;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.scanner.Scanner.Scanable;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.app.MainApp;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.transport.model.Stop;
import com.u4.mobile.transport.model.ShippingOrderLine.SourceDocType;
import com.u4.mobile.transport.ui.shippingorders.ShippingOrderStopLinesListBaseAdapter.LineContextType;
import com.u4.mobile.utils.AppParametersHelper;
import com.u4.mobile.utils.LogHelper;

@OptionsMenu(R.menu.shipping_order_stop_lines_unloading_menu)
@EFragment(R.layout.lista_view_quick_search)
public class ShippingOrderStopLinesUnLoadingFrag extends ListFragmentBase<ShippingOrderLine> implements ITabFrafments, Scanable {

	private ActionMode actionMode;
	private SetDeliveredActionModeCallback actionModeCallback = new SetDeliveredActionModeCallback();
	private Stop stopObject = null;

	@ViewById(R.id.list__list_View)
	protected ListView listView;

	@ViewById(R.id.searchView)
	protected EditText searchView;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	public Dao<ShippingOrderLine, Long> shippingOrderLineDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = Stop.class)
	public Dao<Stop, Long> stopDao;

	@Bean
	ShippingOrderStopLinesListBaseAdapter adapter;

	@ViewById(R.id.list_view_empty_view)
	LinearLayout listViewEmptyView;

	@FragmentArg(ShippingOrderStopLinesAct.SHIPPING_ORDER_STOP_GUID__PARAM)
	String shippingOrderStopGuid;

	@Bean
	AppParametersHelper appParametersHelper;

	@Override
	public String getPageTitle() {
		return MainApp.getInstance().getApplicationContext().getResources().getString(R.string.shipping_order_stop_lines_unloading__tab_title).toUpperCase(Locale.getDefault());
	}

	@Background
	public void loadData() {
		try {
			stopObject = DaoManager.queryForGuid(shippingOrderStopGuid, stopDao);
		} catch (SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		}

		populateView();
	}

	@Override
	public void onBarcodeScan(String data) {
		if (stopObject == null) {
			return;
		}

		try {
			final ShippingOrderLine carrierLine = shippingOrderLineDao
					.queryBuilder()
					.where()
					.raw(String.format("UPPER(%s) = '%s' AND %s = '%s'", ShippingOrderLine.SOURCE_DOC_NUMBER_FIELD_NAME, data.toUpperCase(Locale.getDefault()),
							ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, SourceDocType.Carrier.getValue())).queryForFirst();

			if (carrierLine == null) {
				showDialogInfo(String.format(getString(R.string.shipping_order_stop_lines__msg_carrier_not_found_by_barcode), data));

				return;
			}

			if (!stopObject.getShorId().equals(carrierLine.getShorId())) {
				showDialogInfo(String.format(getString(R.string.shipping_order_stop_lines__msg_carrier_unloading_not_intended_for_location), carrierLine.getSourceDocNumber()));

				return;
			}

			if (carrierLine.isDelivered()) {
				showDialogInfo(String.format(getString(R.string.shipping_order_stop_lines__msg_carrier_already_confirmed), carrierLine.getSourceDocNumber()));

				return;
			}

			if (!carrierLine.getStopUnloading().getGuid().equalsIgnoreCase(stopObject.getGuid())) {
				QueryBuilder<ShippingOrderLine, Long> queryBuilder = shippingOrderLineDao.queryBuilder();
				queryBuilder.where().eq(ShippingOrderLine.SHOI_GUID__FIELD_NAME, carrierLine.getGuid()).and().eq(ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME, stopObject.getGuid()).and()
						.eq(ShippingOrderLine.DELIVERED_FIELD_NAME, false);
				queryBuilder.groupBy(ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME);

				List<ShippingOrderLine> lines = queryBuilder.query();

				if (lines != null && lines.size() == 1 && lines.get(0).getStopUnloading().getGuid().equalsIgnoreCase(stopObject.getGuid())) {
					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setTitle(getString(R.string.shipping_order_stop_lines__question_carrier_unloading_not_intended_for_location_title));
					builder.setMessage(String.format(getString(R.string.shipping_order_stop_lines__question_carrier_unloading_not_intended_for_location), carrierLine.getSourceDocNumber()));
					builder.setPositiveButton(getString(com.u4.mobile.R.string.yes), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							ShippingOrderLine.confirmSubCarrierLines(carrierLine, stopObject.getGuid(), null, ShippingOrderLine.DELIVERED_FIELD_NAME, true);
						}
					});
					builder.setNegativeButton(getString(com.u4.mobile.R.string.no), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});

					AlertDialog alert = builder.create();
					alert.show();
				} else {
					showDialogInfo(String.format(getString(R.string.shipping_order_stop_lines__msg_carrier_unloading_not_intended_for_location), carrierLine.getSourceDocNumber()));
				}
			} else {
				carrierLine.confirmUnloading(true);
			}

			reloadData(-1);
			listView.invalidateViews();
		} catch (SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		}
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);

		MenuItem setDeliveredItem = menu.findItem(R.id.shipping_order_stop_lines_unloading_context_menu__set_delivered);
		MenuItem unsetDeliveredItem = menu.findItem(R.id.shipping_order_stop_lines_unloading_context_menu__unset_delivered);

		ApplicationParameter param = appParametersHelper.getParameter(AppParametersHelper.CONFIRM_LOAD_MANUALLY);
		boolean visible = param != null && "T".equalsIgnoreCase(param.getValue());

		setDeliveredItem.setVisible(visible);
		unsetDeliveredItem.setVisible(visible);
	}

	@OptionsItem(R.id.shipping_order_stop_lines_unloading_context_menu__set_delivered)
	public boolean optionSetDeliveredSelected() {
		adapter.setSelecting(true, String.format("shoi.%s = 0 AND shoi.%s = 'D'", ShippingOrderLine.DELIVERED_FIELD_NAME, ShippingOrderLine.ROZL_TYPE_FIELD_NAME));
		reloadData(-1);
		listView.invalidateViews();

		if (actionMode == null) {
			actionModeCallback.setDelivered(true);
			actionMode = getActivity().startActionMode(actionModeCallback);
		}

		return true;
	}

	@OptionsItem(R.id.shipping_order_stop_lines_unloading_context_menu__unset_delivered)
	public boolean optionUnsetDeliveredSelected() {
		adapter.setSelecting(true, String.format("shoi.%s = 1", ShippingOrderLine.DELIVERED_FIELD_NAME));
		reloadData(-1);
		listView.invalidateViews();

		if (actionMode == null) {
			actionModeCallback.setDelivered(false);
			actionMode = getActivity().startActionMode(actionModeCallback);
		}

		return true;
	}

	@UiThread
	public void populateView() {
		adapter.setBaseFilter(String.format("shoi.%s = %s AND shoi.%s = '%s' AND shoi.%s = 'NOSN'", ShippingOrderLine.SHOR_ID_FIELD_NAME, stopObject.getShorId(),
				ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME, shippingOrderStopGuid, ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME));
		super.getList();
	}

	@AfterViews
	void onCreateView() {
		adapter.setLineContextType(LineContextType.Unloading);
		super.adapter = adapter;
		super.listView = listView;
		listView.setEmptyView(listViewEmptyView);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				if (adapter.isSelecting()) {
					adapter.setItemChecked(position, !adapter.isItemChecked(position));
				}
			}
		});

		searchView.addTextChangedListener(adapter.getTextWatcher());

		loadData();
	}

	private void finishSelectMode() {
		adapter.setSelecting(false);
		reloadData(0);
		listView.invalidateViews();

		if (actionMode != null) {
			actionMode.finish();
		}
	}

	public class SetDeliveredActionModeCallback implements ActionMode.Callback {

		private boolean delivered = true;

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
			case R.id.shipping_order_stop_lines_unloading_context_menu__select_all:
				changeAllItemSelected();

				if (isSetAllSelected()) {
					item.setIcon(R.drawable.ab_select_all);
					item.setTitle(R.string.app_action__unselect_all);
				} else {
					item.setIcon(R.drawable.ab_unselect_all);
					item.setTitle(R.string.app_action__select_all);
				}

				return true;
			case R.id.shipping_order_stop_lines_unloading_context_menu__set_delivered:
			case R.id.shipping_order_stop_lines_unloading_context_menu__unset_delivered:
				final List<Long> selectedItems = adapter.getCheckedItemsIds();

				if (selectedItems.size() > 0) {
					ShippingOrderLine.confirmUnloadingById(selectedItems, delivered);
				}

				break;
			}

			mode.finish();

			return true;
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mode.getMenuInflater().inflate(R.menu.shipping_order_stop_lines_unloading_context_menu, menu);

			MenuItem setDeliveredItem = menu.findItem(R.id.shipping_order_stop_lines_unloading_context_menu__set_delivered);
			MenuItem unsetDeliveredItem = menu.findItem(R.id.shipping_order_stop_lines_unloading_context_menu__unset_delivered);

			setDeliveredItem.setVisible(delivered);
			unsetDeliveredItem.setVisible(!delivered);

			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			actionMode = null;
			finishSelectMode();
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		public void setDelivered(boolean delivered) {
			this.delivered = delivered;
		}
	};
}
