package com.u4.mobile.transport.ui.shippingorders;

import java.sql.SQLException;
import java.util.Locale;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;
import com.u4.mobile.base.app.ApplicationInterfaces.ITabFrafments;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.model.SessionInfo;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.app.MainApp;
import com.u4.mobile.transport.model.ShippingOrder;
import com.u4.mobile.transport.model.Stop;
import com.u4.mobile.transport.ui.shippingorders.hub.ShippingOrderStopHubServiceAct;
import com.u4.mobile.transport.ui.shippingorders.hub.ShippingOrderStopHubServiceAct_;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.DateTimeHelper.DateTimeFormat;
import com.u4.mobile.utils.LogHelper;

@EFragment(R.layout.lista_view_two_lines)
public class ShippingOrderStopsFrag extends FragmentBase implements ITabFrafments {

	public static final long SEARCH_DELAY_TIME = 500;

	private Stop currentStopObject;
	private boolean isInitialized = false;
	private final Long listPageSize = 10L;
	private ShippingOrder shippingOrderObject = null;

	@ViewById(R.id.list__list_View)
	protected ListView listView;

	@Bean
	ShippingOrderStopsListAdapter adapter;

	@ViewById(R.id.list_view_empty_view)
	LinearLayout listViewEmptyView;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrder.class)
	Dao<ShippingOrder, Long> shippingOrderDao;

	@FragmentArg(ShippingOrderAct.SHIPPING_ORDER_ID__PARAM)
	Long shippingOrderId = null;

	public void getList() {
		if (!isInitialized) {
			adapter.init(this, listPageSize);
			listView.setAdapter(adapter);
			isInitialized = true;
		}

		currentStopObject = null;
		reloadData(-1);
		listView.invalidateViews();
	}

	@Override
	public String getPageTitle() {
		return MainApp.getInstance().getApplicationContext().getResources().getString(R.string.shipping_order_stops__tab_title).toUpperCase(Locale.getDefault());
	}

	@Background
	public void loadData() {
		try {
			if (shippingOrderId == null || shippingOrderId < 0) {
				SessionInfo sessionInfo = MainApp.getInstance().getSessionInfo();

				if (sessionInfo != null && sessionInfo.getOsobId() > 0) {
					Long osobId = MainApp.getInstance().getSessionInfo().getOsobId();
					QueryBuilder<ShippingOrder, Long> queryBuilder = shippingOrderDao.queryBuilder();
					queryBuilder.where().ne(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue()).and().eq(ShippingOrder.OSOB_ID_FIELD_NAME, osobId).and()
							.raw(String.format("Date(%s) = Date('%s')", ShippingOrder.FORWARDING_DATE_FIELD_NAME, DateTimeHelper.getCurrentDateAsString(DateTimeFormat.OracleDateFormat)));

					shippingOrderObject = queryBuilder.queryForFirst();

					if (shippingOrderObject != null) {
						shippingOrderId = shippingOrderObject.getId();
					}
				} else {
					shippingOrderId = -1L;
				}
			} else {
				shippingOrderObject = shippingOrderDao.queryForId(shippingOrderId);
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		}

		populateView();
	}

	@Override
	public void onResume() {
		super.onResume();

		if (isInitialized) {
			loadData();
			listView.invalidateViews();
		}
	}

	@UiThread
	public void populateView() {
		adapter.init(shippingOrderId);
		getList();
	}

	@UiThread
	public void reloadData(long selectObjectId) {
		adapter.reloadData(true, 0L);
		listView.setItemChecked(adapter.getItemPosition(selectObjectId), true);
	}

	@AfterViews
	void setupView() {
		listView.setEmptyView(listViewEmptyView);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				currentStopObject = (Stop) parent.getItemAtPosition(position);

				if (currentStopObject != null) {
					if (currentStopObject.getLocaLocationType().equalsIgnoreCase("HUB")) {
						Intent intent = new Intent(getActivity(), ShippingOrderStopHubServiceAct_.class);
						intent.putExtra(ShippingOrderStopHubServiceAct.SHIPPING_ORDER_STOP_HUB_COURIER_GUID__PARAM, currentStopObject.getGuid());
						startActivity(intent);
					} else {
						Intent intent = new Intent(getActivity(), ShippingOrderStopClientServiceAct_.class);
						intent.putExtra(ShippingOrderStopClientServiceAct.SHIPPING_ORDER_STOP_GUID__PARAM, currentStopObject.getGuid());
						startActivity(intent);
					}
				}
			}
		});

		loadData();
	}
}
