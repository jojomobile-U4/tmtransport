package com.u4.mobile.transport.ui.shippingorders;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import org.androidannotations.annotations.*;

import com.u4.mobile.base.ui.*;
import com.u4.mobile.transport.R;

@EActivity(com.u4.mobile.R.layout.fragment_tab)
public class ShippingOrderAct extends FragmentActivity {

	public static final String SHIPPING_ORDER_ID__PARAM = "shippingOrderId";
	public static final String SHIPPING_ORDER_STOP_ID__PARAM = "shippingOrderStopId";

	private ViewPager pager;
	private PagerAdapter pagerAdapter;
	private Long shippingOrderId;

	@AfterViews
	void setupView() {

		Bundle data = getIntent().getExtras();

		if (data != null) {
			setShippingOrderId(data.getLong(SHIPPING_ORDER_ID__PARAM));
		}

		pager = (ViewPager) findViewById(R.id.pager);
		pagerAdapter = new PageSectionsPagerAdapter(getSupportFragmentManager(), getFragments());
		pager.setAdapter(pagerAdapter);
		pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				invalidateOptionsMenu();
			}
		});
	}

	private List<FragmentBase> getFragments() {
		List<FragmentBase> list = new ArrayList<FragmentBase>();

		ShippingOrderHeaderFrag fr1 = ShippingOrderHeaderFrag_.builder().shippingOrderId(getShippingOrderId()).build();
		list.add(fr1);

		ShippingOrderStopsFrag fr2 = ShippingOrderStopsFrag_.builder().shippingOrderId(getShippingOrderId()).build();
		list.add(fr2);

		ShippingOrderLinesFrag fr3 = ShippingOrderLinesFrag_.builder().shippingOrderId(getShippingOrderId()).build();
		list.add(fr3);

		return list;
	}


	public Long getShippingOrderId() {
		return shippingOrderId;
	}

	public void setShippingOrderId(Long shippingOrderId) {
		this.shippingOrderId = shippingOrderId;
	}

}
