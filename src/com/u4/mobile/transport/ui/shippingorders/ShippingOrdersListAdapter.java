package com.u4.mobile.transport.ui.shippingorders;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.OrmLiteDao;
import org.springframework.util.StringUtils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.u4.mobile.R;
import com.u4.mobile.base.ui.ListAdapterBase;
import com.u4.mobile.model.DaoHelper;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.transport.model.ShippingOrder;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.LogHelper;

@EBean
public class ShippingOrdersListAdapter extends ListAdapterBase<ShippingOrder> {

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrder.class)
	static Dao<ShippingOrder, Long> shippingOrderDao;

	public ShippingOrdersListAdapter() {
		super();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder = null;

		try {
			if (convertView == null) {
				final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.list_item_two_lines, null);

				holder = new ViewHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.populateFrom(getItem(position));
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	class ViewHolder {

		TextView firstLineTextView;
		TextView secondLineTextView;

		ViewHolder(View row) {

			firstLineTextView = (TextView) row.findViewById(R.id.first_line__text_view);
			secondLineTextView = (TextView) row.findViewById(R.id.second_line__text_view);
		}

		void populateFrom(ShippingOrder item) {

			highlightText(firstLineTextView, item.getDocNumber());
			highlightText(secondLineTextView,
					item.getRoutRouteNumber() + " " + DateTimeHelper.getDateAsString(DateTimeHelper.getDate(item.getForwardingDate()), DateTimeHelper.DateTimeFormat.OracleDateFormat));
		}
	}

	@Override
	public QueryBuilder<ShippingOrder, Long> prepareQuery() {

		QueryBuilder<ShippingOrder, Long> query = shippingOrderDao.queryBuilder();
		try {
			Where<ShippingOrder, Long> where = null;
			if (StringUtils.hasText(getSearchText())) {
				where = query.where();
				where.like(ShippingOrder.DOC_NUMBER_FIELD_NAME, DaoHelper.makeLikeExpression(getSearchText()));
				where.or();
				where.like(ShippingOrder.ROUT_ROUTE_NUMBER_FIELD_NAME, DaoHelper.makeLikeExpression(getSearchText()));
				where.or();
				where.like(ShippingOrder.FORWARDING_DATE_FIELD_NAME, DaoHelper.makeLikeExpression(getSearchText()));
			}

			query.setWhere(where);

			// / TO DEBAG ;) = query.prepareStatementString()
			query.offset(getListOffset() * getPageSize()).limit((long) getPageSize());
			query.orderBy(DownloadTransferObjectBase.ID__FIELD_NAME, false);
		} catch (final Exception e) {
			LogHelper.setErrorMessage(ShippingOrdersListAdapter.class.getName(), e);
		}
		return query;
	}
}
