package com.u4.mobile.transport.ui.shippingorders;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import org.androidannotations.annotations.*;

import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.u4.mobile.base.app.ApplicationInterfaces.ITabFrafments;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.app.MainApp;
import com.u4.mobile.transport.model.ShippingOrder;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.LogHelper;

@EFragment(R.layout.shipping_order_header_frag)
public class ShippingOrderHeaderFrag extends FragmentBase implements ITabFrafments {

	@ViewById(R.id.shipping_order_header__document_symbol)
	TextView documentSymbolTextView;

	@ViewById(R.id.shipping_order_header__route_number)
	TextView routeNumberTextView;

	@ViewById(R.id.shipping_order_header__document_date)
	TextView documentDateTextView;

	@ViewById(R.id.shipping_order_header__forwarding_date)
	TextView forwardingDateTextView;

	@ViewById(R.id.shipping_order_header__implements)
	TextView implementsTextView;

	@ViewById(R.id.shipping_order_header__description)
	TextView descriptionTextView;

	@FragmentArg(ShippingOrderAct.SHIPPING_ORDER_ID__PARAM)
	Long shippingOrderId;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrder.class)
	Dao<ShippingOrder, Long> shippingOrderDao;

	private ShippingOrder currentShippingOrder;

	@Override
	public String getPageTitle() {

		return MainApp.getInstance().getApplicationContext().getResources().getString(R.string.shipping_order_header__tab_title).toUpperCase(Locale.getDefault());
	}

	@AfterViews
	@Background
	void loadData() {
		if (shippingOrderId != null) {
			try {
				List<ShippingOrder> shippingOrders = shippingOrderDao.queryForEq(DownloadTransferObjectBase.ID__FIELD_NAME, shippingOrderId);
				if (shippingOrders != null && shippingOrders.size() > 0) {
					currentShippingOrder = shippingOrders.get(0);
					loadView();
				}
			} catch (SQLException e) {
				LogHelper.setErrorMessage("ShippingOrderHeaderFrag.loadView", e);
			}
		}
	}

	@UiThread
	public void loadView() {

		if (currentShippingOrder != null) {
			documentSymbolTextView.setText(currentShippingOrder.getDocNumber() != null ? currentShippingOrder.getDocNumber() : "");
			routeNumberTextView.setText(currentShippingOrder.getRoutRouteNumber() != null ? currentShippingOrder.getRoutRouteNumber() : "");
			documentDateTextView.setText(currentShippingOrder.getDocDate() != null ? DateTimeHelper.getDateAsString(currentShippingOrder.getDocDate(), DateTimeHelper.DateTimeFormat.OracleDateFormat)
					: "");
			forwardingDateTextView.setText(currentShippingOrder.getForwardingDate() != null ? DateTimeHelper.getDateAsString(currentShippingOrder.getForwardingDate(),
					DateTimeHelper.DateTimeFormat.OracleDateFormat) : "");
			implementsTextView.setText(currentShippingOrder.getOsobFullName() != null ? currentShippingOrder.getOsobFullName() : "");
			descriptionTextView.setText(currentShippingOrder.getDescription() != null ? currentShippingOrder.getDescription() : "");
		}
	}
}
