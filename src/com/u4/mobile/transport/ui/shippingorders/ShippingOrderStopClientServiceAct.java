package com.u4.mobile.transport.ui.shippingorders;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.u4.mobile.R;
import com.u4.mobile.base.ui.ActivityBase;

@EActivity(R.layout.activity_standard)
public class ShippingOrderStopClientServiceAct extends ActivityBase {

	public static final String SHIPPING_ORDER_STOP_GUID__PARAM = "shippingOrderStopGuid";

	@AfterViews
	public void onCreateView() {
		String shippingOrderStopGuid = null;
		Bundle data = getIntent().getExtras();

		if (data != null && data.containsKey(SHIPPING_ORDER_STOP_GUID__PARAM)) {
			shippingOrderStopGuid = data.getString(SHIPPING_ORDER_STOP_GUID__PARAM);
		}

		Fragment fragment = ShippingOrderStopClientServiceFrag_.builder().shippingOrderStopGuid(shippingOrderStopGuid).build();
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.add(R.id.activity_standard__content, fragment).commit();
	}
}
