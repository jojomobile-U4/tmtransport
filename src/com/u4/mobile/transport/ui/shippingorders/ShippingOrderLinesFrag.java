package com.u4.mobile.transport.ui.shippingorders;

import java.util.Locale;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.widget.ListView;

import com.u4.mobile.base.app.ApplicationInterfaces.ITabFrafments;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.app.MainApp;

@EFragment(R.layout.lista_view_two_lines)
public class ShippingOrderLinesFrag extends FragmentBase implements ITabFrafments {

	public static final long SEARCH_DELAY_TIME = 500;
	private final Long listPageSize = 10L;

	@ViewById(R.id.list__list_View)
	protected ListView listView;

	@FragmentArg(ShippingOrderAct.SHIPPING_ORDER_ID__PARAM)
	Long shippingOrderId;

	@Bean
	ShippingOrderLinesListAdapter adapter;

	private int getAdapterItemPosition(long id) {
		for (int position = 0; position < listView.getCount(); position++) {
			if (adapter.getItem(position).getId() == id) {
				return position;
			}
		}

		return 0;
	}

	public void getList() {
		adapter.init(this, listPageSize);
		listView.setAdapter(adapter);
		reloadData(-1);
	}

	@Override
	public String getPageTitle() {
		return MainApp.getInstance().getApplicationContext().getResources().getString(R.string.shipping_order_lines__tab_title).toUpperCase(Locale.getDefault());
	}

	@UiThread
	public void reloadData(int selectPositionID) {
		adapter.reloadData(true, 0L);
		selectItem(selectPositionID);
	}

	@UiThread
	public void reloadData(long selectObjectID) {
		adapter.reloadData(true, 0L);
		selectItem(selectObjectID);
	}

	@UiThread
	public void selectItem(int position) {
		listView.setItemChecked(position, true);
	}

	@UiThread
	public void selectItem(long id) {
		listView.setItemChecked(getAdapterItemPosition(id), true);
	}

	@AfterViews
	void setupView() {
		adapter.init(shippingOrderId.toString());
		getList();
	}

}
