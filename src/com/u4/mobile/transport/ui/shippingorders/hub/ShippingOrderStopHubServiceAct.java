package com.u4.mobile.transport.ui.shippingorders.hub;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import com.u4.mobile.base.ui.StandardActivity;

@EActivity
public class ShippingOrderStopHubServiceAct extends StandardActivity {

	public static final String SHIPPING_ORDER_STOP_HUB_COURIER_GUID__PARAM = "shippingOrderStopCourierHubGuid";

	@Extra(SHIPPING_ORDER_STOP_HUB_COURIER_GUID__PARAM)
	String shippingOrderStopCourierHubGuid;

	@Override
	public void registerFragments() {
		addFragment(ShippingOrderStopHubServiceFrag_.builder().shippingOrderStopCourierHubGuid(shippingOrderStopCourierHubGuid).build());
	}

}
