package com.u4.mobile.transport.ui.shippingorders.hub;

import java.sql.SQLException;
import java.util.HashMap;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.content.Intent;
import android.view.View;
import android.widget.Button;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;
import com.u4.mobile.base.ui.AuthorizationDialog.OnAuthorizeListener;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.ApplicationParameter;
import com.u4.mobile.model.DaoHelper;
import com.u4.mobile.model.DaoManager;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.model.SessionInfo;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.ShippingOrder;
import com.u4.mobile.transport.model.ShippingOrder.RoutType;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.transport.model.Stop;
import com.u4.mobile.transport.ui.AuthorizationDialog;
import com.u4.mobile.transport.ui.AuthorizationDialog_;
import com.u4.mobile.transport.ui.shippingorders.ShippingOrderStopLinesAct;
import com.u4.mobile.transport.ui.shippingorders.ShippingOrderStopLinesAct_;
import com.u4.mobile.transport.ui.shippingorders.transfer.ShippingOrderLineStartTransferAct_;
import com.u4.mobile.utils.AppParametersHelper;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.DateTimeHelper.DateTimeFormat;
import com.u4.mobile.utils.LogHelper;

@EFragment(R.layout.shipping_order_stop_hub_service_frag)
public class ShippingOrderStopHubServiceFrag extends FragmentBase {

	public static int REQUEST_CODE__OPENING_TRUCK = 1;
	public static int RESULT_CODE__OPENING_TRUCK_UNLOAD = 1;
	public static int RESULT_CODE__OPENING_TRUCK_LOAD = 2;
	public static int RESULT_CODE__OPENING_TRUCK_UNCHANGED = 0;

	private Long shippingOrderTruckId;
	private String shippingOrderStopTruckHubGuidLine;
	private Stop currentObject;
	private ShippingOrderLine lineUnloading;
	private ShippingOrderLine lineLoading;

	@Bean
	AppParametersHelper appParametersHelper;

	@ViewById(R.id.shipping_order_stop_hub_service__courier_car_load_transfer)
	Button takeOverLoadButton;

	@ViewById(R.id.shipping_order_stop_hub_service__courier_car_loading)
	Button courierCarLoadingButton;

	@ViewById(R.id.shipping_order_stop_hub_service__courier_car_unloading)
	Button courierCarUnloadingButton;

	@ViewById(R.id.shipping_order_stop_hub_service__truck_service)
	Button truckServiceButton;

	@FragmentArg(ShippingOrderStopHubServiceAct.SHIPPING_ORDER_STOP_HUB_COURIER_GUID__PARAM)
	String shippingOrderStopCourierHubGuid;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrder.class)
	Dao<ShippingOrder, Long> shippingOrderDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = Stop.class)
	Dao<Stop, Long> stopDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	Dao<ShippingOrderLine, Long> shippingOrderLineDao;

	@Click(R.id.shipping_order_stop_hub_service__courier_car_loading)
	@Background
	public void buttonCourierCarLoadingClick() {
		goToCourierCarChargeService(true);
	}

	@Click(R.id.shipping_order_stop_hub_service__courier_car_load_transfer)
	@Background
	public void buttonCourierCarLoadTransferClick() {		
		final AuthorizationDialog dialog = AuthorizationDialog_.builder().build();
		dialog.setOnAuthorizeListener(new OnAuthorizeListener() {

			@Override
			public void onAuthorize(SessionInfo sessionInfo) {
				if (sessionInfo != null && sessionInfo.getOsobId() > 0) {
					afterAuthorize(sessionInfo.getOsobId());
				}
			}
		});

		dialog.show(getFragmentManager(), "authorizationDialog");
	}

	@Background
	public void afterAuthorize(Long osobId) {
		QueryBuilder<ShippingOrder, Long> queryBuilder = shippingOrderDao.queryBuilder();

		try {
			queryBuilder.where().ne(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue()).and().eq(ShippingOrder.OSOB_ID_FIELD_NAME, osobId).and()
					.raw(String.format("Date(%s) = Date('%s')", ShippingOrder.FORWARDING_DATE_FIELD_NAME, DateTimeHelper.getCurrentDateAsString(DateTimeFormat.OracleDateFormat)));

			ShippingOrder shippingOrderObject = queryBuilder.queryForFirst();

			if (shippingOrderObject != null) {
				ShippingOrderLineStartTransferAct_.intent(getActivity())
						.mOsobId(osobId)
						.mShippingOrderDestinationId(currentObject.getShorId())
						.mShippingOrderSourceId(shippingOrderObject.getId())
						.mShippingOrderStopHubGuid(currentObject.getGuid()).start();
			} else {
				showDialogInfo(R.string.shipping_order_stop_hub_service__after_authorize_no_shor_data_found__msg);
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage(getClass().getName(), e);
			showDialogInfo(R.string.shipping_order_stop_hub_service__after_authorize_get_shor_data_exception__msg);
		}
	}

	@Click(R.id.shipping_order_stop_hub_service__courier_car_unloading)
	@Background
	public void buttonCourierCarUnLoadingClick() {
		goToCourierCarChargeService(false);
	}

	@Click(R.id.shipping_order_stop_hub_service__truck_service)
	@Background
	public void buttonTruckServiceClick() {
		if (checkTruck()) {
			Intent intent = new Intent(getActivity(), ShippingOrderStopTruckServiceAct_.class);
			intent.putExtra(ShippingOrderStopTruckServiceAct.SHIPPING_ORDER_STOP_TRUCK_HUB_GUID__PARAM, shippingOrderStopTruckHubGuidLine);
			startActivity(intent);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RESULT_CODE__OPENING_TRUCK_UNLOAD || requestCode == RESULT_CODE__OPENING_TRUCK_LOAD) {
			refreshData();
		}
	}

	@AfterViews
	@Background
	public void onCreateView() {
		lineUnloading = null;
		lineLoading = null;

		setLoaderVisible(true);

		if (shippingOrderStopCourierHubGuid != null) {
			try {
				currentObject = DaoManager.queryForGuid(shippingOrderStopCourierHubGuid, stopDao);

				if (currentObject != null) {
					HashMap<String, Object> queryParam = new HashMap<String, Object>();
					queryParam.put("ShippingOrder", ShippingOrder.NAME);
					queryParam.put("RoutType", ShippingOrder.ROUT_TYPE_FIELD_NAME);
					queryParam.put("RoutTypeValue", RoutType.line.getValue());
					queryParam.put("ForwardingDate", ShippingOrder.FORWARDING_DATE_FIELD_NAME);
					queryParam.put("ForwardingDateValue", DateTimeHelper.getCurrentDateAsString(DateTimeFormat.OracleDateFormat));
					queryParam.put("Stop", Stop.NAME);
					queryParam.put("ShorId", Stop.SHOR_ID_FIELD_NAME);
					queryParam.put("LocaId", Stop.LOCA_ID__FIELD_NAME);
					queryParam.put("LocaIdValue", currentObject.getLocaId());

					StringBuilder query = new StringBuilder();

					query.append("SELECT shor.Id, stps.Guid");
					query.append("  FROM %(ShippingOrder) shor");
					query.append(" INNER JOIN %(Stop) stps ON shor.Id = stps.%(ShorId) AND stps.%(LocaId) = %(LocaIdValue)");
					query.append(" WHERE shor.%(RoutType) = '%(RoutTypeValue)'");
					query.append("   AND Date(shor.%(ForwardingDate)) = Date('%(ForwardingDateValue)')");

					final String[] result = stopDao.queryRaw(DaoHelper.formatQuery(query.toString(), queryParam)).getFirstResult();

					if (result != null) {
						shippingOrderTruckId = Long.parseLong(result[0]);
						shippingOrderStopTruckHubGuidLine = result[1];
					}
					if (shippingOrderStopTruckHubGuidLine == null || shippingOrderTruckId == null) {
						showDialogInfo(getString(R.string.shipping_order_stop_hub_service__poistion_or_stop_is_unidentified__msg));
					}
				}
			} catch (SQLException e) {
				LogHelper.setErrorMessage(this.getClass().getName(), e);
			} finally {
				setLoaderVisible(false);
			}
		}

		refreshData();
	}

	@UiThread
	public void populateView() {
		if (currentObject == null) {
			takeOverLoadButton.setEnabled(false);
			courierCarLoadingButton.setEnabled(false);
			courierCarUnloadingButton.setEnabled(false);
			truckServiceButton.setEnabled(false);

			showDialogInfo(getString(R.string.shipping_order_stop_hub_service__hub_no_data__msg));

			return;
		}

		courierCarLoadingButton.setEnabled(true);
		courierCarUnloadingButton.setEnabled(true);
		truckServiceButton.setEnabled(true);

		courierCarUnloadingButton.setBackground(getResources().getDrawable(lineUnloading != null && lineUnloading.isDone() ? R.drawable.button_menu_done : R.drawable.button_menu));
		courierCarUnloadingButton.setVisibility(lineUnloading != null ? View.VISIBLE : View.GONE);

		courierCarLoadingButton.setBackground(getResources().getDrawable(lineLoading != null && lineLoading.isDone() ? R.drawable.button_menu_done : R.drawable.button_menu));
		courierCarLoadingButton.setVisibility(lineLoading != null ? View.VISIBLE : View.GONE);

		ApplicationParameter param = appParametersHelper.getParameter(AppParametersHelper.TAKE_OVER_LOAD_AVAILABLE);
		takeOverLoadButton.setVisibility(param != null && "T".equalsIgnoreCase(param.getValue()) ? View.VISIBLE : View.GONE);
	}

	@Background
	public void refreshData() {
		lineUnloading = null;
		lineLoading = null;

		setLoaderVisible(true);

		try {
			if (currentObject != null) {
				QueryBuilder<ShippingOrderLine, Long> queryBuilder = shippingOrderLineDao.queryBuilder();
				queryBuilder.where().eq(ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME, currentObject.getGuid()).and().eq(ShippingOrderLine.SHOR_ID_FIELD_NAME, currentObject.getShorId()).and()
						.eq(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, "NOSN");
				queryBuilder.orderBy(ShippingOrderLine.DONE_FIELD_NAME, true);
				lineUnloading = queryBuilder.queryForFirst();

				queryBuilder = shippingOrderLineDao.queryBuilder();
				queryBuilder.where().eq(ShippingOrderLine.STPS_GUID_LOADING_FIELD_NAME, currentObject.getGuid()).and().eq(ShippingOrderLine.SHOR_ID_FIELD_NAME, currentObject.getShorId()).and()
						.eq(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, "NOSN");
				queryBuilder.orderBy(ShippingOrderLine.DONE_FIELD_NAME, true);
				lineLoading = queryBuilder.queryForFirst();
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		} finally {
			setLoaderVisible(false);
			populateView();
		}
	}

	private boolean checkTruck() {
		if (shippingOrderTruckId == null || shippingOrderStopTruckHubGuidLine == null) {
			showDialogInfo(getString(R.string.shipping_order_stop_hub_service__msg_no_shipping_order_line_data));

			return false;
		}

		return true;
	}

	private void goToCourierCarChargeService(boolean loading) {
		if (checkTruck()) {
			Intent intent = new Intent(getActivity(), ShippingOrderStopLinesAct_.class);
			intent.putExtra(ShippingOrderStopLinesAct.SHIPPING_ORDER_STOP_GUID__PARAM, shippingOrderStopCourierHubGuid);
			intent.putExtra(ShippingOrderStopLinesAct.LOADING__PARAM, loading);
			startActivityForResult(intent, loading ? RESULT_CODE__OPENING_TRUCK_LOAD : RESULT_CODE__OPENING_TRUCK_UNLOAD);
		}
	}
}
