package com.u4.mobile.transport.ui.shippingorders.hub;

import java.sql.SQLException;
import java.util.Locale;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.springframework.util.StringUtils;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.DaoManager;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.scanner.Scanner.Scanable;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.transport.model.ShippingOrderLine.SourceDocType;
import com.u4.mobile.transport.model.Stop;
import com.u4.mobile.transport.ui.incidents.IncidentAct;
import com.u4.mobile.transport.ui.incidents.IncidentAct_;
import com.u4.mobile.utils.LogHelper;

@EFragment(R.layout.shipping_order_stop_truck_plug_frag)
public class ShippingOrderStopTruckPlugFrag extends FragmentBase implements Scanable {

	public static int RESULT_CODE__OPEN_TRUCK = 1;
	public static int RESULT_CODE__CLOSE_TRUCK = 2;

	private ShippingOrderLine currentLineObject;

	private Stop currentStopObject;

	@ViewById(R.id.plug__invalid_button)
	protected Button invalidButton;

	@ViewById(R.id.plug__number_text)
	protected EditText plugNumber;

	@ViewById(R.id.plug__stop_text)
	protected EditText plugStop;

	@ViewById(R.id.plug__valid_button)
	protected Button validButton;

	@ViewById(R.id.plug__incident_info)
	protected TextView incidentInfo;

	@FragmentArg(ShippingOrderStopTruckPlugAct.PLUG_DIRECT_IS_OPEN__PARAM)
	boolean plugDirectIsOpenParam;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	Dao<ShippingOrderLine, Long> shippingOrderLineDao;

	@FragmentArg(ShippingOrderStopTruckPlugAct.SHIPPING_ORDER_STOP_TRUCK_HUB_GUID__PARAM)
	String shippingOrderStopTruckHubGuid;

	@OrmLiteDao(helper = DatabaseHelper.class, model = Stop.class)
	Dao<Stop, Long> stopDao;

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == IncidentAct.RESULT_NUM && resultCode == DialogInterface.BUTTON_POSITIVE) {
			try {
				if (currentLineObject != null && getLineData(currentLineObject.getGuid()) > 0) {
					if (plugDirectIsOpenParam) {
						currentLineObject.setDelivered(true);
						currentLineObject.setStopUnloading(currentStopObject);
						getActivity().setResult(RESULT_CODE__OPEN_TRUCK, getActivity().getIntent());
					} else {
						currentLineObject.setLoaded(true);
						currentLineObject.setStopLoading(currentStopObject);
						getActivity().setResult(RESULT_CODE__CLOSE_TRUCK, getActivity().getIntent());
					}
				}

				currentLineObject.setDone(true);
				currentLineObject.update();
			} catch (SQLException e) {
				LogHelper.setErrorMessage("PlugFrag.onActivityResult()", e);
			} finally {
				populateView();
			}
		}
	}

	@Override
	public void onBarcodeScan(String data) {
		if (plugDirectIsOpenParam) {
			getUnloadTruckLine(data);
		} else {
			getLoadTruckLine(data);
		}

		if (currentLineObject == null) {
			showDialogInfo(String.format(getString(R.string.shipping_order_stop_truck_plug__plug_is_unidentified__msg), data));
		} else if (currentLineObject.isDone()) {
			showDialogInfo(getString(R.string.shipping_order_stop_truck_plug__plug_is_veryfied__msg));
		}

		populateView();
	}

	@UiThread
	public void populateView() {
		boolean butonsEnabled = false;

		if (currentStopObject != null) {
			plugStop.setText(currentStopObject.getLocaLocationAddress());
		}

		if (currentLineObject != null) {
			butonsEnabled = !currentLineObject.isDone();
			plugNumber.setText(currentLineObject.getSourceDocNumber());
		}

		if (currentLineObject != null && StringUtils.hasText(currentLineObject.getComments())) {
			incidentInfo.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
			incidentInfo.setVisibility(View.VISIBLE);

			butonsEnabled = false;
		} else {
			incidentInfo.setVisibility(View.GONE);
		}

		invalidButton.setEnabled(butonsEnabled);
		validButton.setEnabled(butonsEnabled);

		if (butonsEnabled) {
			invalidButton.setBackground(getResources().getDrawable(R.drawable.button_menu));
			validButton.setBackground(getResources().getDrawable(R.drawable.button_menu_done));
		} else {
			invalidButton.setBackground(getResources().getDrawable(R.drawable.button_menu__disabled));
			validButton.setBackground(getResources().getDrawable(R.drawable.button_menu__disabled));
		}
	}

	@Click(R.id.plug__incident_info)
	void incidentInfoClicked() {

		if (currentLineObject != null) {
			IncidentAct_.intent(this).incidentLineId(currentLineObject.getId())
					.incidentContext(String.format(getString(R.string.incident__context_header__text, plugNumber.getText(), currentStopObject.getLocaLocationAddress())))
					.startForResult(IncidentAct.RESULT_NUM);
		}
	}

	@Click(R.id.plug__invalid_button)
	void invalidButtonClicked() {
		if (currentLineObject != null) {
			IncidentAct_.intent(this).incidentLineId(currentLineObject.getId())
					.incidentContext(String.format(getString(R.string.incident__context_header__text, plugNumber.getText(), currentStopObject.getLocaLocationAddress())))
					.startForResult(IncidentAct.RESULT_NUM);
		}
	}

	@AfterViews
	@Background
	void loadData() {
		try {
			if (shippingOrderStopTruckHubGuid != null) {
				currentStopObject = DaoManager.queryForGuid(shippingOrderStopTruckHubGuid, stopDao);
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage("ShippingOrderHeaderFrag.loadView", e);
		} finally {
			populateView();
		}
	}

	@Click(R.id.plug__valid_button)
	void validButtonClicked() {
		try {
			if (currentLineObject != null) {
				if (plugDirectIsOpenParam) {
					currentLineObject.setDelivered(true);
					currentLineObject.setStopUnloading(currentStopObject);
					getActivity().setResult(ShippingOrderStopHubServiceFrag.RESULT_CODE__OPENING_TRUCK_UNLOAD);
				} else {
					currentLineObject.setLoaded(true);
					currentLineObject.setStopLoading(currentStopObject);
					getActivity().setResult(ShippingOrderStopHubServiceFrag.RESULT_CODE__OPENING_TRUCK_LOAD);
				}

				currentLineObject.setDone(true);
				currentLineObject.update();

				getActivity().finish();
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage("PlugFrag.validButtonClicked()", e);
		}
	}

	private long getLineData(String uid) {

		try {
			currentLineObject = DaoManager.queryForGuid(uid, shippingOrderLineDao);

			if (currentLineObject != null) {
				return currentLineObject.getId();
			}

		} catch (SQLException e) {
			LogHelper.setErrorMessage(String.format("PlugFrag.getLine(String %s)", plugNumber), e);
		}

		return 0;
	}

	private void getLoadTruckLine(String plugNumber) {
		try {
			QueryBuilder<ShippingOrderLine, Long> query = shippingOrderLineDao.queryBuilder();

			if (StringUtils.hasText(plugNumber)) {
				query.where().raw(
						String.format("UPPER(%s) = '%s' AND %s = %s AND %s = '%s'", ShippingOrderLine.SOURCE_DOC_NUMBER_FIELD_NAME, plugNumber.toUpperCase(Locale.getDefault()),
								ShippingOrderLine.SHOR_ID_FIELD_NAME, currentStopObject.getShorId(), ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, SourceDocType.Plug.getValue()));
			} else {

				query.where().eq(ShippingOrderLine.STPS_GUID_LOADING_FIELD_NAME, shippingOrderStopTruckHubGuid).and().eq(ShippingOrderLine.SHOR_ID_FIELD_NAME, currentStopObject.getShorId()).and()
						.eq(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, SourceDocType.Plug.getValue());
			}

			currentLineObject = query.queryForFirst();
		} catch (SQLException e) {
			LogHelper.setErrorMessage(String.format("PlugFrag.getLine(String %s)", plugNumber), e);
		}
	}

	private void getUnloadTruckLine(String plugNumber) {
		try {
			QueryBuilder<ShippingOrderLine, Long> query = shippingOrderLineDao.queryBuilder();

			if (plugNumber != null) {
				query.where().raw(
						String.format("UPPER(%s) = '%s' AND %s = %s AND %s = '%s'", ShippingOrderLine.SOURCE_DOC_NUMBER_FIELD_NAME, plugNumber.toUpperCase(Locale.getDefault()),
								ShippingOrderLine.SHOR_ID_FIELD_NAME, currentStopObject.getShorId(), ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, SourceDocType.Plug.getValue()));
			} else {
				query.where().isNotNull(ShippingOrderLine.SOURCE_DOC_NUMBER_FIELD_NAME).and().eq(ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME, shippingOrderStopTruckHubGuid).and()
						.eq(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, SourceDocType.Plug.getValue());
			}

			currentLineObject = query.queryForFirst();
		} catch (SQLException e) {
			LogHelper.setErrorMessage(String.format("PlugFrag.getLine(String %s)", plugNumber), e);
		}
	}
}
