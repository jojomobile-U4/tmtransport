package com.u4.mobile.transport.ui.shippingorders.hub;

import java.sql.SQLException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.app.ApplicationEnums.SyncDirection;
import com.u4.mobile.base.helpers.NetworkHelper;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.DaoManager;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.ShippingOrder;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.transport.model.ShippingOrderLine.SourceDocType;
import com.u4.mobile.transport.model.Stop;
import com.u4.mobile.transport.services.RestHelper;
import com.u4.mobile.transport.ui.shippingorders.ShippingOrderStopLinesAct;
import com.u4.mobile.transport.ui.shippingorders.ShippingOrderStopLinesAct_;
import com.u4.mobile.transport.utils.SynchronizationHelper;
import com.u4.mobile.utils.LogHelper;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

@EFragment(R.layout.shipping_order_stop_truck_service_frag)
public class ShippingOrderStopTruckServiceFrag extends FragmentBase {

	public static int REQUEST_CODE__OPENING_TRUCK = 1;
	public static int REQUEST_CODE__TRUCK_UNLOADING = 2;
	public static int RESULT_CODE__TRUCK_OPPENING = 1;
	public static int RESULT_CODE__TRUCK_CLOSING = 2;
	public static int RESULT_CODE__OPENING_TRUCK_UNCHANGED = 0;

	private Stop currentObject;
	private ShippingOrderLine lineOpening;
	private ShippingOrderLine lineClosing;
	private ShippingOrderLine lineUnloading;
	private ShippingOrderLine lineLoading;

	@Bean
	SynchronizationHelper mSynchronizationHelper;

	@Bean
	RestHelper mRestHelper;

	@ViewById(R.id.shipping_order_stop_truck_service__truck_opening)
	Button truckOpeningButton;

	@ViewById(R.id.shipping_order_stop_truck_service__truck_closing)
	Button truckClosingButton;

	@ViewById(R.id.shipping_order_stop_truck_service__truck_unloading)
	Button truckUnloadingButton;

	@ViewById(R.id.shipping_order_stop_truck_service__truck_loading)
	Button truckLoadingButton;

	@ViewById(R.id.shipping_order_stop_truck_service__send_data)
	Button truckSendDataButton;

	@FragmentArg(ShippingOrderStopTruckServiceAct.SHIPPING_ORDER_STOP_TRUCK_HUB_GUID__PARAM)
	String shippingOrderStopTruckHubGuid;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrder.class)
	Dao<ShippingOrder, Long> shippingOrderDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = Stop.class)
	Dao<Stop, Long> stopDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	Dao<ShippingOrderLine, Long> shippingOrderLineDao;

	@Click(R.id.shipping_order_stop_truck_service__send_data)
	@Background
	public void buttonSendDataClick() {
		sendData();
	}

	@Click(R.id.shipping_order_stop_truck_service__truck_closing)
	@Background
	public void buttonTruckClosingClick() {
		goToTruckCheckPlugService(false);
	}

	@Click(R.id.shipping_order_stop_truck_service__truck_loading)
	@Background
	public void buttonTruckLoadingClick() {
		goToTruckChargeService(true);
	}

	@Click(R.id.shipping_order_stop_truck_service__truck_opening)
	@Background
	public void buttonTruckOpeningClick() {
		goToTruckCheckPlugService(true);
	}

	@Click(R.id.shipping_order_stop_truck_service__truck_unloading)
	@Background
	public void buttonTruckUnloadingClick() {
		goToTruckChargeService(false);
	}

	private void goToTruckChargeService(boolean loading) {
		Intent intent = new Intent(getActivity(), ShippingOrderStopLinesAct_.class);
		intent.putExtra(ShippingOrderStopLinesAct.SHIPPING_ORDER_STOP_GUID__PARAM, currentObject.getGuid());
		intent.putExtra(ShippingOrderStopLinesAct.LOADING__PARAM, loading);
		startActivityForResult(intent, REQUEST_CODE__TRUCK_UNLOADING);
	}

	private void goToTruckCheckPlugService(boolean opening) {
		Intent intent = new Intent(getActivity(), ShippingOrderStopTruckPlugAct_.class);
		intent.putExtra(ShippingOrderStopTruckPlugAct.SHIPPING_ORDER_STOP_TRUCK_HUB_GUID__PARAM, currentObject.getGuid());
		intent.putExtra(ShippingOrderStopTruckPlugAct.PLUG_DIRECT_IS_OPEN__PARAM, opening);
		startActivityForResult(intent, REQUEST_CODE__OPENING_TRUCK);
	}

	private void loadData() {
		lineOpening = null;
		lineClosing = null;
		lineUnloading = null;
		lineLoading = null;

		setLoaderVisible(true);

		try {
			if (shippingOrderStopTruckHubGuid != null) {
				currentObject = DaoManager.queryForGuid(shippingOrderStopTruckHubGuid, stopDao);

				if (currentObject != null) {
					QueryBuilder<ShippingOrderLine, Long> queryBuilder = shippingOrderLineDao.queryBuilder();
					queryBuilder.where().eq(ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME, currentObject.getGuid()).and().eq(ShippingOrderLine.SHOR_ID_FIELD_NAME, currentObject.getShorId()).and()
							.eq(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, SourceDocType.Plug.getValue());

					lineOpening = queryBuilder.queryForFirst();

					queryBuilder = shippingOrderLineDao.queryBuilder();
					queryBuilder.where().eq(ShippingOrderLine.STPS_GUID_LOADING_FIELD_NAME, currentObject.getGuid()).and().eq(ShippingOrderLine.SHOR_ID_FIELD_NAME, currentObject.getShorId()).and()
							.eq(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, SourceDocType.Plug.getValue());

					lineClosing = queryBuilder.queryForFirst();

					queryBuilder = shippingOrderLineDao.queryBuilder();
					queryBuilder.where().eq(ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME, currentObject.getGuid()).and().eq(ShippingOrderLine.SHOR_ID_FIELD_NAME, currentObject.getShorId()).and()
							.eq(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, "NOSN");
					queryBuilder.orderBy(ShippingOrderLine.DONE_FIELD_NAME, true);
					lineUnloading = queryBuilder.queryForFirst();

					queryBuilder = shippingOrderLineDao.queryBuilder();
					queryBuilder.where().eq(ShippingOrderLine.STPS_GUID_LOADING_FIELD_NAME, currentObject.getGuid()).and().eq(ShippingOrderLine.SHOR_ID_FIELD_NAME, currentObject.getShorId()).and()
							.eq(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, "NOSN");
					queryBuilder.orderBy(ShippingOrderLine.DONE_FIELD_NAME, true);
					lineLoading = queryBuilder.queryForFirst();
				}
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		} finally {
			setLoaderVisible(false);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == REQUEST_CODE__OPENING_TRUCK && (resultCode == RESULT_CODE__TRUCK_OPPENING || resultCode == RESULT_CODE__TRUCK_CLOSING)) {
			loadData();

			if (resultCode == RESULT_CODE__TRUCK_CLOSING) {
				if ((lineUnloading == null || (lineUnloading != null && lineUnloading.isDone())) && (lineLoading == null || (lineLoading != null && lineLoading.isDone()))
						&& (lineClosing != null && lineClosing.isDone())) {
					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setTitle(getString(R.string.shipping_order_stop_truck_service__truck_closing_done_question_title));
					builder.setMessage(getString(R.string.shipping_order_stop_truck_service__truck_closing_done_question));
					builder.setPositiveButton(getString(com.u4.mobile.R.string.yes), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							sendData();
						}
					});
					builder.setNegativeButton(getString(com.u4.mobile.R.string.no), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});

					AlertDialog alert = builder.create();
					alert.show();
				}
			}

			populateView();
		} else if (requestCode == REQUEST_CODE__TRUCK_UNLOADING) {
			loadData();

			if (lineUnloading != null && lineUnloading.isDone()) {
				if ((lineLoading == null || (lineLoading != null && lineLoading.isDone())) && (lineClosing == null || (lineClosing != null && !lineClosing.isDone()))) {
					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setTitle(getString(R.string.shipping_order_stop_truck_service__truck_unloading_done_question_title));
					builder.setMessage(getString(R.string.shipping_order_stop_truck_service__truck_unloading_done_question));
					builder.setPositiveButton(getString(com.u4.mobile.R.string.yes), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							goToTruckCheckPlugService(false);
						}
					});
					builder.setNegativeButton(getString(com.u4.mobile.R.string.no), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});

					AlertDialog alert = builder.create();
					alert.show();
				}
			}

			populateView();
		}
	}

	@AfterViews
	@Background
	public void onCreateView() {
		loadData();
		populateView();
	}

	@UiThread
	public void populateView() {
		if (currentObject == null) {
			truckOpeningButton.setEnabled(false);
			truckClosingButton.setEnabled(false);
			truckUnloadingButton.setEnabled(false);
			truckLoadingButton.setEnabled(false);
			truckSendDataButton.setEnabled(false);

			showDialogInfo(getString(R.string.shipping_order_stop_truck_service__truck_hub_no_data__msg));

			return;
		}

		truckOpeningButton.setBackground(getResources().getDrawable(lineOpening != null && lineOpening.isDone() ? R.drawable.button_menu_done : R.drawable.button_menu));
		truckClosingButton.setBackground(getResources().getDrawable(lineClosing != null && lineClosing.isDone() ? R.drawable.button_menu_done : R.drawable.button_menu));

		truckUnloadingButton.setBackground(getResources().getDrawable(lineUnloading != null && lineUnloading.isDone() ? R.drawable.button_menu_done : R.drawable.button_menu));
		truckUnloadingButton.setVisibility(lineUnloading != null ? View.VISIBLE : View.GONE);

		truckLoadingButton.setBackground(getResources().getDrawable(lineLoading != null && lineLoading.isDone() ? R.drawable.button_menu_done : R.drawable.button_menu));
		truckLoadingButton.setVisibility(lineLoading != null ? View.VISIBLE : View.GONE);
	}

	@Background
	public void sendData() {
		if (!NetworkHelper.checkInternet()) {
			showDialogInfo(com.u4.mobile.R.string.network__no_connection_to_internet);

			return;
		}

		try {
			setLoaderVisible(true, com.u4.mobile.R.string.adm_sync__sending_data__msg);

			if (mSynchronizationHelper.sendDataByShorId(currentObject.getShorId())) {
				showDialogInfo(com.u4.mobile.R.string.adm_sync__send_data_finish_ok__message);
			}
		} catch (Exception e) {
			showDialogInfo(NetworkHelper.translateMessage(e, SyncDirection.Upload));
		} finally {
			setLoaderVisible(false);
		}
	}

}
