package com.u4.mobile.transport.ui.shippingorders.hub;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import com.u4.mobile.base.ui.StandardActivity;
import com.u4.mobile.transport.R;

@EActivity
public class ShippingOrderStopTruckPlugAct extends StandardActivity {

	public static final String PLUG_DIRECT_IS_OPEN__PARAM = "plugDirectIsOpen";
	public static final int RESULT_NUM = 1;
	public static final String SHIPPING_ORDER_STOP_TRUCK_HUB_GUID__PARAM = "shippingOrderStopTruckHubGuid";

	@Extra(SHIPPING_ORDER_STOP_TRUCK_HUB_GUID__PARAM)
	String shippingOrderStopTruckHubGuid;

	@Extra(PLUG_DIRECT_IS_OPEN__PARAM)
	boolean plugDirtectIsOpen;

	@Override
	public void registerFragments() {
		addFragment(ShippingOrderStopTruckPlugFrag_.builder().plugDirectIsOpenParam(plugDirtectIsOpen).shippingOrderStopTruckHubGuid(shippingOrderStopTruckHubGuid).build());

		if (plugDirtectIsOpen) {
			setTitle(R.string.shipping_order_stop_truck_plug_act__direct_open__title);
		} else {
			setTitle(R.string.shipping_order_stop_truck_plug_act__direct_close__title);
		}
	}
}
