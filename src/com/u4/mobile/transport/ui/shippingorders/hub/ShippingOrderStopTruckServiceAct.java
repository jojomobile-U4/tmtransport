package com.u4.mobile.transport.ui.shippingorders.hub;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import com.u4.mobile.base.ui.StandardActivity;

@EActivity
public class ShippingOrderStopTruckServiceAct extends StandardActivity {

	public static final String SHIPPING_ORDER_STOP_TRUCK_HUB_GUID__PARAM = "shippingOrderStopTruckHubGuid";

	@Extra(SHIPPING_ORDER_STOP_TRUCK_HUB_GUID__PARAM)
	String shippingOrderStopTruckHubGuid;

	@Override
	public void registerFragments() {
		addFragment(ShippingOrderStopTruckServiceFrag_.builder().shippingOrderStopTruckHubGuid(shippingOrderStopTruckHubGuid).build());
	}
}
