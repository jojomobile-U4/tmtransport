package com.u4.mobile.transport.ui.shippingorders;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.OrmLiteDao;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.u4.mobile.R;
import com.u4.mobile.base.ui.ListAdapterBase;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.utils.LogHelper;

@EBean
public class ShippingOrderLinesListAdapter extends ListAdapterBase<ShippingOrderLine> {

	private String shorId;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	static Dao<ShippingOrderLine, Long> shippingOrderLineDao;

	public ShippingOrderLinesListAdapter() {
		super();
	}

	public void init(String shorId) {
		this.shorId = shorId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		super.getView(position, convertView, parent);
		
		ViewHolder holder = null;
		try {
			

			if (convertView == null) {
				final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.list_item_two_lines, null);
				holder = new ViewHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.populateFrom(getItem(position));
		} catch (final Exception e) {
			LogHelper.setErrorMessage("ShippingOrderLinesListAdapter.getView", e);
		}
		return convertView;
	}

	@Override
	public QueryBuilder<ShippingOrderLine, Long> prepareQuery() {

		QueryBuilder<ShippingOrderLine, Long> query = shippingOrderLineDao.queryBuilder();
		try {

			// TO DEBAG ;) = query.prepareStatementString()
			Where<ShippingOrderLine, Long> where = null;

			if (shorId != null) {
				where = query.where();
				where.eq(ShippingOrderLine.SHOR_ID_FIELD_NAME, shorId);
			}

			query.setWhere(where);
			query.offset(getListOffset() * getPageSize()).limit((long) getPageSize());
			query.orderBy(ShippingOrderLine.PRIO_CODE_FIELD_NAME, true);

		} catch (final Exception e) {
			LogHelper.setErrorMessage(ShippingOrderLinesListAdapter.class.getName(), e);
		}
		return query;
	}

}

class ViewHolder {

	TextView firstLineTextView;
	TextView secondLineTextView;

	ViewHolder(View row) {

		firstLineTextView = (TextView) row.findViewById(R.id.first_line__text_view);
		secondLineTextView = (TextView) row.findViewById(R.id.second_line__text_view);
	}

	void populateFrom(ShippingOrderLine item) {

		firstLineTextView.setText(item.getSourceDocNumber());
		secondLineTextView.setText(item.getGuid());
	}
}
