package com.u4.mobile.transport.ui.shippingorders;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.springframework.util.StringUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.DaoManager;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.ContractorAccounts;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.transport.model.Stop;
import com.u4.mobile.transport.ui.cashbox.CashDocumentGenerationAct;
import com.u4.mobile.transport.ui.cashbox.CashDocumentGenerationAct_;
import com.u4.mobile.transport.ui.cashbox.CashDocumentGenerationFrag;
import com.u4.mobile.utils.LogHelper;

@EFragment(R.layout.shipping_order_stop_client_service_frag)
public class ShippingOrderStopClientServiceFrag extends FragmentBase {

	private static final int CURRENT_PAYMENT_CASH_DOKUMENTS_GENERATION_CODE = 1;
	private static final int ARREARAGE_CONTRACTOR_ACCOUNTS_CASH_DOKUMENTS_GENERATION_CODE = 2;
	private static final int SHIPPING_ORDER_STOP_LINES_REALIZATION_CODE = 3;

	public static final String LOCATION_TYPE_CONTRACTOR = "CONTRACTOR";

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	static Dao<ShippingOrderLine, Long> shippingOrderLineDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = Stop.class)
	static Dao<Stop, Long> stopDao;

	private Stop currentObcject = null;

	private boolean isContractor = false;
	private ShippingOrderLine lineArrearage = null;
	private ShippingOrderLine lineCurrentPayment = null;
	private ShippingOrderLine lineUnloading = null;
	private ShippingOrderLine lineLoading = null;

	@ViewById(R.id.shipping_order_stop_client_service__arrearages)
	Button arrearagesButton;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ContractorAccounts.class)
	Dao<ContractorAccounts, Long> contractorAccountsDao;

	@ViewById(R.id.shipping_order_stop_client_service__loading)
	Button loadingButton;

	@ViewById(R.id.shipping_order_stop_client_service__payment_acceptance)
	Button paymentAcceptanceButton;

	@FragmentArg(ShippingOrderStopClientServiceAct.SHIPPING_ORDER_STOP_GUID__PARAM)
	String shippingOrderStopGuid;

	@ViewById(R.id.shipping_order_stop_client_service__unloading)
	Button unloadingButton;

	private void goToCourierCarChargeService(boolean loading) {
		Intent intent = new Intent(getActivity(), ShippingOrderStopLinesAct_.class);
		intent.putExtra(ShippingOrderStopLinesAct.SHIPPING_ORDER_STOP_GUID__PARAM, shippingOrderStopGuid);
		intent.putExtra(ShippingOrderStopLinesAct.LOADING__PARAM, loading);
		startActivityForResult(intent, SHIPPING_ORDER_STOP_LINES_REALIZATION_CODE);
	}

	@Click(R.id.shipping_order_stop_client_service__arrearages)
	@Background
	public void buttonArrearagesClick() {
		Long contractorId = -1L;

		if (isContractor) {
			contractorId = currentObcject.getLocaLocationSourceId();
		}

		Intent intent = new Intent(getActivity(), ShippingOrderStopContractorAccountsAct_.class);
		intent.putExtra(ShippingOrderStopContractorAccountsAct.CONTRACTOR_ID__PARAM, contractorId);
		intent.setAction(Intent.ACTION_INSERT);
		startActivityForResult(intent, ARREARAGE_CONTRACTOR_ACCOUNTS_CASH_DOKUMENTS_GENERATION_CODE);
	}

	@Click(R.id.shipping_order_stop_client_service__loading)
	@Background
	public void buttonLoadingClick() {
		goToCourierCarChargeService(true);
	}

	@Click(R.id.shipping_order_stop_client_service__payment_acceptance)
	@Background
	public void buttonPaymentAcceptanceClick() {
		if (lineCurrentPayment == null) {
			return;
		}

		List<String> accountsGuids = new ArrayList<String>();

		try {
			for (ContractorAccounts account : contractorAccountsDao.queryBuilder().where()
					.raw(String.format("UPPER(REPLACE(%s, '-', '')) IN (%s)", ContractorAccounts.SINV_GUID__FIELD_NAME, lineCurrentPayment.getSourceDocAdditionalData())).query()) {
				accountsGuids.add(account.getGuid());
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		}

		final Intent intent = new Intent(getActivity(), CashDocumentGenerationAct_.class);
		intent.putExtra(CashDocumentGenerationAct.SOURCE_ACCOUNTS_GUID_ARRAY__PARAM, accountsGuids.toArray(new String[accountsGuids.size()]));
		intent.putExtra(CashDocumentGenerationAct.CONTRACTOR_ID__PARAM, currentObcject.getLocaLocationSourceId());
		intent.setAction(Intent.ACTION_INSERT);

		startActivityForResult(intent, CURRENT_PAYMENT_CASH_DOKUMENTS_GENERATION_CODE);
	}

	@Click(R.id.shipping_order_stop_client_service__unloading)
	@Background
	public void buttonUnloadingClick() {
		goToCourierCarChargeService(false);
	}

	@Background
	public void loadData() {
		if (StringUtils.hasText(shippingOrderStopGuid)) {
			try {
				currentObcject = DaoManager.queryForGuid(shippingOrderStopGuid, stopDao);

				isContractor = currentObcject.getLocaLocationType().equalsIgnoreCase(LOCATION_TYPE_CONTRACTOR) && currentObcject.getLocaLocationSourceId() != null;

				QueryBuilder<ShippingOrderLine, Long> currentPaymentQueryBuilder = shippingOrderLineDao.queryBuilder();
				currentPaymentQueryBuilder.where().eq(ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME, shippingOrderStopGuid).and().eq(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, "PAYM").and()
						.eq(ShippingOrderLine.SOURCE_DOC_SUBTYPE_FIELD_NAME, "PAYM");
				currentPaymentQueryBuilder.orderBy(ShippingOrderLine.DELIVERED_FIELD_NAME, true);
				lineCurrentPayment = currentPaymentQueryBuilder.queryForFirst();
				
				QueryBuilder<ShippingOrderLine, Long> arrearageQueryBuilder = shippingOrderLineDao.queryBuilder();
				arrearageQueryBuilder.where().eq(ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME, shippingOrderStopGuid).and().eq(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, "PAYM").and()
						.eq(ShippingOrderLine.SOURCE_DOC_SUBTYPE_FIELD_NAME, "PAYLIA");
				arrearageQueryBuilder.orderBy(ShippingOrderLine.DELIVERED_FIELD_NAME, true);
				lineArrearage = arrearageQueryBuilder.queryForFirst();

				QueryBuilder<ShippingOrderLine, Long> unloadingQueryBuilder = shippingOrderLineDao.queryBuilder();
				unloadingQueryBuilder.where().eq(ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME, shippingOrderStopGuid).and().eq(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, "NOSN");
				unloadingQueryBuilder.orderBy(ShippingOrderLine.DELIVERED_FIELD_NAME, true);
				lineUnloading = unloadingQueryBuilder.queryForFirst();

				QueryBuilder<ShippingOrderLine, Long> loadingQueryBuilder = shippingOrderLineDao.queryBuilder();
				loadingQueryBuilder.where().eq(ShippingOrderLine.STPS_GUID_LOADING_FIELD_NAME, shippingOrderStopGuid).and().eq(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, "NOSN");
				loadingQueryBuilder.orderBy(ShippingOrderLine.DELIVERED_FIELD_NAME, true);
				lineLoading = loadingQueryBuilder.queryForFirst();
			} catch (final SQLException e) {
				LogHelper.setErrorMessage(ShippingOrderStopClientServiceFrag.class.getName(), e);
			}
		}

		populateView();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		
		Bundle data = intent != null ? intent.getExtras() : null;;

		switch (requestCode) {
		case CURRENT_PAYMENT_CASH_DOKUMENTS_GENERATION_CODE:
			if (resultCode == Activity.RESULT_OK) {
				try {
					if (data != null) {
						if (data.containsKey(CashDocumentGenerationFrag.CASH_DOCUMENT_GENERATION__CASH_IN_GUID_PARAM)) {
							lineCurrentPayment.setSourceDocGuid(data.getString(CashDocumentGenerationFrag.CASH_DOCUMENT_GENERATION__CASH_IN_GUID_PARAM));
						}
						
						if (data.containsKey(CashDocumentGenerationFrag.CASH_DOCUMENT_GENERATION__CASH_IN_BLANK_NUMBER_PARAM)) {
							lineCurrentPayment.setSourceDocNumber(data.getString(CashDocumentGenerationFrag.CASH_DOCUMENT_GENERATION__CASH_IN_BLANK_NUMBER_PARAM));
						}
					}
					
					lineCurrentPayment.setDelivered(true);
					lineCurrentPayment.update();
					
					loadData();
				} catch (SQLException e) {
					LogHelper.setErrorMessage(this.getClass().getName(), e);
				}
			}
			break;
		case ARREARAGE_CONTRACTOR_ACCOUNTS_CASH_DOKUMENTS_GENERATION_CODE:
			if (resultCode == Activity.RESULT_OK) {
				try {
					if (data != null) {
						if (data.containsKey(CashDocumentGenerationFrag.CASH_DOCUMENT_GENERATION__CASH_IN_GUID_PARAM)) {
							lineArrearage.setSourceDocGuid(data.getString(CashDocumentGenerationFrag.CASH_DOCUMENT_GENERATION__CASH_IN_GUID_PARAM));
						}
						
						if (data.containsKey(CashDocumentGenerationFrag.CASH_DOCUMENT_GENERATION__CASH_IN_BLANK_NUMBER_PARAM)) {
							lineArrearage.setSourceDocNumber(data.getString(CashDocumentGenerationFrag.CASH_DOCUMENT_GENERATION__CASH_IN_BLANK_NUMBER_PARAM));
						}
					}
					
					lineArrearage.setDelivered(true);
					lineArrearage.update();
					
					loadData();
				} catch (SQLException e) {
					LogHelper.setErrorMessage(this.getClass().getName(), e);
				}
			}
			break;
		case SHIPPING_ORDER_STOP_LINES_REALIZATION_CODE:
			loadData();
			break;
		}
	}

	@AfterViews
	public void onCreateView() {
		paymentAcceptanceButton.setEnabled(false);
		arrearagesButton.setEnabled(false);
		unloadingButton.setEnabled(false);
		loadingButton.setEnabled(false);

		loadData();
	}

	@UiThread
	public void populateView() {
		if (currentObcject == null) {
			return;
		}

		boolean paymentAcceptanceEnabled = isContractor && lineCurrentPayment != null && !lineCurrentPayment.isDelivered();

		// Set as done
		if (isContractor && lineCurrentPayment != null && lineCurrentPayment.isDelivered()) {
			paymentAcceptanceButton.setBackground(getResources().getDrawable(R.drawable.button_menu_done));
		}

		paymentAcceptanceButton.setEnabled(paymentAcceptanceEnabled);
		paymentAcceptanceButton.setVisibility(lineCurrentPayment != null ? View.VISIBLE : View.GONE);

		// Set as done
		if (isContractor && lineArrearage != null && lineArrearage.isDelivered()) {
			arrearagesButton.setBackground(getResources().getDrawable(R.drawable.button_menu_done));
		}

		arrearagesButton.setEnabled(isContractor && lineArrearage != null && !paymentAcceptanceEnabled);
		arrearagesButton.setVisibility(lineArrearage != null ? View.VISIBLE : View.GONE);

		// Set as done
		if (lineUnloading != null) {
			unloadingButton.setBackground(getResources().getDrawable(lineUnloading.isDelivered() ? R.drawable.button_menu_done: R.drawable.button_menu));
		} 

		unloadingButton.setVisibility(lineUnloading != null ? View.VISIBLE : View.GONE);

		// Set as done
		if (lineLoading != null) {
			loadingButton.setBackground(getResources().getDrawable(lineLoading.isLoaded() ? R.drawable.button_menu_done: R.drawable.button_menu));
		}

		loadingButton.setVisibility(lineLoading != null ? View.VISIBLE : View.GONE);

		boolean enable = (isContractor && !paymentAcceptanceEnabled) || !isContractor;
		unloadingButton.setEnabled(enable);
		loadingButton.setEnabled(enable);
	}
}
