package com.u4.mobile.transport.ui.shippingorders.transfer;

import java.sql.SQLException;
import java.util.Locale;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.squareup.otto.Subscribe;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;
import com.u4.mobile.base.bus.RefreshDataEvent;
import com.u4.mobile.base.bus.UpButtonPressedEvent;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.scanner.Scanner.Scanable;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.bus.FinishTransferingEvent;
import com.u4.mobile.transport.bus.UndoTransferingEvent;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.transport.model.ShippingOrderLine.TransferState;
import com.u4.mobile.utils.LogHelper;

@OptionsMenu(R.menu.shipping_order_lines_start_transfer_menu)
@EFragment(R.layout.shipping_order_line_start_transfer_frag)
public class ShippingOrderLineStartTransferFrag extends FragmentBase implements Scanable, OnKeyListener {
	@FragmentArg(ShippingOrderLineStartTransferAct.OSOB_ID__PARAM)
	Long mOsobId;

	@FragmentArg(ShippingOrderLineStartTransferAct.SHIPPING_ORDER_DESTINATION_ID__PARAM)
	Long mShippingOrderDestinationId;

	@FragmentArg(ShippingOrderLineStartTransferAct.SHIPPING_ORDER_SOURCE_ID__PARAM)
	Long mShippingOrderSourceId;

	@FragmentArg(ShippingOrderLineStartTransferAct.SHIPPING_ORDER_STOP_HUB_GUID__PARAM)
	String mShippingOrderStopHubGuid;

	@ViewById(R.id.shipping_order_line_start_tranfer__carrier)
	EditText mCarrierSymbol;

	@ViewById(R.id.shipping_order_line_start_tranfer__stop_header)
	TextView mStopHeader;

	@ViewById(R.id.shipping_order_line_start_tranfer__stop_address)
	TextView mStopAddress;

	@ViewById(R.id.shipping_order_line_start_tranfer__stop_info)
	LinearLayout mStopInfo;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	public Dao<ShippingOrderLine, Long> shippingOrderLineDao;

	private ShippingOrderLine mLineObject;
	private boolean mTransferingLineExists = false;

	private void checkTransfetStateBeforeLeave() {
		if (mTransferingLineExists) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(getString(R.string.alert_dialog__message__title));
			builder.setMessage(getString(R.string.shipping_order_line_start_transfer__before_go_back_msg));
			builder.setPositiveButton(getString(com.u4.mobile.R.string.yes), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					undoTransfering(true);
				}
			});
			builder.setNegativeButton(getString(com.u4.mobile.R.string.no), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			builder.create().show();
		} else {
			exit();
		}
	}

	@UiThread
	protected void exit() {
		getActivity().finish();
	}

	@Click(R.id.shipping_order_line_start_tranfer__continue_button)
	@Background
	public void buttonContinueClick() {
		if (mLineObject == null) {
			return;
		}

		try {
			setLoaderVisible(true);
			mLineObject.setTransferingState();
			ShippingOrderLine.setTransferingNonCarrierLines(mLineObject.getStopUnloading().getGuid());
			ShippingOrderStopLinesToTransferAct_.intent(getActivity()).mShippingOrderStopSourceGuid(mLineObject.getStopUnloading().getGuid()).mOsobId(mOsobId)
					.mShippingOrderDestinationId(mShippingOrderDestinationId).mShippingOrderSourceId(mShippingOrderSourceId).mShippingOrderStopHubGuid(mShippingOrderStopHubGuid).start();
		} catch (SQLException e) {
			LogHelper.setErrorMessage(getClass().getName(), e);
		} finally {
			setLoaderVisible(false);
		}
	}

	@Background
	public void loadData() {
		mTransferingLineExists = false;

		try {
			setLoaderVisible(true);
			mTransferingLineExists = shippingOrderLineDao.queryBuilder().where().eq(ShippingOrderLine.SHOR_ID_FIELD_NAME, mShippingOrderSourceId).and()
					.eq(ShippingOrderLine.TRANSFER_STATE__FIELD_NAME, TransferState.Transfering.getValue()).countOf() > 0;
		} catch (SQLException e) {
			LogHelper.setErrorMessage(getClass().getName(), e);
		} finally {
			setLoaderVisible(false);
			populateView();
		}
	}

	@Override
	public void onBarcodeScan(String data) {
		try {
			final ShippingOrderLine scannedLine = shippingOrderLineDao
					.queryBuilder()
					.where()
					.raw(String.format("UPPER(%s) = '%s' AND %s = %s AND %s != %s", ShippingOrderLine.SOURCE_DOC_NUMBER_FIELD_NAME, data.toUpperCase(Locale.getDefault()),
							ShippingOrderLine.SHOR_ID_FIELD_NAME, mShippingOrderSourceId, DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue())).queryForFirst();

			if (scannedLine == null) {
				showDialogInfo(String.format(getString(R.string.shipping_order_line_start_transfer__carrier_not_found_by_barcode_msg), data));

				return;
			}

			if (scannedLine.getTransferState() == TransferState.Transfered) {
				showDialogInfo(String.format(getString(R.string.shipping_order_stop_lines_to_transfer__carrier_already_transfered_msg), data));

				return;
			}

			mLineObject = scannedLine;
			populateView();

		} catch (SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		}
	}

	@AfterViews
	public void onCreateView() {
		getView().setFocusableInTouchMode(true);
		getView().setOnKeyListener(this);
		mLineObject = null;
		loadData();
	}

	@Subscribe
	public void onFinishTransfering(FinishTransferingEvent event) {
		mLineObject = null;
		getActivity().finish();
	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_UP && (keyCode == KeyEvent.KEYCODE_BACK)) {
			checkTransfetStateBeforeLeave();

			return true;
		}

		return false;
	}

	@Subscribe
	public void onRefreshData(RefreshDataEvent event) {
		mLineObject = null;
		loadData();
	}

	@Subscribe
	public void onUndoTransfering(UndoTransferingEvent event) {
		mLineObject = null;
		loadData();
	}

	@Subscribe
	public void onUpButtonPressed(UpButtonPressedEvent event) {
		if (getActivity().hashCode() == event.getActivityId()) {
			checkTransfetStateBeforeLeave();
		}
	}

	@OptionsItem(R.id.shipping_order_line_start_transfer_menu__accept_transfer)
	public boolean optionAcceptTransferingSelected() {
		if (!mTransferingLineExists) {
			showDialogInfo(R.string.shipping_order_line_start_transfer_menu__accept_transfer_no_data_msg);

			return true;
		}

		ShippingOrderLineEndTransferAct_.intent(getActivity()).mOsobId(mOsobId).mShippingOrderDestinationId(mShippingOrderDestinationId).mShippingOrderSourceId(mShippingOrderSourceId)
				.mShippingOrderStopHubGuid(mShippingOrderStopHubGuid).start();

		return true;
	}

	@OptionsItem(R.id.shipping_order_line_start_transfer_menu__show_stops_to_transfer)
	public boolean optionShowStopsToTransferSelected() {
		if (mShippingOrderSourceId == null) {
			return false;
		}

		ShippingOrderStopsToTransferAct_.intent(getActivity()).mShippingOrderSourceId(mShippingOrderSourceId).start();

		return true;
	}

	@OptionsItem(R.id.shipping_order_line_start_transfer_menu__undo_transfer)
	public boolean optionUndoTransferingSelected() {
		if (!mTransferingLineExists) {
			showDialogInfo(R.string.shipping_order_line_start_transfer_menu__undo_transfer_no_data_msg);

			return true;
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(getString(R.string.alert_dialog__message__title));
		builder.setMessage(getString(R.string.shipping_order_line_start_transfer_menu__undo_transfer_question));
		builder.setPositiveButton(getString(com.u4.mobile.R.string.yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				undoTransfering(false);
			}
		});
		builder.setNegativeButton(getString(com.u4.mobile.R.string.no), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		builder.create().show();

		return true;
	}

	@UiThread
	public void populateView() {
		if (mLineObject == null) {
			mCarrierSymbol.setText("");
			mStopHeader.setText("");
			mStopAddress.setText("");
			mStopInfo.setVisibility(View.GONE);

			return;
		}

		mCarrierSymbol.setText(mLineObject.getSourceDocNumber());

		if (mLineObject.getStopUnloading() != null) {
			mStopInfo.setVisibility(View.VISIBLE);
			mStopHeader.setText(mLineObject.getStopUnloading().getLocaLocationName());
			mStopAddress.setText(mLineObject.getStopUnloading().getLocaLocationAddress());
		} else {
			mStopInfo.setVisibility(View.GONE);
		}
	}

	@Background
	public void undoTransfering(boolean exitAfterUndo) {
		try {
			setLoaderVisible(true, R.string.shipping_order_line_start_transfer_menu__undo_transfer_in_progress_msg);
			ShippingOrderLine.undoTransferingByShorId(mShippingOrderSourceId);
			mLineObject = null;

			if (exitAfterUndo) {
				exit();
			} else {
				loadData();
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage(getClass().getName(), e);
		} finally {
			setLoaderVisible(false);
		}
	}
}
