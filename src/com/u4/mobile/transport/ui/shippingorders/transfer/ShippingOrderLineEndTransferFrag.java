package com.u4.mobile.transport.ui.shippingorders.transfer;

import java.sql.SQLException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.squareup.otto.Subscribe;
import com.u4.mobile.base.bus.UpButtonPressedEvent;
import com.u4.mobile.base.ui.AuthorizationDialog.OnAuthorizeListener;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.SessionInfo;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.bus.FinishTransferingEvent;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.transport.model.ShippingOrderLine.SourceDocType;
import com.u4.mobile.transport.model.ShippingOrderLine.TransferState;
import com.u4.mobile.transport.model.Stop;
import com.u4.mobile.transport.ui.AuthorizationDialog;
import com.u4.mobile.transport.ui.AuthorizationDialog_;
import com.u4.mobile.utils.LogHelper;

@OptionsMenu(R.menu.shipping_order_lines_end_transfer_menu)
@EFragment(R.layout.shipping_order_line_end_transfer_frag)
public class ShippingOrderLineEndTransferFrag extends FragmentBase implements OnKeyListener {
	private long mStopsCount = 0;
	private long mCarriersCount = 0;
	private long mOthersCount = 0;

	@FragmentArg(ShippingOrderLineEndTransferAct.OSOB_ID__PARAM)
	Long mOsobId;

	@FragmentArg(ShippingOrderLineEndTransferAct.SHIPPING_ORDER_DESTINATION_ID__PARAM)
	Long mShippingOrderDestinationId;

	@FragmentArg(ShippingOrderLineEndTransferAct.SHIPPING_ORDER_SOURCE_ID__PARAM)
	Long mShippingOrderSourceId;

	@FragmentArg(ShippingOrderLineEndTransferAct.SHIPPING_ORDER_STOP_HUB_GUID__PARAM)
	String mShippingOrderStopHubGuid;

	@ViewById(R.id.shipping_order_line_end_tranfer__stops_count)
	TextView mStopsCountTextView;

	@ViewById(R.id.shipping_order_line_end_tranfer__carries_count)
	TextView mCarriersCountTextView;

	@ViewById(R.id.shipping_order_line_end_tranfer__others_count)
	TextView mOthersCountTextView;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	public Dao<ShippingOrderLine, Long> mShippingOrderLineDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = Stop.class)
	public Dao<Stop, Long> mStopDao;

	private void checkTransfetStateBeforeLeave() {
		if (mStopsCount > 0) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(getString(R.string.alert_dialog__message__title));
			builder.setMessage(getString(R.string.shipping_order_line_end_transfer__before_go_back_msg));
			builder.setPositiveButton(getString(com.u4.mobile.R.string.yes), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					undoTransfering();
				}
			});
			builder.setNegativeButton(getString(com.u4.mobile.R.string.no), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			builder.create().show();
		} else {
			getActivity().finish();
		}
	}

	@UiThread
	public void afterConfirmTransfering() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(getString(R.string.alert_dialog__message__title));
		builder.setMessage(getString(R.string.shipping_order_line_end_transfer__confirm_transfering_success));
		builder.setPositiveButton(getString(com.u4.mobile.R.string.ok), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				finishTransfering();
			}
		});

		builder.create().show();
	}

	@Click(R.id.shipping_order_line_end_tranfer__button_authorize)
	public void buttonAuthorizeClick() {
		final AuthorizationDialog dialog = AuthorizationDialog_.builder().build();
		dialog.setOnAuthorizeListener(new OnAuthorizeListener() {

			@Override
			public void onAuthorize(SessionInfo sessionInfo) {
				if (sessionInfo != null && sessionInfo.getOsobId() > 0) {
					if (mOsobId.equals(sessionInfo.getOsobId())) {
						confirmTransfering();
					} else {
						showDialogInfo(R.string.shipping_order_line_start_transfer_menu__authorize_osob_not_eqals_msg);
					}
				}
			}
		});

		dialog.show(getFragmentManager(), "authorizationDialog");
	}

	@Click(R.id.shipping_order_line_end_tranfer__button_cancel)
	public void buttonCancelClick() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(getString(R.string.alert_dialog__message__title));
		builder.setMessage(getString(R.string.shipping_order_line_start_transfer_menu__undo_transfer_question));
		builder.setPositiveButton(getString(com.u4.mobile.R.string.yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				undoTransfering();
			}
		});
		builder.setNegativeButton(getString(com.u4.mobile.R.string.no), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		builder.create().show();
	}

	@Background
	public void confirmTransfering() {
		try {
			setLoaderVisible(true, R.string.shipping_order_line_start_transfer_menu__confirm_transfer_in_progress_msg);
			ShippingOrderLine.confirmTransfering(mShippingOrderSourceId, mShippingOrderDestinationId);
			afterConfirmTransfering();
		} catch (SQLException e) {
			LogHelper.setErrorMessage(getClass().getName(), e);
			showDialogInfo(R.string.shipping_order_line_end_transfer__confirm_transfering_failed);
		} finally {
			setLoaderVisible(false);
		}
	}

	@UiThread
	public void finishTransfering() {
		getActivity().finish();
		mDataBus.post(new FinishTransferingEvent());
	}

	@Background
	public void loadData() {
		try {
			setLoaderVisible(true);
			mCarriersCount = mShippingOrderLineDao.queryBuilder().where().eq(ShippingOrderLine.SHOR_ID_FIELD_NAME, mShippingOrderSourceId).and()
					.eq(ShippingOrderLine.TRANSFER_STATE__FIELD_NAME, TransferState.Transfering.getValue()).and().eq(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, SourceDocType.Carrier.getValue())
					.countOf();

			mOthersCount = mShippingOrderLineDao.queryBuilder().where().eq(ShippingOrderLine.SHOR_ID_FIELD_NAME, mShippingOrderSourceId).and()
					.eq(ShippingOrderLine.TRANSFER_STATE__FIELD_NAME, TransferState.Transfering.getValue()).and().ne(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, SourceDocType.Carrier.getValue())
					.countOf();

			mStopsCount = mStopDao.queryBuilder().where().eq(Stop.SHOR_ID_FIELD_NAME, mShippingOrderSourceId).and().eq(Stop.TRANSFER_STATE__FIELD_NAME, TransferState.Transfering.getValue()).countOf();
		} catch (SQLException e) {
			LogHelper.setErrorMessage(getClass().getName(), e);
		} finally {
			setLoaderVisible(false);
			populateView();
		}
	}

	@AfterViews
	public void onCreateView() {
		loadData();
	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_UP && (keyCode == KeyEvent.KEYCODE_BACK)) {
			checkTransfetStateBeforeLeave();

			return true;
		}

		return false;
	}

	@Subscribe
	public void onUpButtonPressed(UpButtonPressedEvent event) {
		if (getActivity().hashCode() == event.getActivityId()) {
			checkTransfetStateBeforeLeave();
		}
	}

	@OptionsItem(R.id.shipping_order_line_end_transfer_menu__show_all_lines_to_transfer)
	public boolean optionShowAllLinesTotransferSelected() {
		ShippingOrderStopLinesTransferingAct_.intent(getActivity()).mShippingOrderSourceId(mShippingOrderSourceId).start();

		return true;
	}

	@UiThread
	public void populateView() {
		mStopsCountTextView.setText(String.format("%s", mStopsCount));
		mCarriersCountTextView.setText(String.format("%s", mCarriersCount));
		mOthersCountTextView.setText(String.format("%s", mOthersCount));
	}

	@Background
	public void undoTransfering() {
		try {
			setLoaderVisible(true, R.string.shipping_order_line_start_transfer_menu__undo_transfer_in_progress_msg);
			ShippingOrderLine.undoTransferingByShorId(mShippingOrderSourceId);
			finishTransfering();
		} catch (SQLException e) {
			LogHelper.setErrorMessage(getClass().getName(), e);
		} finally {
			setLoaderVisible(false);
		}
	}
}
