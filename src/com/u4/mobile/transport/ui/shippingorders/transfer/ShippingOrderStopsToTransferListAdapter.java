package com.u4.mobile.transport.ui.shippingorders.transfer;

import java.sql.SQLException;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.OrmLiteDao;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.u4.mobile.base.ui.ListAdapterBase;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.ShippingOrderLine.TransferState;
import com.u4.mobile.transport.model.Stop;
import com.u4.mobile.utils.LogHelper;

@EBean
public class ShippingOrderStopsToTransferListAdapter extends ListAdapterBase<Stop> {

	private Long shorId;

	@OrmLiteDao(helper = DatabaseHelper.class, model = Stop.class)
	static Dao<Stop, Long> stopsDao;

	public ShippingOrderStopsToTransferListAdapter() {
		super();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder = null;

		try {
			if (convertView == null) {
				final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.shipping_order_stops_to_transfer_list_view_item, parent, false);

				holder = new ViewHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.populateFrom(getItem(position));
		} catch (final Exception e) {
			e.printStackTrace();
		}

		return convertView;
	}

	public void init(Long shorId) {
		this.shorId = shorId;
	}

	@Override
	public QueryBuilder<Stop, Long> prepareQuery() {

		QueryBuilder<Stop, Long> query = stopsDao.queryBuilder();

		try {
			Where<Stop, Long> where = null;
			where = query.where();
			where.eq(Stop.SHOR_ID_FIELD_NAME, shorId != null ? shorId : -1);
			where.and().ne(Stop.LOCA_LOCATION_TYPE_FIELD_NAME, "HUB");
			where.and().ne(Stop.TRANSFER_STATE__FIELD_NAME, TransferState.Transfered.getValue());
			query.setWhere(where);
			query.offset(getListOffset() * getPageSize()).limit((long) getPageSize());
			query.orderBy(Stop.STOP_SEQUENCE_FIELD_NAME, true);
		} catch (SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		}

		return query;
	}

	class ViewHolder {
		TextView primaryLineTextView;
		TextView secondaryLineTextView;
		CheckBox checkbox;

		ViewHolder(View row) {
			primaryLineTextView = (TextView) row.findViewById(R.id.shipping_order_stops_to_transfer__primary_line);
			secondaryLineTextView = (TextView) row.findViewById(R.id.shipping_order_stops_to_transfer__secondary_line);
			checkbox = (CheckBox) row.findViewById(R.id.shipping_order_stops_to_transfer__checkbox);
		}

		void populateFrom(Stop item) {
			if (isSelecting()) {
				checkbox.setVisibility(View.VISIBLE);
				checkbox.setChecked(getCheckedItems().contains(item));
			} else {
				checkbox.setVisibility(View.GONE);
			}

			highlightText(primaryLineTextView, item.getLocaLocationName());
			highlightText(secondaryLineTextView, item.getLocaLocationAddress());
		}
	}

}
