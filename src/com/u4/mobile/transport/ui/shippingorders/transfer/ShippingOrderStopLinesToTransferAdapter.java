package com.u4.mobile.transport.ui.shippingorders.transfer;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.OrmLiteDao;
import org.springframework.util.StringUtils;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.base.ui.ListAdapterBase;
import com.u4.mobile.model.DaoHelper;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.transport.model.ShippingOrderLine.TransferState;
import com.u4.mobile.transport.model.Stop;
import com.u4.mobile.transport.ui.shippingorders.ShippingOrderStopLineDetailsDialog_;
import com.u4.mobile.utils.ControlsHelper;

@EBean
public class ShippingOrderStopLinesToTransferAdapter extends ListAdapterBase<ShippingOrderLine> {
	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	protected static Dao<ShippingOrderLine, Long> shippingOrderLineDao;

	private String mShippingOrderStopSourceGuid;

	public ShippingOrderStopLinesToTransferAdapter() {
		super();
	}

	@Override
	protected List<ShippingOrderLine> getData() throws SQLException {
		HashMap<String, Object> queryParam = new HashMap<String, Object>();
		queryParam.put("ShippingOrderLine", ShippingOrderLine.NAME);
		queryParam.put("SourceDocNumber", ShippingOrderLine.SOURCE_DOC_NUMBER_FIELD_NAME);
		queryParam.put("SourceDocId", ShippingOrderLine.SOURCE_DOC_ID_FIELD_NAME);
		queryParam.put("SourceDocType", ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME);
		queryParam.put("Delivered", ShippingOrderLine.DELIVERED_FIELD_NAME);
		queryParam.put("Loaded", ShippingOrderLine.LOADED_FIELD_NAME);
		queryParam.put("Canceled", ShippingOrderLine.CANCELED_FIELD_NAME);
		queryParam.put("TransferState", ShippingOrderLine.TRANSFER_STATE__FIELD_NAME);
		queryParam.put("ShoiGuid", ShippingOrderLine.SHOI_GUID__FIELD_NAME);
		queryParam.put("Stop", Stop.NAME);
		queryParam.put("StpsGuidUnloading", ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME);
		queryParam.put("StpsGuidLoading", ShippingOrderLine.STPS_GUID_LOADING_FIELD_NAME);
		queryParam.put("StpsGuidValue", mShippingOrderStopSourceGuid);

		StringBuilder query = new StringBuilder();
		query.append("SELECT shoi.Id, shoi.Guid, shoi.%(SourceDocNumber), shoi_parent.%(SourceDocNumber)");
		query.append(", shoi.%(Delivered), shoi.%(Loaded), shoi.%(Canceled), shoi.%(TransferState)");
		query.append("  FROM %(ShippingOrderLine) shoi");
		query.append(" INNER JOIN %(Stop) stps_u ON stps_u.Guid = shoi.%(StpsGuidUnloading)");
		query.append("  LEFT JOIN %(ShippingOrderLine) shoi_parent ON shoi_parent.Guid = shoi.%(ShoiGuid) AND shoi.%(SourceDocType) = 'NOSN'");
		query.append(" WHERE (shoi.%(StpsGuidUnloading) = '%(StpsGuidValue)' OR shoi.%(StpsGuidLoading) = '%(StpsGuidValue)')");
		query.append(" ORDER BY shoi.%(TransferState), shoi_parent.%(SourceDocNumber) DESC, shoi.%(SourceDocNumber)");

		final GenericRawResults<ShippingOrderLine> data = shippingOrderLineDao.queryRaw(DaoHelper.formatQuery(query.toString(), queryParam), new RawRowMapper<ShippingOrderLine>() {

			@Override
			public ShippingOrderLine mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
				ShippingOrderLine result = new ShippingOrderLine();
				result.setId(Long.parseLong(resultColumns[0]));
				result.setGuid(resultColumns[1]);
				result.setSourceDocNumber(resultColumns[2]);
				result.setPalletSymbol(resultColumns[3]);
				result.setDelivered(Integer.parseInt((resultColumns[4])) == 1);
				result.setLoaded(Integer.parseInt((resultColumns[5])) == 1);
				result.setCanceled(Integer.parseInt((resultColumns[6])) == 1);
				result.setTransferStateValue(Integer.parseInt((resultColumns[7])));

				return result;
			}
		});

		return data.getResults();
	}

	public int getInidicatorBackground(ShippingOrderLine shippingOrderLine) {
		if (shippingOrderLine.getTransferState() == TransferState.Transfering) {
			return R.drawable.list_item_indicator_green;
		}

		return R.drawable.list_item_indicator_red;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder = null;

		try {
			if (convertView == null) {
				final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.shipping_order_stop_lines_to_transfer_list_view_item, parent, false);

				holder = new ViewHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.populateFrom(getItem(position));
		} catch (final Exception e) {
			e.printStackTrace();
		}

		return convertView;
	}

	public void init(FragmentBase fragmentBase, String shippingOrderStopSourceGuid) {
		mShippingOrderStopSourceGuid = shippingOrderStopSourceGuid;

		super.init(fragmentBase, 0L);
	}

	@Override
	public QueryBuilder<ShippingOrderLine, Long> prepareQuery() {
		return null;
	}

	class ViewHolder {
		TextView mIndicator;
		TextView mPrimaryLineTextView;
		TextView mSecondaryLineTextView;
		Button mDetailsButton;

		ViewHolder(View row) {
			mPrimaryLineTextView = (TextView) row.findViewById(R.id.shipping_order_stop_lines_to_transfer__primary_line);
			mSecondaryLineTextView = (TextView) row.findViewById(R.id.shipping_order_stop_lines_to_transfer__secondary_line);
			mIndicator = (TextView) row.findViewById(R.id.shipping_order_stop_lines_to_transfer__indicator);
			mDetailsButton = (Button) row.findViewById(R.id.shipping_order_stop_lines_to_transfer__details_button);
		}

		void populateFrom(final ShippingOrderLine shippingOrderLine) {
			mIndicator.setVisibility(View.VISIBLE);
			mIndicator.setBackgroundResource(getInidicatorBackground(shippingOrderLine));

			highlightText(mPrimaryLineTextView, shippingOrderLine.getSourceDocNumber());

			if (StringUtils.hasText(shippingOrderLine.getPalletSymbol())) {
				SpannableStringBuilder spannable = new SpannableStringBuilder(String.format("%s ", getContext().getString(R.string.shipping_order_stop_lines__pallet)));
				spannable.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.light_gray)), 0, spannable.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);

				SpannableStringBuilder spannableText = ControlsHelper.highlightText(shippingOrderLine.getPalletSymbol(), getHighlightText());
				spannableText.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.white)), 0, spannableText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
				spannableText.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, spannableText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
				spannable.append(spannableText);

				mSecondaryLineTextView.setText(spannable, BufferType.SPANNABLE);
			} else {
				mSecondaryLineTextView.setText("");
			}

			mDetailsButton.setOnClickListener(new OnClickListener() {
				Long mShippingOrderLineId = shippingOrderLine.getId();

				@Override
				public void onClick(View v) {
					ShippingOrderStopLineDetailsDialog_.builder().mShippingOrderLineId(mShippingOrderLineId).build().show(getFragmentBase().getFragmentManager(), "ShippingOrderStopLineDetailsDialog");
				}
			});
		}
	}
}
