package com.u4.mobile.transport.ui.shippingorders.transfer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import android.widget.LinearLayout;
import android.widget.ListView;

import com.u4.mobile.base.ui.ListFragmentBase;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.Stop;

@EFragment(R.layout.lista_view_two_lines)
public class ShippingOrderStopsToTransferFrag extends ListFragmentBase<Stop> {

	@FragmentArg(ShippingOrderStopsToTransferAct.SHIPPING_ORDER_SOURCE_ID__PARAM)
	Long mShippingOrderSourceId;

	@ViewById(R.id.list__list_View)
	protected ListView listView;

	@Bean
	ShippingOrderStopsToTransferListAdapter adapter;

	@ViewById(R.id.list_view_empty_view)
	LinearLayout listViewEmptyView;

	@AfterViews
	void setupView() {
		listView.setEmptyView(listViewEmptyView);
		adapter.init(mShippingOrderSourceId);

		super.adapter = adapter;
		super.listView = listView;
		super.getList();
	}
}
