package com.u4.mobile.transport.ui.shippingorders.transfer;

import java.sql.SQLException;
import java.util.Locale;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.squareup.otto.Subscribe;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;
import com.u4.mobile.base.bus.RefreshDataEvent;
import com.u4.mobile.base.bus.UpButtonPressedEvent;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.DaoManager;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.scanner.Scanner.Scanable;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.bus.UndoTransferingEvent;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.transport.model.ShippingOrderLine.TransferState;
import com.u4.mobile.transport.model.Stop;
import com.u4.mobile.utils.LogHelper;

@OptionsMenu(R.menu.shipping_order_stop_lines_to_transfer_menu)
@EFragment(R.layout.shipping_order_stop_lines_to_transfer_frag)
public class ShippingOrderStopLinesToTransferFrag extends FragmentBase implements Scanable, OnKeyListener {

	@FragmentArg(ShippingOrderStopLinesToTransferAct.SHIPPING_ORDER_STOP_SOURCE_GUID__PARAM)
	String mShippingOrderStopSourceGuid;

	@FragmentArg(ShippingOrderStopLinesToTransferAct.OSOB_ID__PARAM)
	Long mOsobId;

	@FragmentArg(ShippingOrderStopLinesToTransferAct.SHIPPING_ORDER_DESTINATION_ID__PARAM)
	Long mShippingOrderDestinationId;

	@FragmentArg(ShippingOrderStopLinesToTransferAct.SHIPPING_ORDER_SOURCE_ID__PARAM)
	Long mShippingOrderSourceId;

	@FragmentArg(ShippingOrderStopLinesToTransferAct.SHIPPING_ORDER_STOP_HUB_GUID__PARAM)
	String mShippingOrderStopHubGuid;

	@ViewById(R.id.shipping_order_stop_lines_to_tranfer__list)
	ListView listView;

	@ViewById(R.id.shipping_order_stop_lines_to_tranfer__stop_header)
	TextView mStopHeader;

	@ViewById(R.id.shipping_order_stop_lines_to_tranfer__stop_address)
	TextView mStopAddress;

	@ViewById(R.id.list_view_empty_view)
	LinearLayout listViewEmptyView;

	@OrmLiteDao(helper = DatabaseHelper.class, model = Stop.class)
	public Dao<Stop, Long> stopDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	public Dao<ShippingOrderLine, Long> shippingOrderLineDao;

	@Bean
	ShippingOrderStopLinesToTransferAdapter adapter;

	private Stop mStopObject;

	private void checkTransfetStateBeforeLeave() {
		if (!adapter.isEmpty() && adapter.getItem(0) != null) {
			if (adapter.getItem(0).getTransferState() == TransferState.NotTransfered) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle(getString(R.string.alert_dialog__message__title));
				builder.setMessage(getString(R.string.shipping_order_stop_lines_to_transfer__before_go_back_msg));
				builder.setPositiveButton(getString(com.u4.mobile.R.string.yes), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						undoTransfering();
					}
				});
				builder.setNegativeButton(getString(com.u4.mobile.R.string.no), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});

				builder.create().show();
			}
		}
	}

	@UiThread
	public void afterRefreshData() {
		listView.invalidateViews();
		checkTransferState();
	}

	@UiThread
	public void afterUndoTransfering() {
		getActivity().finish();
		mDataBus.post(new UndoTransferingEvent());
	}

	@UiThread
	public void checkTransferState() {
		if (!adapter.isEmpty() && adapter.getItem(0) != null) {
			if (adapter.getItem(0).getTransferState() == TransferState.Transfering) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle(getString(R.string.shipping_order_stop_lines_to_transfer__finish_dialog_title));
				builder.setMessage(getString(R.string.shipping_order_stop_lines_to_transfer__finish_dialog_message));
				builder.setPositiveButton(getString(com.u4.mobile.R.string.finish), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Intent intent = ShippingOrderLineEndTransferAct_.intent(getActivity()).mOsobId(mOsobId).mShippingOrderDestinationId(mShippingOrderDestinationId)
								.mShippingOrderSourceId(mShippingOrderSourceId).mShippingOrderStopHubGuid(mShippingOrderStopHubGuid).get();
						getActivity().finish();
						mDataBus.post(new RefreshDataEvent());
						startActivity(intent);
					}
				});
				builder.setNeutralButton(getString(R.string.shipping_order_stop_lines_to_transfer__finish_dialog_button_next), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						getActivity().finish();
						mDataBus.post(new RefreshDataEvent());
					}
				});
				builder.setNegativeButton(getString(com.u4.mobile.R.string.cancel), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});

				builder.create().show();
			}
		}
	}

	@Background
	public void loadData() {
		try {
			mStopObject = DaoManager.queryForGuid(mShippingOrderStopSourceGuid, stopDao);
			adapter.reloadData(true, 0L);
		} catch (SQLException e) {
			LogHelper.setErrorMessage(getClass().getName(), e);
		} finally {
			populateView();
		}
	}

	@Override
	public void onBarcodeScan(String data) {
		try {
			final ShippingOrderLine scannedLine = shippingOrderLineDao
					.queryBuilder()
					.where()
					.raw(String.format("UPPER(%s) = '%s' AND %s = '%s' AND %s != %s", ShippingOrderLine.SOURCE_DOC_NUMBER_FIELD_NAME, data.toUpperCase(Locale.getDefault()),
							ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME, mShippingOrderStopSourceGuid, DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue()))
					.queryForFirst();

			if (scannedLine == null) {
				showDialogInfo(String.format(getString(R.string.shipping_order_stop_lines_to_transfer__carrier_not_found_by_barcode_msg), data));

				return;
			}

			if (scannedLine.getTransferState() == TransferState.Transfering) {
				showDialogInfo(String.format(getString(R.string.shipping_order_stop_lines_to_transfer__carrier_already_confirmed_msg), data));

				return;
			}

			if (scannedLine.getTransferState() == TransferState.Transfered) {
				showDialogInfo(String.format(getString(R.string.shipping_order_stop_lines_to_transfer__carrier_already_transfered_msg), data));

				return;
			}

			scannedLine.setTransferingState();
			refreshData();
		} catch (SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		}
	}

	@AfterViews
	public void onCreateView() {
		adapter.init(this, mShippingOrderStopSourceGuid);
		listView.setAdapter(adapter);
		listView.setEmptyView(listViewEmptyView);
		getView().setFocusableInTouchMode(true);
		getView().setOnKeyListener(this);

		loadData();
	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_UP && (keyCode == KeyEvent.KEYCODE_BACK)) {
			checkTransfetStateBeforeLeave();

			return true;
		}

		return false;
	}

	@Subscribe
	public void onUpButtonPressedEvent(UpButtonPressedEvent event) {
		if (getActivity().hashCode() == event.getActivityId()) {
			checkTransfetStateBeforeLeave();
		}
	}

	@OptionsItem(R.id.shipping_order_stop_lines_to_transfer_menu__accept_transfer)
	public boolean optionAcceptTransferingSelected() {
		if (!adapter.isEmpty() && adapter.getItem(0) != null && adapter.getItem(0).getTransferState() != TransferState.Transfering) {
			showDialogInfo(R.string.shipping_order_stop_lines_to_transfer_menu__before_accept_transfer_msg);

			return true;
		}

		checkTransferState();

		return true;
	}

	@OptionsItem(R.id.shipping_order_stop_lines_to_transfer_menu__undo_transfer)
	public boolean optionUndoTransferingSelected() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(getString(R.string.alert_dialog__message__title));
		builder.setMessage(getString(R.string.shipping_order_stop_lines_to_transfer_menu__undo_transfer_msg));
		builder.setPositiveButton(getString(com.u4.mobile.R.string.yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				undoTransfering();
			}
		});
		builder.setNegativeButton(getString(com.u4.mobile.R.string.no), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		builder.create().show();

		return true;
	}

	@UiThread
	public void populateView() {
		if (mStopObject == null) {
			mStopHeader.setText("");
			mStopAddress.setText("");

			return;
		}

		mStopHeader.setText(mStopObject.getLocaLocationName());
		mStopAddress.setText(mStopObject.getLocaLocationAddress());
		checkTransferState();
	}

	@Background
	public void refreshData() {
		adapter.reloadData(true, 0L);
		afterRefreshData();
	}

	@Background
	public void undoTransfering() {
		try {
			setLoaderVisible(true, R.string.shipping_order_line_start_transfer_menu__undo_transfer_in_progress_msg);
			ShippingOrderLine.undoTransferingByStop(mShippingOrderStopSourceGuid);
			afterUndoTransfering();
		} catch (SQLException e) {
			LogHelper.setErrorMessage(getClass().getName(), e);
		} finally {
			setLoaderVisible(false);
		}
	}
}
