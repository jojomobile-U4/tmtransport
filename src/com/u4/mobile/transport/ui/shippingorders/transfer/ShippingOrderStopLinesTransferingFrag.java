package com.u4.mobile.transport.ui.shippingorders.transfer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.ViewById;

import com.j256.ormlite.dao.Dao;
import com.u4.mobile.base.ui.ListFragmentBase;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.ShippingOrderLine;

import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

@EFragment(R.layout.lista_view_quick_search)
public class ShippingOrderStopLinesTransferingFrag extends ListFragmentBase<ShippingOrderLine> {

	@FragmentArg(ShippingOrderLineEndTransferAct.SHIPPING_ORDER_SOURCE_ID__PARAM)
	Long mShippingOrderSourceId;

	@ViewById(R.id.list__list_View)
	protected ListView listView;

	@ViewById(R.id.searchView)
	protected EditText searchView;

	@ViewById(R.id.list_view_empty_view)
	LinearLayout listViewEmptyView;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	public Dao<ShippingOrderLine, Long> shippingOrderLineDao;

	@Bean
	ShippingOrderStopLinesTransferingAdapter adapter;

	@AfterViews
	public void onCreateView() {
		adapter.setShippingOrderSourceId(mShippingOrderSourceId);
		super.adapter = adapter;
		super.listView = listView;
		listView.setEmptyView(listViewEmptyView);
		searchView.addTextChangedListener(adapter.getTextWatcher());
		super.getList();
	}
}
