package com.u4.mobile.transport.ui.shippingorders.transfer;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import com.u4.mobile.base.ui.StandardActivity;

@EActivity
public class ShippingOrderStopsToTransferAct extends StandardActivity {

	public static final String SHIPPING_ORDER_SOURCE_ID__PARAM = "shippingOrderSourceId";

	@Extra(SHIPPING_ORDER_SOURCE_ID__PARAM)
	Long mShippingOrderSourceId;

	@Override
	public void registerFragments() {
		addFragment(ShippingOrderStopsToTransferFrag_.builder().mShippingOrderSourceId(mShippingOrderSourceId).build());
	}
}
