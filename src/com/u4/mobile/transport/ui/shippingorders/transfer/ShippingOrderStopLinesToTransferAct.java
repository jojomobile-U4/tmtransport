package com.u4.mobile.transport.ui.shippingorders.transfer;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import android.view.MenuItem;

import com.u4.mobile.base.bus.UpButtonPressedEvent;
import com.u4.mobile.base.ui.StandardActivity;

@EActivity
public class ShippingOrderStopLinesToTransferAct extends StandardActivity {
	public static final String SHIPPING_ORDER_STOP_SOURCE_GUID__PARAM = "shippingOrderStopSourceGuid";
	public static final String OSOB_ID__PARAM = "osobId";
	public static final String SHIPPING_ORDER_DESTINATION_ID__PARAM = "shippingOrderDestinationId";
	public static final String SHIPPING_ORDER_SOURCE_ID__PARAM = "shippingOrderSourceId";
	public static final String SHIPPING_ORDER_STOP_HUB_GUID__PARAM = "shippingOrderStopHubGuid";

	@Extra(SHIPPING_ORDER_STOP_SOURCE_GUID__PARAM)
	String mShippingOrderStopSourceGuid;

	@Extra(OSOB_ID__PARAM)
	Long mOsobId;

	@Extra(SHIPPING_ORDER_DESTINATION_ID__PARAM)
	Long mShippingOrderDestinationId;

	@Extra(SHIPPING_ORDER_SOURCE_ID__PARAM)
	Long mShippingOrderSourceId;

	@Extra(SHIPPING_ORDER_STOP_HUB_GUID__PARAM)
	String mShippingOrderStopHubGuid;

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			
			mDataBus.post(new UpButtonPressedEvent(hashCode()));
			return true;
		}

		return (super.onOptionsItemSelected(item));
	}

	@Override
	public void registerFragments() {
		addFragment(ShippingOrderStopLinesToTransferFrag_.builder().mShippingOrderStopSourceGuid(mShippingOrderStopSourceGuid).mOsobId(mOsobId)
				.mShippingOrderDestinationId(mShippingOrderDestinationId).mShippingOrderSourceId(mShippingOrderSourceId).mShippingOrderStopHubGuid(mShippingOrderStopHubGuid).build());
	}
}
