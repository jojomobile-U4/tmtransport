package com.u4.mobile.transport.ui.shippingorders;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.u4.mobile.R;
import com.u4.mobile.base.ui.ListFragmentBase;
import com.u4.mobile.transport.model.ShippingOrder;

@EFragment(R.layout.lista_view_quick_search)
public class ShippingOrdersFrag extends ListFragmentBase<ShippingOrder> {

	private ShippingOrder currentObject;

	@ViewById(R.id.list__list_View)
	protected ListView listView;

	@ViewById(R.id.searchView)
	protected EditText searchView;

	@ViewById(R.id.list_view_empty_view)
	LinearLayout listViewEmptyView;

	@Bean
	ShippingOrdersListAdapter adapter;

	void prepareFrag() {
		super.adapter = adapter;
		super.listView = listView;
		currentObject = null;
	}

	@AfterViews
	void setupView() {
		prepareFrag();

		listView.setEmptyView(listViewEmptyView);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				currentObject = (ShippingOrder) parent.getItemAtPosition(position);
				if (currentObject != null) {
					Intent intent = new Intent(getActivity(), ShippingOrderAct_.class);
					intent.putExtra(ShippingOrderAct.SHIPPING_ORDER_ID__PARAM, currentObject.getId());
					startActivity(intent);
				}
			}
		});

		searchView.addTextChangedListener(adapter.getTextWatcher());
		super.getList();
	}

}
