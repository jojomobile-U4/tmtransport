package com.u4.mobile.transport.ui.shippingorders;

import org.androidannotations.annotations.EActivity;

import android.support.v4.app.FragmentTransaction;

import com.u4.mobile.base.ui.NavigationDrawerActivity;
import com.u4.mobile.transport.R;

@EActivity
public class ShippingOrdersAct extends NavigationDrawerActivity {

	@Override
	public void poplutateView() {
		super.poplutateView();

		ShippingOrdersFrag newFragment = ShippingOrdersFrag_.builder().build();

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

		ft.add(R.id.navigation_drawer_frame_container, newFragment).commit();
	}
}
