package com.u4.mobile.transport.ui;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import com.u4.mobile.base.ui.StandardActivity;
import com.u4.mobile.model.UpdateInfo;
import com.u4.mobile.transport.R;

import android.os.Bundle;

@EActivity
public class NotificationAct extends StandardActivity {

	public static final String APP_UPDATE_IS_AVAILABLE__PARAM = "isAppUpdateAvailable";
	public static final String APP_UPDATE_DATA__PARAM = "updateInfo";

	@Extra(APP_UPDATE_IS_AVAILABLE__PARAM)
	Boolean mIsAppUpdateAvailable;

	@Extra(APP_UPDATE_DATA__PARAM)
	UpdateInfo mUpdateInfo;

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		if (getActionBar() != null) {
			getActionBar().setDisplayHomeAsUpEnabled(false);
		}
	}

	@Override
	public void registerFragments() {
		if (mIsAppUpdateAvailable != null && mIsAppUpdateAvailable) {
			setTitle(R.string.application_update_act__title);
			addFragment(ApplicationUpdateFrag_.builder().mUpdateInfo(mUpdateInfo).build());
		}
	}
}
