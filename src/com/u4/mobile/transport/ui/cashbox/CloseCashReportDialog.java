package com.u4.mobile.transport.ui.cashbox;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import com.u4.mobile.base.helpers.DialogFactory;
import com.u4.mobile.base.ui.DialogFragmentBase;
import com.u4.mobile.base.ui.RequiredFieldValidator;
import com.u4.mobile.transport.R;
import com.u4.mobile.utils.CalculationHelper;
import com.u4.mobile.utils.ControlsHelper;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.DateTimeHelper.DateTimeFormat;

@EFragment(R.layout.cash_report_close)
public class CloseCashReportDialog extends DialogFragmentBase {

	@ViewById(R.id.cash_report_close__date_from)
	TextView dateFromTextView;

	@ViewById(R.id.cash_report_close__open_state)
	TextView openStateTextView;

	@ViewById(R.id.cash_report_close__cashbox_state)
	TextView cashboxStateTextView;

	@ViewById(R.id.cash_report_close__generate_kw)
	CheckBox generateKwCheckBox;

	@ViewById(R.id.cash_report_close__kw_value)
	EditText kwValueEditText;

	@Bean
	CashBoxHelper cashBoxHelper;

	@UiThread
	public void afterCloseCashbox(Integer result) {
		switch (result) {
		case -1:
			DialogFactory.createWarning(getActivity(), getString(R.string.cash_report_close__no_currently_open_message)).show(getFragmentManager(), "closeCashReportWarring");
			break;
		case -2:
			DialogFactory.createWarning(getActivity(), getString(R.string.cash_report_close__notapproved_doc_exists_message)).show(getFragmentManager(), "closeCashReportWarring");
			break;
		default:
			dismiss();
			break;
		}
	}

	@Click(R.id.cash_report_close__cancel_button)
	public void cancelClick() {
		dismiss();
	}

	@Click(R.id.cash_report_close__ok_button)
	@Background
	public void okClick() {
		final boolean generateKw = generateKwCheckBox.isChecked();
		final double kwValue = CalculationHelper.parseDouble(kwValueEditText.getText().toString());

		afterCloseCashbox(cashBoxHelper.closeCurrentCashReport(generateKw, kwValue));
	}

	@AfterViews
	@Background
	public void onCreateView() {
		populateView(cashBoxHelper.isCashBoxAvailable());
	}

	@UiThread
	public void populateView(boolean isCashBoxAvailable) {
		if (isCashBoxAvailable) {
			dateFromTextView.setText(DateTimeHelper.getDateAsString(cashBoxHelper.getOpenDate(), DateTimeFormat.OracleDateFormat));
			openStateTextView.setText(String.format("%.2f", cashBoxHelper.getOpenState()));
			cashboxStateTextView.setText(String.format("%.2f", cashBoxHelper.getCashBoxState()));
			kwValueEditText.setText(String.format("%.2f", cashBoxHelper.getCashBoxState()));

			final RequiredFieldValidator validator = new RequiredFieldValidator(kwValueEditText, false);
			validator.setValidateMinMaxValue(true, 0.00, cashBoxHelper.getCashBoxState());
			ControlsHelper.addRequiredFieldValidation(kwValueEditText, validator);

			generateKwCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					kwValueEditText.setEnabled(isChecked);
				}
			});
		}

		getDialog().setTitle(R.string.cash_report_close__title);
	}
}
