package com.u4.mobile.transport.ui.cashbox;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.EActivity;

import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.base.ui.NavigationDrawerTabActivity;

@EActivity
public class CashBoxAct extends NavigationDrawerTabActivity {

	@Override
	public List<FragmentBase> getFragments() {
		List<FragmentBase> fragmentList = new ArrayList<FragmentBase>();
		fragmentList.add(CashBoxDetailsFrag_.builder().build());
		fragmentList.add(CashDocumentsListFrag_.builder().build());

		return fragmentList;
	}
}
