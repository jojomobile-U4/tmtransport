package com.u4.mobile.transport.ui.cashbox;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.base.ui.PageSectionsPagerAdapter;
import com.u4.mobile.transport.R;

@EActivity(com.u4.mobile.R.layout.fragment_tab)
public class CashDocumentAct extends FragmentActivity {

	public static final String CASH_DOCUMENT_ID__PARAM = "cashDocumentId";
	
	@Extra(CASH_DOCUMENT_ID__PARAM)
	Long cashDocumentId;
	
	private ViewPager viewPager;
	private PagerAdapter pagerAdapter;

	private List<FragmentBase> getFragments() {
		List<FragmentBase> fragmentList = new ArrayList<FragmentBase>();
		CashDocumentDetailsFrag fragment1 = CashDocumentDetailsFrag_.builder().cashDocumentId(cashDocumentId).build();
		fragmentList.add(fragment1);

		return fragmentList;
	}

	@AfterViews
	void setupView() {
		viewPager = (ViewPager) findViewById(R.id.pager);
		pagerAdapter = new PageSectionsPagerAdapter(getSupportFragmentManager(), getFragments());
		viewPager.setAdapter(pagerAdapter);
		viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				invalidateOptionsMenu();
			}
		});
	}
}
