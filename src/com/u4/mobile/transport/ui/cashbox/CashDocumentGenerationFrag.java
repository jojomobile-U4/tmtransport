package com.u4.mobile.transport.ui.cashbox;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.base.ui.RequiredFieldValidator;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.CashDocument;
import com.u4.mobile.transport.model.CashDocument.CashDocumentType;
import com.u4.mobile.transport.model.CashDocumentPosition;
import com.u4.mobile.transport.model.Contractor;
import com.u4.mobile.transport.model.ContractorAccounts;
import com.u4.mobile.utils.CalculationHelper;
import com.u4.mobile.utils.ControlsHelper;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.DateTimeHelper.DateTimeFormat;
import com.u4.mobile.utils.LogHelper;

@OptionsMenu(R.menu.cash_document_generation_menu)
@EFragment(R.layout.cash_document_generation_frag)
public class CashDocumentGenerationFrag extends FragmentBase {

	public static final String CASH_DOCUMENT_GENERATION__CASH_IN_GUID_PARAM = "cashInGuid";
	public static final String CASH_DOCUMENT_GENERATION__CASH_IN_BLANK_NUMBER_PARAM = "cashInBlankNumber";
	public static final String CASH_DOCUMENT_GENERATION__CASH_OUT_GUID_PARAM = "cashOutGuid";
	private List<ContractorAccounts> cashInPosition;
	private double cashInValue = 0;
	private List<ContractorAccounts> cashOutPosition;
	private double cashOutValue = 0;
	private Contractor currentObject;

	@ViewById(R.id.cash_document_generation__blank_number)
	EditText blankNumberEditText;

	@OrmLiteDao(helper = DatabaseHelper.class, model = CashDocument.class)
	Dao<CashDocument, Long> cashDocumentDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = CashDocumentPosition.class)
	Dao<CashDocumentPosition, Long> cashDocumentPositionDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ContractorAccounts.class)
	Dao<ContractorAccounts, Long> contractorAccountsDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = Contractor.class)
	Dao<Contractor, Long> contractorDao;

	@FragmentArg(CashDocumentGenerationAct.CONTRACTOR_ID__PARAM)
	Long contractorId;

	@ViewById(R.id.cash_document_generation__kp_content)
	LinearLayout kpContentLayout;

	@ViewById(R.id.cash_document_generation__kp_value)
	EditText kpValueEditText;

	@ViewById(R.id.cash_document_generation__kw_content)
	LinearLayout kwContentLayout;

	@ViewById(R.id.cash_document_generation__no_source_data)
	LinearLayout noSourceDataLayout;

	@ViewById(R.id.cash_document_generation__kw_value)
	EditText kwValueEditText;

	@FragmentArg(CashDocumentGenerationAct.SOURCE_ACCOUNTS_GUID_ARRAY__PARAM)
	String[] sourceAccountsGuidArray;

	@OptionsItem(R.id.cash_document_generation_menu__cancel)
	public void cancelSelected() {
		getActivity().setResult(Activity.RESULT_CANCELED);
		getActivity().finish();
	}

	@OptionsItem(R.id.cash_document_generation_menu__generate)
	public void generateSelected() {
		generateCashDocuments();
	}

	@AfterViews
	@Background
	public void onCreateView() {
		cashInPosition = new ArrayList<ContractorAccounts>();
		cashOutPosition = new ArrayList<ContractorAccounts>();

		try {
			currentObject = contractorDao.queryForId(contractorId);

			if (currentObject != null) {
				final QueryBuilder<ContractorAccounts, Long> builder = contractorAccountsDao.queryBuilder();
				final Where<ContractorAccounts, Long> where = builder.where();

				if (sourceAccountsGuidArray != null) {
					where.in(DownloadTransferObjectBase.GUID__FIELD_NAME, (Object[]) sourceAccountsGuidArray);
				}

				where.and().notIn(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue());

				final List<ContractorAccounts> accountsToPay = contractorAccountsDao.query(builder.orderBy(ContractorAccounts.MATURITY_DATE__FIELD_NAME, true).prepare());

				if (accountsToPay != null && accountsToPay.size() > 0) {
					for (final ContractorAccounts acount : accountsToPay) {
						if (acount.getLeftToPay() > 0) {
							cashInPosition.add(acount);
							cashInValue += acount.getLeftToPay();
						} else {
							cashOutPosition.add(acount);
							cashOutValue += -acount.getLeftToPay();
						}
					}

					cashInValue = CalculationHelper.round(cashInValue);
					cashOutValue = CalculationHelper.round(cashOutValue);
				}
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		}

		populateView();
	}

	@UiThread
	public void populateView() {
		if (cashOutValue > 0) {
			kwValueEditText.setText(String.format("%.2f", cashOutValue));
			kwValueEditText.requestFocus();

			final RequiredFieldValidator validator = new RequiredFieldValidator(kwValueEditText, false);
			validator.setValidateMinMaxValue(true, 0.001, 99999999999999.99);
			ControlsHelper.addRequiredFieldValidation(kwValueEditText, validator);
		} else {
			kwValueEditText.setEnabled(false);
			kwContentLayout.setVisibility(View.GONE);
		}

		if (cashInValue > 0) {
			kpValueEditText.setText(String.format("%.2f", cashInValue));

			final RequiredFieldValidator validator = new RequiredFieldValidator(kpValueEditText, false);
			validator.setValidateMinMaxValue(true, 0.001, 99999999999999.99);
			ControlsHelper.addRequiredFieldValidation(kpValueEditText, validator);

			blankNumberEditText.requestFocus();

		} else {
			kpValueEditText.setEnabled(false);
			kpContentLayout.setVisibility(View.GONE);
		}

		if (cashInValue <= 0 && cashOutValue <= 0) {
			noSourceDataLayout.setVisibility(View.VISIBLE);
		}

	}

	private void generateCashDocuments() {
		if (kpValueEditText.getError() == null && kwValueEditText.getError() == null) {
			CashDocument cashInDocument = null;
			CashDocument cashOutDocument = null;
			cashInValue = CalculationHelper.round(CalculationHelper.parseDouble(kpValueEditText.getText().toString()));
			cashOutValue = CalculationHelper.round(CalculationHelper.parseDouble(kwValueEditText.getText().toString()));

			if (cashInValue != 0.0) {
				try {
					cashInDocument = new CashDocument();
					cashInDocument.setRecordState(RecordStates.Inserted);
					cashInDocument.setContractor(currentObject);
					cashInDocument.setDocumentDate(DateTimeHelper.getCurrentDateAsString(DateTimeFormat.OracleDateTimeFormat));
					cashInDocument.setDocumentSymbol(CashDocumentType.KP.toString());
					cashInDocument.setDocumentType(CashDocumentType.KP);
					cashInDocument.setDescription(blankNumberEditText.getText().toString());

					cashDocumentDao.create(cashInDocument);

					for (final ContractorAccounts account : cashInPosition) {
						double payment = CalculationHelper.round(account.getLeftToPay());

						if (payment > cashInValue) {
							payment = cashInValue;
						}

						final CashDocumentPosition cashInPosition = new CashDocumentPosition();
						cashInPosition.setRecordState(RecordStates.Inserted);
						cashInPosition.setCashDocument(cashInDocument);
						cashInPosition.setSinvGuid(account.getSinvGuid());
						cashInPosition.setIncome(payment);
						cashInPosition.setTitle(account.getRndoSymbol());
						cashInPosition.setContractorAccounts(account);

						cashDocumentPositionDao.create(cashInPosition);

						cashInValue = CalculationHelper.round(cashInValue - payment);

						if (cashInValue == 0.0) {
							break;
						}
					}

					cashInDocument.approve();
				} catch (final SQLException e) {
					LogHelper.setErrorMessage(this.getClass().getName(), e);
				}
			}

			if (cashOutValue != 0.0) {
				try {
					cashOutDocument = new CashDocument();
					cashOutDocument.setRecordState(RecordStates.Inserted);
					cashOutDocument.setContractor(currentObject);
					cashOutDocument.setDocumentDate(DateTimeHelper.getCurrentDateAsString(DateTimeFormat.OracleDateTimeFormat));
					cashOutDocument.setDocumentSymbol(CashDocumentType.KW.toString());
					cashOutDocument.setDocumentType(CashDocumentType.KW);

					cashDocumentDao.create(cashOutDocument);

					for (final ContractorAccounts account : cashOutPosition) {
						double payment = -CalculationHelper.round(account.getLeftToPay());

						if (payment > cashOutValue) {
							payment = cashOutValue;
						}

						final CashDocumentPosition cashOutPosition = new CashDocumentPosition();
						cashOutPosition.setRecordState(RecordStates.Inserted);
						cashOutPosition.setCashDocument(cashOutDocument);
						cashOutPosition.setSinvGuid(account.getSinvGuid());
						cashOutPosition.setExpenditure(payment);
						cashOutPosition.setTitle(account.getRndoSymbol());
						cashOutPosition.setContractorAccounts(account);

						cashDocumentPositionDao.create(cashOutPosition);

						cashOutValue = CalculationHelper.round(cashOutValue - payment);

						if (cashOutValue == 0.0) {
							break;
						}
					}

					cashOutDocument.approve();
				} catch (final SQLException e) {
					LogHelper.setErrorMessage(this.getClass().getName(), e);
				}
			}

			Intent intent = new Intent();

			if (cashInDocument != null) {
				intent.putExtra(CASH_DOCUMENT_GENERATION__CASH_IN_GUID_PARAM, cashInDocument.getGuid());
				intent.putExtra(CASH_DOCUMENT_GENERATION__CASH_IN_BLANK_NUMBER_PARAM, cashInDocument.getDescription());
			}

			if (cashOutDocument != null) {
				intent.putExtra(CASH_DOCUMENT_GENERATION__CASH_OUT_GUID_PARAM, cashOutDocument.getGuid());
			}

			getActivity().setResult(Activity.RESULT_OK, intent);
			getActivity().finish();
		} else if (cashInValue <= 0 && cashOutValue <= 0) {
			showDialogInfo(getString(R.string.cash_document_generation__no_source_data));
		}
	}
}
