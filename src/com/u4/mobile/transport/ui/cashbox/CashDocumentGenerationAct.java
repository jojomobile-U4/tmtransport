package com.u4.mobile.transport.ui.cashbox;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.u4.mobile.R;
import com.u4.mobile.base.ui.ActivityBase;

@EActivity(R.layout.activity_standard)
public class CashDocumentGenerationAct extends ActivityBase {

	public static final String CONTRACTOR_ID__PARAM = "contractorId";
	public static final String SOURCE_ACCOUNTS_GUID_ARRAY__PARAM = "sourceAccountsGuidArray";

	@AfterViews
	public void onCreateView() {
		Long contractorId = -1L;
		String[] sourceAccountsGuidArray = null;

		Bundle data = getIntent().getExtras();

		if (data != null) {
			if (data.containsKey(CONTRACTOR_ID__PARAM)) {
				contractorId = data.getLong(CONTRACTOR_ID__PARAM);
			}

			if (data.containsKey(SOURCE_ACCOUNTS_GUID_ARRAY__PARAM)) {
				sourceAccountsGuidArray = data.getStringArray(SOURCE_ACCOUNTS_GUID_ARRAY__PARAM);
			}
		}

		Fragment fragment = CashDocumentGenerationFrag_.builder().contractorId(contractorId).sourceAccountsGuidArray(sourceAccountsGuidArray).build();
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.add(R.id.activity_standard__content, fragment).commit();
	}
}
