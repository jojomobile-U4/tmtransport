package com.u4.mobile.transport.ui.cashbox;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.springframework.util.StringUtils;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.u4.mobile.base.ui.DateTimePickerDialog;
import com.u4.mobile.base.ui.DateTimePickerDialog.OnDateTimeSetListener;
import com.u4.mobile.base.ui.DateTimePickerDialog_;
import com.u4.mobile.base.ui.DialogFragmentBase;
import com.u4.mobile.base.ui.RequiredFieldValidator;
import com.u4.mobile.transport.R;
import com.u4.mobile.utils.CalculationHelper;
import com.u4.mobile.utils.ControlsHelper;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.DateTimeHelper.DateTimeFormat;

@EFragment(R.layout.cash_report_open)
public class OpenCashReportDialog extends DialogFragmentBase {

	@ViewById(R.id.cash_report_open__date_from)
	TextView dateFromTextView;

	@ViewById(R.id.cash_report_open__open_state)
	EditText openStateEditText;

	@Bean
	CashBoxHelper cashBoxHelper;

	@Click(R.id.cash_report_open__cancel_button)
	public void cancelClick() {
		dismiss();
	}

	@Click(R.id.cash_report_open__ok_button)
	@Background
	public void okClick() {
		boolean validated = validateDateFrom();

		if (dateFromTextView.getError() != null || openStateEditText.getError() != null) {
			return;
		}
		if (validated) {
			final Date dateFrom = StringUtils.hasLength(dateFromTextView.getText()) ? DateTimeHelper.getDate(dateFromTextView.getText().toString()) : DateTimeHelper.getCurrentDate();
			final double openState = CalculationHelper.parseDouble(openStateEditText.getText().toString());

			cashBoxHelper.openCashReport(dateFrom, openState);
			dismiss();
		}
	}

	@AfterViews
	@Background
	public void onCreateView() {
		cashBoxHelper.isCashBoxAvailable();
		populateView();
	}

	@UiThread
	public void populateView() {
		Date dateFrom = DateTimeHelper.getCurrentDate();
		dateFromTextView.setText(DateTimeHelper.getDateAsString(dateFrom, DateTimeFormat.OracleDateFormat));
		dateFromTextView.setTag(dateFrom);
		dateFromTextView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final DateTimePickerDialog datePickerFragment = DateTimePickerDialog_.builder().onlyDate(true).dateTime((Date) dateFromTextView.getTag()).build();
				datePickerFragment.setOnDateTimeSetListener(new OnDateTimeSetListener() {

					@Override
					public void onDateTimeSet(int year, int month, int day, int hour, int minute) {
						final Calendar calendar = new GregorianCalendar();
						calendar.set(year, month, day);

						final Date openDate = calendar.getTime();
						final String newDate = DateTimeHelper.getDateAsString(openDate, DateTimeFormat.OracleDateFormat);

						if (newDate != dateFromTextView.getText().toString()) {
							dateFromTextView.setText(newDate);
							dateFromTextView.setTag(openDate);
							validateDateFrom();
						}
					}
				});

				datePickerFragment.show(getFragmentManager(), "datePicker");
			}
		});

		openStateEditText.setText(String.format("%.2f", cashBoxHelper.getOpenState()));
		final RequiredFieldValidator validator = new RequiredFieldValidator(openStateEditText, false);
		validator.setValidateMinMaxValue(true, 0.00, 99999999999999.99);
		ControlsHelper.addRequiredFieldValidation(openStateEditText, validator);

		getDialog().setTitle(R.string.cash_report_open__title);
		validateDateFrom();
	}

	private boolean validateDateFrom() {
		Date dateFrom = StringUtils.hasLength(dateFromTextView.getText()) ? DateTimeHelper.getDate(dateFromTextView.getText().toString()) : DateTimeHelper.getCurrentDate();

		int validate = cashBoxHelper.isValidForOpening(dateFrom);

		switch (validate) {
		case -1:
			dateFromTextView.setError(getString(R.string.cash_report_open__date_from_already_exists));
			return false;
		case -2:
			dateFromTextView.setError(getString(R.string.cash_report_open__date_from_for_future_unavailable));
			return false;
		case -3:
			dateFromTextView.setError(getString(R.string.cash_report_open__date_from_unsent_data_exists));
			return false;
		case 0:
			dateFromTextView.setError(null);
			break;
		}

		return true;
	}
}
