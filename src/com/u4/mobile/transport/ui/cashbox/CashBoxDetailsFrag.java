package com.u4.mobile.transport.ui.cashbox;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.u4.mobile.base.app.ApplicationInterfaces.ITabFrafments;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.DaoHelper;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.app.MainApp;
import com.u4.mobile.transport.model.CashDocument;
import com.u4.mobile.transport.model.CashDocumentPosition;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.DateTimeHelper.DateTimeFormat;
import com.u4.mobile.utils.LogHelper;

@EFragment(R.layout.cash_box_details_frag)
public class CashBoxDetailsFrag extends FragmentBase implements ITabFrafments {

	@OrmLiteDao(helper = DatabaseHelper.class, model = CashDocument.class)
	static Dao<CashDocument, Long> cashDocumentDao;

	@ViewById(R.id.cash_box_details__date_from)
	TextView dateFromTextView;

	@ViewById(R.id.cash_box_details__income)
	TextView incomeTextView;

	@ViewById(R.id.cash_box_details__expenditure)
	TextView expenditureTextView;

	@ViewById(R.id.cash_box_details__cash_box_state)
	TextView cashBoxStateTextView;

	private double expenditure = 0.0;
	private double income = 0.0;
	private double state = 0.0;

	@Override
	public String getPageTitle() {
		return MainApp.getInstance().getApplicationContext().getResources().getString(R.string.cash_box_details__tab_title).toUpperCase(Locale.getDefault());
	}

	@Background
	public void loadData() {
		setLoaderVisible(true);

		try {
			HashMap<String, Object> queryParam = new HashMap<String, Object>();
			queryParam.put("CashDocument", CashDocument.NAME);
			queryParam.put("CashDocumentPosition", CashDocumentPosition.NAME);
			queryParam.put("Expenditure", CashDocumentPosition.EXPENDITURE_FIELD_NAME);
			queryParam.put("Income", CashDocumentPosition.INCOME_FIELD_NAME);
			queryParam.put("KsdkGuid", CashDocumentPosition.KSDK_GUID_FIELD_NAME);
			queryParam.put("DocumentType", CashDocument.DOCUMENT_TYPE_FIELD_NAME);
			queryParam.put("Approved", CashDocument.APPROVED_FIELD_NAME);
			queryParam.put("DocumentDate", CashDocument.DOCUMENT_DATE_FIELD_NAME);
			queryParam.put("DocumentDateFilter", DateTimeHelper.getCurrentDateAsString(DateTimeFormat.OracleDateFormat));

			StringBuilder query = new StringBuilder();

			query.append("SELECT COALESCE(SUM(kspd.%(Income)), 0), COALESCE(SUM(kspd.%(Expenditure)), 0) ");
			query.append("  FROM %(CashDocumentPosition) kspd");
			query.append(" INNER JOIN %(CashDocument) ksdk ON ksdk.Guid = kspd.%(KsdkGuid)");
			query.append(" WHERE ksdk.%(Approved) = 1");
			query.append("   AND Date(ksdk.%(DocumentDate)) = Date('%(DocumentDateFilter)')");
			query.append("   AND ksdk.%(DocumentType) IN ('KP', 'KW')");

			String[] result = cashDocumentDao.queryRaw(DaoHelper.formatQuery(query.toString(), queryParam)).getFirstResult();

			if (result != null) {
				income = Double.parseDouble(result[0]);
				expenditure = Double.parseDouble(result[0]);
			}
		} catch (final SQLException e) {
			LogHelper.setErrorMessage(CashBoxHelper.class.getClass().getName(), e);
		} finally {
			setLoaderVisible(false);
		}

		state = income - expenditure;

		populateView();
	}

	@AfterViews
	public void onCreateView() {
		dateFromTextView.setText(DateTimeHelper.getCurrentDateAsString(DateTimeFormat.OracleDateFormat));
		incomeTextView.setText(String.format("%.2f", 0.0));
		expenditureTextView.setText(String.format("%.2f", 0.0));
		cashBoxStateTextView.setText(String.format("%.2f", 0.0));

		loadData();
	}

	@UiThread
	public void populateView() {
		incomeTextView.setText(String.format("%.2f", income));
		expenditureTextView.setText(String.format("%.2f", expenditure));
		cashBoxStateTextView.setText(String.format("%.2f", state));
	}
}
