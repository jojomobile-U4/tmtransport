package com.u4.mobile.transport.ui.cashbox;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.OrmLiteDao;
import org.springframework.util.StringUtils;

import android.view.View;
import android.view.ViewGroup;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.u4.mobile.base.ui.ListAdapterBase;
import com.u4.mobile.model.DaoHelper;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.transport.model.CashDocument;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.DateTimeHelper.DateTimeFormat;
import com.u4.mobile.utils.LogHelper;

@EBean
public class CashDocumentsListAdapter extends ListAdapterBase<CashDocument> {

	@OrmLiteDao(helper = DatabaseHelper.class, model = CashDocument.class)
	static Dao<CashDocument, Long> cashDocumentDao;

	public CashDocumentsListAdapter() {
		super();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		CashDocumentsListItemView listItemView = null;

		try {
			if (convertView == null) {
				listItemView = CashDocumentsListItemView_.build(getContext());
			} else {
				listItemView = (CashDocumentsListItemView) convertView;
			}

			CashDocument item = getItem(position);
			boolean checked = isSelecting() && getCheckedItems() != null && getCheckedItems().contains(item);
			listItemView.populateFrom(item, isSelecting(), checked, getHighlightText());

			return listItemView;
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	@Override
	public QueryBuilder<CashDocument, Long> prepareQuery() {

		QueryBuilder<CashDocument, Long> query = cashDocumentDao.queryBuilder();
		try {

			Where<CashDocument, Long> where = null;
			where = query.where();
			where.raw(String.format("Date(%s) = DATE('%s')", CashDocument.DOCUMENT_DATE_FIELD_NAME, DateTimeHelper.getCurrentDateAsString(DateTimeFormat.OracleDateFormat)));

			if (StringUtils.hasText(getSearchText())) {
				where.and().like(CashDocument.DOCUMENT_SYMBOL_FIELD_NAME, DaoHelper.makeLikeExpression(getSearchText()));
			}
			
			query.setWhere(where);
			query.offset(getListOffset() * getPageSize()).limit((long) getPageSize());
			query.orderBy(DownloadTransferObjectBase.ID__FIELD_NAME, false);

		} catch (final Exception e) {
			LogHelper.setErrorMessage(CashDocumentsListAdapter.class.getName(), e);
		}
		
		return query;
	}
}
