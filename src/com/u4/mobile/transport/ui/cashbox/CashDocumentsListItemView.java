package com.u4.mobile.transport.ui.cashbox;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.CashDocument;
import com.u4.mobile.utils.ControlsHelper;
import com.u4.mobile.utils.DateTimeHelper;

@EViewGroup(R.layout.cash_documents_list_view_item)
public class CashDocumentsListItemView extends LinearLayout {

	@ViewById(R.id.cash_documents_list_indicator__text_view)
	TextView indicatorTextView;

	@ViewById(R.id.cash_documents_list_symbol__text_view)
	TextView symbolTextView;

	@ViewById(R.id.cash_documents_list_type__text_view)
	TextView typeTextView;

	@ViewById(R.id.cash_documents_list_date__text_view)
	TextView dateTextView;

	@ViewById(R.id.cash_documents_list_amount__text_view)
	TextView amountTextView;

	@ViewById(R.id.cash_documents_list_contractor__text_view)
	TextView contractorTextView;

	@ViewById(R.id.cash_documents_list_select_checkbox)
	CheckBox selectCheckBox;

	public CashDocumentsListItemView(Context context) {
		super(context);
	}

	public void populateFrom(final CashDocument cashDocument, Boolean selecting, Boolean checked, String highlightText) {
		symbolTextView.setText(cashDocument.getDocumentSymbol());
		typeTextView.setText(cashDocument.getDocumentType().toString());
		dateTextView.setText(DateTimeHelper.getDateAsString(DateTimeHelper.getDate(cashDocument.getDocumentDate().toString()), com.u4.mobile.utils.DateTimeHelper.DateTimeFormat.OracleDateFormat));
		contractorTextView.setText(cashDocument.getContractor() != null ? cashDocument.getContractor().getName() : getContext().getString(R.string.cash_documents_list__no_contractor_data));

		final AsyncTask<Void, Void, Double> setAmount = new AsyncTask<Void, Void, Double>() {
			@Override
			protected Double doInBackground(Void... params) {
				return cashDocument.getAmount();
			}

			@Override
			protected void onPostExecute(Double amount) {
				amountTextView.setText(String.format("%.2f", amount));
			}
		};
		setAmount.execute();

		if (cashDocument.isApproved()) {
			indicatorTextView.setBackgroundResource(R.drawable.list_item_indicator_green);
		} else {
			indicatorTextView.setBackgroundResource(R.drawable.list_item_indicator_red);
		}

		if (selecting) {
			selectCheckBox.setVisibility(View.VISIBLE);
			selectCheckBox.setChecked(checked);
		} else {
			selectCheckBox.setVisibility(View.GONE);
		}

		selectCheckBox.setVisibility(View.GONE);
		ControlsHelper.highlightText(symbolTextView, highlightText);
	}

}
