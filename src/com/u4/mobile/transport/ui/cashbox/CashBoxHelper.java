package com.u4.mobile.transport.ui.cashbox;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.OrmLiteDao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DatabaseHolder;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.app.MainApp;
import com.u4.mobile.transport.model.CashDocument;
import com.u4.mobile.transport.model.CashDocument.CashDocumentType;
import com.u4.mobile.transport.model.CashDocumentPosition;
import com.u4.mobile.transport.model.CashReport;
import com.u4.mobile.transport.model.CashReport.ReportState;
import com.u4.mobile.utils.CalculationHelper;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.DateTimeHelper.DateTimeFormat;
import com.u4.mobile.utils.LogHelper;

@EBean(scope = Scope.Singleton)
public class CashBoxHelper {

	@OrmLiteDao(helper = DatabaseHelper.class, model = CashReport.class)
	static Dao<CashReport, Long> cashReportDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = CashDocument.class)
	static Dao<CashDocument, Long> cashDocumentDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = CashDocumentPosition.class)
	static Dao<CashDocumentPosition, Long> cashDocumentPositionDao;

	private static CashReport currentCashReport;

	private static boolean isCurrentCashReportInitialized = false;

	public static void generateKw(double kwValue) {
		try {
			final CashDocument cashOutDocument = new CashDocument();
			cashOutDocument.setRecordState(RecordStates.Inserted);
			cashOutDocument.setDocumentDate(DateTimeHelper.getDateAsString(new GregorianCalendar().getTime(), DateTimeFormat.OracleDateTimeFormat));
			cashOutDocument.setDocumentSymbol(CashDocumentType.KW.toString());
			cashOutDocument.setDocumentType(CashDocumentType.KW);
			cashDocumentDao.create(cashOutDocument);

			final CashDocumentPosition cashOutPosition = new CashDocumentPosition();
			cashOutPosition.setRecordState(RecordStates.Inserted);
			cashOutPosition.setCashDocument(cashOutDocument);
			cashOutPosition.setExpenditure(kwValue);
			cashOutPosition.setTitle(MainApp.getInstance().getApplicationContext().getResources().getString(R.string.cash_box_helper__kw_to_sales_title));
			cashDocumentPositionDao.create(cashOutPosition);

			cashOutDocument.approve();
		} catch (final SQLException e) {
			LogHelper.setErrorMessage(CashBoxHelper.class.getClass().getName(), e);
		}
	}

	public static long getCashDocumentNextNumber(CashDocumentType type) {
		try {
			final Dao<CashDocument, Long> dao = DatabaseHolder.getInstance().getDao(CashDocument.class);
			final QueryBuilder<CashDocument, Long> qb = dao.queryBuilder();
			qb.selectRaw("MAX(" + CashDocument.DOCUMENT_NUMBER_FIELD_NAME + ")");
			qb.where().eq(CashDocument.DOCUMENT_TYPE_FIELD_NAME, type).and().notIn(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue());
			final String[] result = dao.queryRaw(qb.prepareStatementString()).getFirstResult();

			if (result != null) {
				if (result[0] != null) {
					return CalculationHelper.parseLong(result[0]) + 1;
				}
			}

		} catch (final SQLException e) {
			LogHelper.setErrorMessage(CashBoxHelper.class.getName(), e);
		}

		return 1;
	}

	public static CashReport getCurrentCashReport() {
		if (currentCashReport == null && !isCurrentCashReportInitialized) {
			try {
				final QueryBuilder<CashReport, Long> qb = cashReportDao.queryBuilder();
				qb.where().eq(CashReport.STATUS_FIELD_NAME, ReportState.B).and().notIn(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue());

				qb.orderBy(CashReport.DATE_FROM_FIELD_NAME, false);

				currentCashReport = cashReportDao.queryForFirst(qb.prepare());

			} catch (final SQLException e) {
				LogHelper.setErrorMessage(CashBoxHelper.class.getName(), e);
			} finally {
				isCurrentCashReportInitialized = true;
			}
		}

		return currentCashReport;
	}

	public int checkCurrentCashReportClosePossibility() {
		if (getCurrentCashReport() == null || (getCurrentCashReport() != null && getCurrentCashReport().getStatus() != ReportState.B)) {
			return -1;
		}

		try {
			final QueryBuilder<CashDocument, Long> qb = cashDocumentDao.queryBuilder();
			qb.where().ge(CashDocument.KSRK_GUID_FIELD_NAME, getCurrentCashReport().getGuid()).and().notIn(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue()).and()
					.eq(CashDocument.APPROVED_FIELD_NAME, false);
			qb.setCountOf(true);

			String[] result = qb.queryRawFirst();

			if (result != null && !result[0].equals("0")) {
				return -2;
			}
		} catch (final SQLException e) {
			LogHelper.setErrorMessage(CashBoxHelper.class.getClass().getName(), e);
		}

		return 0;
	}

	public int closeCurrentCashReport(boolean generateKw, double kwValue) {
		int state = checkCurrentCashReportClosePossibility();

		if (state == 0) {
			try {
				if (generateKw && kwValue > 0.0) {
					currentCashReport.setCloseState(CalculationHelper.round(getCashBoxState() - kwValue));
					generateKw(kwValue);
				} else {
					currentCashReport.setCloseState(getCashBoxState());
				}

				currentCashReport.setStatus(ReportState.Z);
				currentCashReport.setDateTo(DateTimeHelper.getDateAsString(new GregorianCalendar().getTime(), DateTimeFormat.DateTimeFormat));

				cashReportDao.update(currentCashReport);

				currentCashReport = null;
				isCurrentCashReportInitialized = false;
			} catch (final SQLException e) {
				LogHelper.setErrorMessage(CashBoxHelper.class.getClass().getName(), e);
			}
		}

		return state;
	}

	public double getCashBoxState() {
		double state = 0.0;

		if (getCurrentCashReport() != null) {
			if (getCurrentCashReport().getStatus() == ReportState.B) {
				state = getOpenState() + getIncome() - getExpenditure();
			} else {
				state = getCurrentCashReport().getCloseState();
			}
		}

		return state;
	}

	public Date getDateByOpenDate() {
		Calendar calendar = new GregorianCalendar();
		Date openDate = getOpenDate();

		if (openDate != null) {
			calendar.setTime(openDate);
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			calendar = new GregorianCalendar();
			calendar.set(Calendar.DAY_OF_MONTH, day);
		}

		return calendar.getTime();
	}

	public double getExpenditure() {
		double expenditure = 0.0;

		if (getCurrentCashReport() != null && getCurrentCashReport().getStatus() == ReportState.B) {
			try {
				final QueryBuilder<CashDocument, Long> qbDocument = cashDocumentDao.queryBuilder();
				qbDocument.where().eq(CashDocument.KSRK_GUID_FIELD_NAME, getCurrentCashReport().getGuid()).and().eq(CashDocument.DOCUMENT_TYPE_FIELD_NAME, CashDocumentType.KW).and()
						.eq(CashDocument.APPROVED_FIELD_NAME, true);

				for (final CashDocument document : cashDocumentDao.query(qbDocument.prepare())) {
					expenditure += document.getAmount();
				}
			} catch (final SQLException e) {
				LogHelper.setErrorMessage(CashBoxHelper.class.getClass().getName(), e);
			}
		}

		return expenditure;
	}

	public double getIncome() {
		double income = 0.0;

		if (getCurrentCashReport() != null && getCurrentCashReport().getStatus() == ReportState.B) {
			try {
				final QueryBuilder<CashDocument, Long> qbDocument = cashDocumentDao.queryBuilder();
				qbDocument.where().eq(CashDocument.KSRK_GUID_FIELD_NAME, getCurrentCashReport().getGuid()).and().eq(CashDocument.DOCUMENT_TYPE_FIELD_NAME, CashDocumentType.KP).and()
						.eq(CashDocument.APPROVED_FIELD_NAME, true);

				for (final CashDocument document : cashDocumentDao.query(qbDocument.prepare())) {
					income += document.getAmount();
				}
			} catch (final SQLException e) {
				LogHelper.setErrorMessage(CashBoxHelper.class.getClass().getName(), e);
			}
		}

		return income;
	}

	public Date getOpenDate() {
		if (getCurrentCashReport() != null && getCurrentCashReport().getStatus() == ReportState.B) {
			return DateTimeHelper.getDate(getCurrentCashReport().getDateFrom());
		}

		return null;
	}

	public double getOpenState() {
		if (getCurrentCashReport() != null && getCurrentCashReport().getStatus() == ReportState.B) {
			return getCurrentCashReport().getOpenState();
		}

		return 0.0;
	}

	public boolean isCashBoxAvailable() {

		return getCurrentCashReport() != null && getCurrentCashReport().getStatus() == ReportState.B;
	}

	public int isValidForOpening(Date openDate) {

		Calendar calendar = new GregorianCalendar();
		calendar.setTime(openDate);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		try {
			final QueryBuilder<CashReport, Long> qb = cashReportDao.queryBuilder();
			qb.where().raw(String.format("Date(%s) >= Date('%s')", CashReport.DATE_FROM_FIELD_NAME, DateTimeHelper.getDateAsString(openDate, DateTimeFormat.OracleDateFormat)));
			qb.setCountOf(true);
			String[] result = qb.queryRawFirst();

			if (result != null && !result[0].equals("0")) {
				return -1;
			}

		} catch (final SQLException e) {
			LogHelper.setErrorMessage(CashBoxHelper.class.getClass().getName(), e);
		}

		java.sql.Date newDate = new java.sql.Date(calendar.getTimeInMillis());
		java.sql.Date currDate = new java.sql.Date(new GregorianCalendar().getTimeInMillis());

		if (currDate.before(newDate)) {
			return -2;
		}

		return 0;
	}

	public void openCashReport(Date dateFrom, double openState) {
		try {
			final CashReport cashReport = new CashReport();
			cashReport.setRecordState(RecordStates.Inserted);
			cashReport.setDateFrom(DateTimeHelper.getDateAsString(dateFrom, DateTimeFormat.DateTimeFormat));
			cashReport.setOpenState(openState);
			cashReport.setStatus(ReportState.B);

			cashReportDao.create(cashReport);
			currentCashReport = null;
			isCurrentCashReportInitialized = false;
		} catch (final SQLException e) {
			LogHelper.setErrorMessage(CashBoxHelper.class.getClass().getName(), e);
		}
	}
}
