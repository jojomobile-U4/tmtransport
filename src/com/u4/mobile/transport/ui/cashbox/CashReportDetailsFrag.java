package com.u4.mobile.transport.ui.cashbox;

import java.util.Locale;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.widget.TextView;

import com.u4.mobile.base.app.ApplicationInterfaces.ITabFrafments;
import com.u4.mobile.base.ui.DialogFragmentBase;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.app.MainApp;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.DateTimeHelper.DateTimeFormat;

@EFragment(R.layout.cash_report_details_frag)
@OptionsMenu(R.menu.cash_report_details_menu)
public class CashReportDetailsFrag extends FragmentBase implements ITabFrafments {

	@ViewById(R.id.cash_report_details__date_from)
	TextView dateFromTextView;

	@ViewById(R.id.cash_report_details__open_state)
	TextView openStateTextView;

	@ViewById(R.id.cash_report_details__income)
	TextView incomeTextView;

	@ViewById(R.id.cash_report_details__expenditure)
	TextView expenditureTextView;

	@ViewById(R.id.cash_report_details__cash_box_state)
	TextView cashBoxStateTextView;

	@OptionsMenuItem(R.id.cash_report_details__open_close_cash_report)
	MenuItem menuOpenCloseCashReport;

	@Bean
	CashBoxHelper cashBoxHelper;

	@Override
	public String getPageTitle() {
		return MainApp.getInstance().getApplicationContext().getResources().getString(R.string.cash_report_details__tab_title).toUpperCase(Locale.getDefault());
	}

	@Background
	public void loadData() {
		populateView(cashBoxHelper.isCashBoxAvailable());
	}

	@AfterViews
	public void onCreateView() {
		loadData();
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		if (cashBoxHelper.isCashBoxAvailable()) {
			menuOpenCloseCashReport.setIcon(R.drawable.ab_cashbox_day_stop_normal);
			menuOpenCloseCashReport.setTitle(R.string.cash_report_details_menu__close_cash_report);
			menuOpenCloseCashReport.setOnMenuItemClickListener(new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					showCloseCashReportDialog();
					return true;
				}
			});
		} else {
			menuOpenCloseCashReport.setIcon(R.drawable.ab_cashbox_day_start_normal);
			menuOpenCloseCashReport.setTitle(R.string.cash_report_details_menu__open_cash_report);
			menuOpenCloseCashReport.setOnMenuItemClickListener(new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					showOpenCashReportDialog();
					return true;
				}
			});
		}
	}

	@UiThread
	public void populateView(boolean isCashBoxAvailable) {

		if (isCashBoxAvailable) {
			dateFromTextView.setText(DateTimeHelper.getDateAsString(cashBoxHelper.getOpenDate(), DateTimeFormat.OracleDateFormat));
			openStateTextView.setText(String.format("%.2f", cashBoxHelper.getOpenState()));
			incomeTextView.setText(String.format("%.2f", cashBoxHelper.getIncome()));
			expenditureTextView.setText(String.format("%.2f", cashBoxHelper.getExpenditure()));
			cashBoxStateTextView.setText(String.format("%.2f", cashBoxHelper.getCashBoxState()));
		} else {
			dateFromTextView.setText(null);
			openStateTextView.setText(String.format("%.2f", 0.0));
			incomeTextView.setText(String.format("%.2f", 0.0));
			expenditureTextView.setText(String.format("%.2f", 0.0));
			cashBoxStateTextView.setText(String.format("%.2f", 0.0));
		}

		setLoaderVisible(false);
	}

	private void showCloseCashReportDialog() {
		final DialogFragmentBase df = new CloseCashReportDialog_();
		df.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {
				loadData();
				getActivity().invalidateOptionsMenu();
			}
		});

		df.show(getFragmentManager(), "closeCashReport");
	}

	private void showOpenCashReportDialog() {

		final DialogFragmentBase df = new OpenCashReportDialog_();
		df.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {
				loadData();
				getActivity().invalidateOptionsMenu();
			}
		});

		df.show(getFragmentManager(), "openCashReport");
	}

}
