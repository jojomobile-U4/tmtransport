package com.u4.mobile.transport.ui.cashbox;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.EActivity;

import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.base.ui.NavigationDrawerTabActivity;

@EActivity
public class CashReportAct extends NavigationDrawerTabActivity {

	@Override
	public List<FragmentBase> getFragments() {
		List<FragmentBase> fragmentList = new ArrayList<FragmentBase>();
		CashReportDetailsFrag_ fragment1 = new CashReportDetailsFrag_();
		fragmentList.add(fragment1);

		CashDocumentsListFrag_ fragment2 = new CashDocumentsListFrag_();
		fragmentList.add(fragment2);

		return fragmentList;
	}
}
