package com.u4.mobile.transport.ui.cashbox;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;
import com.u4.mobile.base.app.ApplicationInterfaces.ITabFrafments;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.app.MainApp;
import com.u4.mobile.transport.model.CashDocument;
import com.u4.mobile.transport.model.CashDocument.CashDocumentType;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.DateTimeHelper.DateTimeFormat;
import com.u4.mobile.utils.LogHelper;

@EFragment(R.layout.cash_document_details_frag)
public class CashDocumentDetailsFrag extends FragmentBase implements ITabFrafments {

	private CashDocument currentObject;
	private double documentValue;

	@ViewById(R.id.cash_document_details__symbol)
	EditText symbolEditText;

	@ViewById(R.id.cash_document_details__approved)
	CheckBox approvedCheckBox;

	@ViewById(R.id.cash_document_details__contractor)
	TextView contractorTextView;

	@ViewById(R.id.cash_document_details__type)
	Spinner typeSpinner;

	@ViewById(R.id.cash_document_details__issue_date)
	TextView issueDateTextView;

	@ViewById(R.id.cash_document_details__value)
	TextView valueTextView;

	@ViewById(R.id.cash_document_details__blank_number)
	EditText descriptionEditText;

	@FragmentArg(CashDocumentAct.CASH_DOCUMENT_ID__PARAM)
	Long cashDocumentId;

	@OrmLiteDao(helper = DatabaseHelper.class, model = CashDocument.class)
	Dao<CashDocument, Long> cashDocumentDao;

	@Override
	public String getPageTitle() {
		return MainApp.getInstance().getApplicationContext().getResources().getString(R.string.cash_document_details__tab_title).toUpperCase(Locale.getDefault());
	}

	@AfterViews
	@Background
	void loadData() {
		if (cashDocumentId != null) {
			try {
				QueryBuilder<CashDocument, Long> queryBuilder = cashDocumentDao.queryBuilder();
				queryBuilder.where().eq(DownloadTransferObjectBase.ID__FIELD_NAME, cashDocumentId);
				currentObject = queryBuilder.queryForFirst();
				documentValue = currentObject.getAmount();
				loadView();
			} catch (SQLException e) {
				LogHelper.setErrorMessage(CashDocumentDetailsFrag.class.getName(), e);
			}
		}
	}

	@UiThread
	public void loadView() {
		symbolEditText.setText(currentObject.getDocumentSymbol());
		approvedCheckBox.setChecked(currentObject.isApproved());
		contractorTextView.setText(currentObject.getContractor().getShortName());
		contractorTextView.setTag(currentObject.getContractor());

		final ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(getActivity(), com.u4.mobile.R.layout.spinner_item);
		final List<String> types = new ArrayList<String>();
		types.add(CashDocumentType.KP.toString());
		types.add(CashDocumentType.KW.toString());

		typeAdapter.addAll(types);
		typeSpinner.setAdapter(typeAdapter);
		typeSpinner.setSelection(currentObject.getDocumentType() != null ? currentObject.getDocumentType().ordinal() : CashDocumentType.KP.ordinal());
		typeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if (typeSpinner.isEnabled() && currentObject.getRecordState() == RecordStates.Inserted) {
					currentObject.setDocumentSymbol(typeSpinner.getAdapter().getItem(position).toString());
					symbolEditText.setText(currentObject.getDocumentSymbol());
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		issueDateTextView.setText(DateTimeHelper.getDateAsString(currentObject.getDocumentDate(), DateTimeFormat.OracleDateFormat));

		if (documentValue > 0.0) {
			typeSpinner.setEnabled(false);
		}

		valueTextView.setText(String.format("%.2f", documentValue));
		descriptionEditText.setText(currentObject.getDescription());

		if (currentObject.isApproved()) {
			issueDateTextView.setEnabled(false);
			contractorTextView.setEnabled(false);
			typeSpinner.setEnabled(false);
			descriptionEditText.setEnabled(false);
		}
		
		setLoaderVisible(false);
	}
}
