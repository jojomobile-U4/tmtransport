package com.u4.mobile.transport.ui.cashbox;

import java.util.Locale;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.u4.mobile.base.app.ApplicationInterfaces.ITabFrafments;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.app.MainApp;
import com.u4.mobile.transport.model.CashDocument;

@EFragment(com.u4.mobile.R.layout.lista_view_quick_search)
public class CashDocumentsListFrag extends FragmentBase implements ITabFrafments {

	private CashDocument currentObject;
	private final Long listPageSize = 10L;

	@Bean
	CashDocumentsListAdapter adapter;

	@ViewById(R.id.list__list_View)
	protected ListView listView;

	@ViewById(R.id.searchView)
	protected EditText searchView;

	private int getAdapterItemPosition(long id) {

		for (int position = 0; position < listView.getCount(); position++) {
			if (adapter.getItem(position).getId() == id) {
				return position;
			}
		}
		return 0;
	}

	public void getList() {
		adapter.init(this, listPageSize);
		listView.setAdapter(adapter);
		currentObject = null;
		reloadData(-1);
	}

	@Override
	public String getPageTitle() {
		return MainApp.getInstance().getApplicationContext().getResources().getString(R.string.cash_documents_list__tab_title).toUpperCase(Locale.getDefault());
	}

	@UiThread
	public void reloadData(int selectPositionID) {

		adapter.reloadData(true, 0L);
		selectItem(selectPositionID);
	}

	@UiThread
	public void reloadData(long selectObjectID) {

		adapter.reloadData(true, 0L);
		selectItem(selectObjectID);
	}

	@UiThread
	public void selectItem(int position) {

		listView.setItemChecked(position, true);
	}

	@UiThread
	public void selectItem(long id) {

		listView.setItemChecked(getAdapterItemPosition(id), true);
	}

	@AfterViews
	void setupView() {


		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				currentObject = (CashDocument) parent.getItemAtPosition(position);
				if (currentObject != null) {
					// Intent intent = new Intent(getActivity(),
					// ShippingOrderAct_.class);
					// intent.putExtra(ShippingOrderAct.SHIPPING_ORDER_ID__PARAM,
					// currentObject.getGuid());
					// startActivity(intent);
				}
			}
		});

		searchView.addTextChangedListener(adapter.getTextWatcher());

		getList();
	}

}
