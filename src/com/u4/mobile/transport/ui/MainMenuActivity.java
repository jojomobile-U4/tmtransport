package com.u4.mobile.transport.ui;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import org.androidannotations.annotations.*;

import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.base.ui.MainMenuSectionsPagerAdapter;
import com.u4.mobile.transport.R;

@EActivity(com.u4.mobile.R.layout.fragment_menu)
public class MainMenuActivity extends FragmentActivity {

	MainMenuSectionsPagerAdapter mainMenuSectionsPagerAdapter;
	private ViewPager mPager;
	private PagerAdapter mPagerAdapter;

	@AfterViews
	void setupView() {

		mPager = (ViewPager) findViewById(R.id.pager);
		mPagerAdapter = new MainMenuSectionsPagerAdapter(getSupportFragmentManager(), getFragments());
		mPager.setAdapter(mPagerAdapter);
		mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				invalidateOptionsMenu();
			}
		});
	}

	private List<FragmentBase> getFragments() {
		List<FragmentBase> list = new ArrayList<FragmentBase>();

		Bundle bundle = new Bundle();

		MainMenuModulesFragment_ fr1 = new MainMenuModulesFragment_();
		fr1.setArguments(bundle);
		list.add(fr1);

		AdministrationFrag_ fr = new AdministrationFrag_();
		fr.setArguments(bundle);
		list.add(fr);

		return list;
	}

}
