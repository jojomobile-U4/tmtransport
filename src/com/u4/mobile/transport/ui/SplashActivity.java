package com.u4.mobile.transport.ui;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;

import android.content.Intent;

import com.u4.mobile.base.ui.NavigationDrawerHolder;
import com.u4.mobile.base.ui.NavigationDrawerListItem;
import com.u4.mobile.scanner.ScennerTesterActivity_;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.ui.cashbox.CashBoxAct_;
import com.u4.mobile.transport.ui.shippingorders.ShippingOrderStopsAct_;

@EActivity(R.layout.splash_screen)
public class SplashActivity extends com.u4.mobile.base.ui.SplashActivity {

	@Bean
	NavigationDrawerHolder navigationDrawerHolder;

	@AfterViews
	@Background(delay = 400)
	public void runSplash() {

		navigationDrawerHolder.clear();

		// Start
		NavigationDrawerListItem navigationDrawerItem = new NavigationDrawerListItem(R.string.shipping_order_stops_act__title, R.drawable.nd_start, ShippingOrderStopsAct_.class);
		navigationDrawerHolder.addNavigationDrawerItem(navigationDrawerItem);

		// Cash box
		navigationDrawerItem = new NavigationDrawerListItem(R.string.cash_box_act__title, R.drawable.nd_cashbox, CashBoxAct_.class);
		navigationDrawerHolder.addNavigationDrawerItem(navigationDrawerItem);

		// Administration
		navigationDrawerItem = new NavigationDrawerListItem(R.string.administration_act__title, R.drawable.nd_administration, AdministrationAct_.class);
		navigationDrawerHolder.addNavigationDrawerItem(navigationDrawerItem);

		// Scanner test
		navigationDrawerItem = new NavigationDrawerListItem(R.string.core_adm_scanner_tester__fragment__title, R.drawable.nd_test_scanner, ScennerTesterActivity_.class);
		navigationDrawerHolder.addNavigationDrawerItem(navigationDrawerItem);

		final Intent intent = new Intent(getApplicationContext(), LoginAct_.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);

		finish();
	}
}
