package com.u4.mobile.transport.ui.incidents;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import org.androidannotations.annotations.*;

import com.u4.mobile.base.ui.ActivityBase;
import com.u4.mobile.transport.R;

@EActivity(R.layout.incident_act)
public class IncidentAct extends ActivityBase {

	public static final int RESULT_NUM = 1;

	public static final String INCIDENT_CONTEXT__PARAM = "incidentContext";
	public static final String INCIDENT_LINE_ID__PARAM = "incidentLineId";

	@Extra
	  String incidentContext;

	@Extra
	  long incidentLineId;

	@AfterViews
	void setupView() {
		Fragment fragment = IncidentFrag_.builder().incidentContext(incidentContext).incidentLineId(incidentLineId).build();
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.add(R.id.activity_standard__content, fragment).commit();
	}
}
