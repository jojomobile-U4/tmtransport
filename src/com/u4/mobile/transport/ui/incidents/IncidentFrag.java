package com.u4.mobile.transport.ui.incidents;

import java.sql.SQLException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.ViewById;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.DaoManager;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.DeliveryRejectionReason;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.utils.LogHelper;

@EFragment(R.layout.incident_frag)
public class IncidentFrag extends FragmentBase {

	private ShippingOrderLine currentLine;

	@ViewById(R.id.incident__header_text)
	protected TextView incidentHeaderText;

	@ViewById(R.id.incident__reason_description)
	protected EditText incidentReasonDescription;

	@FragmentArg(IncidentAct.INCIDENT_CONTEXT__PARAM)
	String incidentContext;

	@FragmentArg(IncidentAct.INCIDENT_LINE_ID__PARAM)
	long incidentLineId;

	@ViewById(R.id.incident__reason)
	Spinner reasonSpinner;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	Dao<ShippingOrderLine, Long> shippingOrderLineDao;

	DeliveryRejectionReason deliveryRejectionReason;

	@AfterViews()
	void setupView() {

		incidentHeaderText.setText(incidentContext);
		
		getReasons();
		setSpinnerReason();
		loadData();
	}

	@Background(serial = "Serial 1")
	void setSpinnerReason() {
		reasonSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				deliveryRejectionReason = (DeliveryRejectionReason) parent.getItemAtPosition(position);
				incidentReasonDescription.setText(deliveryRejectionReason.getDescription());

				Intent intent = new Intent();
				intent.putExtra(DeliveryRejectionReason.DESCRIPTION__FIELD_NAME, deliveryRejectionReason.getDescription());
				intent.putExtra(DownloadTransferObjectBase.ID__FIELD_NAME, deliveryRejectionReason.getId());

				getActivity().setResult(IncidentAct.RESULT_NUM, intent);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
	}

	@Background(serial = "Serial 1")
	void loadData() {
		if (getShippingOrderLine(incidentLineId) > 0) {
			if (currentLine == null) {
				return;
			}
			
			if (currentLine.getProdId() != null) {
				reasonSpinner.setSelection(getItemIndexById(currentLine.getProdId()));
			}
			
			incidentReasonDescription.setText(currentLine.getComments());
		}
	}

	private long getShippingOrderLine(long id) {
		try {
			QueryBuilder<ShippingOrderLine, Long> query = shippingOrderLineDao.queryBuilder();
			query.where().eq(DownloadTransferObjectBase.ID__FIELD_NAME, incidentLineId);

			currentLine = query.queryForFirst();
			
			if (currentLine != null) {
				return currentLine.getId();
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage("IncidentFrag.getShippingOrderLine(long id)", e);

		}
		
		return 0;
	}

	@Click(R.id.incident__save_button)
	void saveButton() {
		try {
			if (currentLine != null) {
				currentLine.setCanceled(true);
				currentLine.setComments(incidentReasonDescription.getText().toString());
				
				if (deliveryRejectionReason != null) {
					currentLine.setProdId(deliveryRejectionReason.getId());
				}
				
				currentLine.update();

				getActivity().setResult(DialogInterface.BUTTON_POSITIVE);
				getActivity().finish();
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage("IncidentFrag.saveButton()", e);
		}
	}

	@Background(serial = "Serial 1")
	protected void getReasons() {
		final ArrayAdapter<DeliveryRejectionReason> reasonAdapter = new ArrayAdapter<DeliveryRejectionReason>(getActivity(), com.u4.mobile.R.layout.spinner_item);

		Dao<DeliveryRejectionReason, Long> dao = DaoManager.lookupDao(DeliveryRejectionReason.class);

		try {
			QueryBuilder<DeliveryRejectionReason, Long> qb = dao.queryBuilder();
			qb.where().eq(DeliveryRejectionReason.UP_TO_DATE__FIELD_NAME, true);
			qb.orderBy(DeliveryRejectionReason.DESCRIPTION__FIELD_NAME, false);
			reasonAdapter.addAll(qb.query());
			reasonSpinner.setAdapter(reasonAdapter);
		} catch (SQLException e) {
			LogHelper.setErrorMessage("com.u4.mobile.transport.ui.incidents.IncidentFrag.getReasons()", e);
		}
	}

	public int getItemIndexById(long id) {
		for (int i = 0; i <= reasonSpinner.getCount() - 1; i++) {
			DeliveryRejectionReason reason = (DeliveryRejectionReason) reasonSpinner.getItemAtPosition(i);
			if (reason != null && reason.getId() == id) {
				return i;
			}
		}
		
		return 0;
	}
}
