package com.u4.mobile.transport.ui.incidents;

import java.sql.SQLException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.ui.DialogFragmentBase;
import com.u4.mobile.model.DaoManager;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.DeliveryRejectionReason;
import com.u4.mobile.utils.LogHelper;

@EFragment(R.layout.select_incident_dialog)
public class SelectIncidentDialog extends DialogFragmentBase {

	private OnSelectIncydentListener listener;

	@ViewById(R.id.select_incident__reason_description)
	protected EditText incidentReasonDescription;

	@ViewById(R.id.select_incident__reason)
	Spinner reasonSpinner;

	DeliveryRejectionReason deliveryRejectionReason;

	@UiThread
	public void populateView() {
		getDialog().setTitle(R.string.select_incident_act__title);

		reasonSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				deliveryRejectionReason = (DeliveryRejectionReason) parent.getItemAtPosition(position);
				incidentReasonDescription.setText(deliveryRejectionReason.getDescription());
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		deliveryRejectionReason = (DeliveryRejectionReason) reasonSpinner.getSelectedItem();

		if (deliveryRejectionReason != null) {
			incidentReasonDescription.setText(deliveryRejectionReason.getDescription());
		}
	}

	public void setListener(OnSelectIncydentListener listener) {
		this.listener = listener;
	}

	@AfterViews()
	@Background
	void onCreateView() {
		final ArrayAdapter<DeliveryRejectionReason> reasonAdapter = new ArrayAdapter<DeliveryRejectionReason>(getActivity(), com.u4.mobile.R.layout.spinner_item);

		Dao<DeliveryRejectionReason, Long> dao = DaoManager.lookupDao(DeliveryRejectionReason.class);

		try {
			QueryBuilder<DeliveryRejectionReason, Long> qb = dao.queryBuilder();
			qb.where().eq(DeliveryRejectionReason.UP_TO_DATE__FIELD_NAME, true);
			qb.orderBy(DeliveryRejectionReason.DESCRIPTION__FIELD_NAME, false);
			reasonAdapter.addAll(qb.query());
			reasonSpinner.setAdapter(reasonAdapter);
		} catch (SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		} finally {
			populateView();
		}
	}

	@Click(R.id.select_incident__save_button)
	void saveButton() {
		if (deliveryRejectionReason == null) {
			showDialogInfo(R.string.select_incident__no_reason_selected_msg);

			return;
		}

		if (!validateControl(incidentReasonDescription)) {
			return;
		}

		dismiss();

		if (listener != null) {
			listener.onSelectIncydent(deliveryRejectionReason.getId(), incidentReasonDescription.getText().toString());
		}
	}

	public static interface OnSelectIncydentListener {
		public void onSelectIncydent(Long reasonId, String reasonDescription);
	}
}
