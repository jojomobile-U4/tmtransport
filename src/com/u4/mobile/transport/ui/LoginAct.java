package com.u4.mobile.transport.ui;

import org.androidannotations.annotations.*;

import android.os.Bundle;

import com.u4.mobile.base.ui.StandardActivity;

@EActivity
public class LoginAct extends StandardActivity {

	@Override
	public void registerFragments() {
		addFragment(LoginFrag_.builder().build());
	}

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		if (getIntent().getBooleanExtra("EXIT", false)) {
			finish();
		}
	}

}
