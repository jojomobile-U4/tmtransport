package com.u4.mobile.transport.ui;

import java.util.Locale;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

import android.content.Intent;

import com.u4.mobile.R;
import com.u4.mobile.base.app.ApplicationInterfaces.IMainMenuFrafments;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.transport.app.MainApp;
import com.u4.mobile.transport.ui.cashbox.CashReportAct_;
import com.u4.mobile.transport.ui.shippingorders.ShippingOrdersAct_;

@EFragment(R.layout.fragment_main_menu_modules)
public class MainMenuModulesFragment extends FragmentBase implements IMainMenuFrafments {

	@Override
	public String getPageTitle() {

		return MainApp.getInstance().getApplicationContext().getResources().getString(R.string.main_menu__modules__tab_title).toUpperCase(Locale.getDefault());
	}

	@Click(com.u4.mobile.R.id.main_menu__cash_documents__button)
	@Background
	public void goToCashDocuments() {
		startActivity(new Intent(getActivity(), CashReportAct_.class));
	}

	@Click(com.u4.mobile.R.id.main_menu__shipping_orders__button)
	@Background
	public void showSynchronize() {
		startActivity(new Intent(getActivity(), ShippingOrdersAct_.class));
	}

}
