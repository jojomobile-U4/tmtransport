package com.u4.mobile.transport.ui;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.u4.mobile.R;
import com.u4.mobile.base.ui.NavigationDrawerActivity;

@EActivity
public class AdministrationAct extends NavigationDrawerActivity {

	public static final String GO_TO_START_ACT_AFTER_DOWNLOAD_DATABASE__PARAM = "goToStartActAfterDownloadDatabase";
	
	@Extra(GO_TO_START_ACT_AFTER_DOWNLOAD_DATABASE__PARAM)
	Boolean mGoToStartActAfterDownloadDatabase;
	
	@Override
	public void poplutateView() {
		super.poplutateView();

		Fragment fragment = AdministrationFrag_.builder().mGoToStartActAfterDownloadDatabase(mGoToStartActAfterDownloadDatabase).build();
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.add(R.id.navigation_drawer_frame_container, fragment).commit();
	}
}
