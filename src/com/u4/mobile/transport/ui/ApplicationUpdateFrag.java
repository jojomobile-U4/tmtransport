package com.u4.mobile.transport.ui;

import java.io.File;
import java.sql.SQLException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import com.u4.mobile.base.app.ApplicationEnums.SyncDirection;
import com.u4.mobile.base.app.MainApp;
import com.u4.mobile.base.helpers.NetworkHelper;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.UpdateInfo;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.services.RestHelper;
import com.u4.mobile.transport.utils.SynchronizationHelper;
import com.u4.mobile.utils.FileHelper;
import com.u4.mobile.utils.LogHelper;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.LinearLayout;
import android.widget.TextView;

@EFragment(R.layout.application_update_frag)
public class ApplicationUpdateFrag extends FragmentBase implements OnKeyListener {

	@Bean
	SynchronizationHelper mSynchronizationHelper;

	@Bean
	RestHelper mRestHelper;

	@FragmentArg(NotificationAct.APP_UPDATE_DATA__PARAM)
	UpdateInfo mUpdateInfo;

	@ViewById(R.id.application_update__header_information)
	TextView mHeaderInformation;

	@ViewById(R.id.application_update__new_available_version)
	TextView mAvailableVersion;

	@ViewById(R.id.application_update__current_version)
	TextView mCurrentVersion;

	@ViewById(R.id.application_update__button_install_later_layout)
	LinearLayout mButtonsInstalLaterLayout;

	@Click(R.id.application_update__button_install)
	public void buttonInstallClick() {
		if (SynchronizationHelper.existsDataToSend()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(getString(R.string.alert_dialog__information__title));
			builder.setMessage(getString(R.string.application_update__before_install_data_to_send_exists__msg));
			builder.setPositiveButton(getString(com.u4.mobile.R.string.yes), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					sendDataAndInstall();
				}
			});
			builder.setNegativeButton(getString(com.u4.mobile.R.string.no), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			AlertDialog alert = builder.create();
			alert.show();
		} else {
			install();
		}
	}

	@Click(R.id.application_update__button_install_later)
	public void buttonInstallLaterClick() {
		getActivity().finish();
	}

	@Background
	public void install() {
		setLoaderVisible(true, R.string.application_update__get_app_update_in_progress__msg);

		try {
			File updateAppFile = FileHelper.saveAppUpdateFile(MainApp.getInstance().getApplicationContext(), mRestHelper.getRestService().getUpdate(mUpdateInfo.getFileName()));

			if (updateAppFile != null && updateAppFile.exists()) {
				Intent i = new Intent();
				i.setAction(Intent.ACTION_VIEW);
				i.setDataAndType(Uri.fromFile(updateAppFile), "application/vnd.android.package-archive");
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);

				// Finish activity when do update application
				setLoaderVisible(false);
			}
		} catch (Exception e) {
			LogHelper.setErrorMessage(getClass().getName(), e);
			showDialogInfo(NetworkHelper.translateMessage(e));
			setLoaderVisible(false);
		}
	}

	@AfterViews
	public void onCreateView() {
		if (mUpdateInfo != null && mUpdateInfo.isRequired()) {
			mHeaderInformation.setText(R.string.application_update__header_information_required);
			mButtonsInstalLaterLayout.setVisibility(View.GONE);
		} else {
			mHeaderInformation.setText(R.string.application_update__header_information);
			mButtonsInstalLaterLayout.setVisibility(View.VISIBLE);
		}

		mCurrentVersion.setText(MainApp.getInstance().getAppVersion().get());
		mAvailableVersion.setText(mUpdateInfo.getVersion().get());

		getView().setFocusableInTouchMode(true);
		getView().setOnKeyListener(this);
	}

	public void quit() {
		int pid = android.os.Process.myPid();
		android.os.Process.killProcess(pid);
		System.exit(0);
	}

	@Background
	public void sendDataAndInstall() {
		setLoaderVisible(true, com.u4.mobile.R.string.adm_sync__sending_data__msg);

		try {
			if (mSynchronizationHelper.sendData()) {
				install();
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage(getClass().getName(), e);
			showDialogInfo(NetworkHelper.translateMessage(e, SyncDirection.Upload));
			setLoaderVisible(false);
		}
	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK && mUpdateInfo != null && mUpdateInfo.isRequired()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(getString(R.string.alert_dialog__information__title));
			builder.setMessage(getString(R.string.application_update__before_go_back__msg));
			builder.setPositiveButton(getString(com.u4.mobile.R.string.yes), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					getActivity().finish();
					Intent intent = LoginAct_.intent(MainApp.getInstance()).get();
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("EXIT", true);
					startActivity(intent);
				}
			});
			builder.setNegativeButton(getString(com.u4.mobile.R.string.no), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			AlertDialog alert = builder.create();
			alert.show();

			return true;
		}

		return false;
	}
}
