package com.u4.mobile.transport.ui.contractor;

import java.sql.SQLException;
import java.util.Date;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.OrmLiteDao;

import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.base.ui.ListAdapterBase;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.ContractorAccounts;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.DateTimeHelper.DateTimeFormat;
import com.u4.mobile.utils.LogHelper;

@EBean
public class ContractorAccountsListAdapter extends ListAdapterBase<ContractorAccounts> {

	class ViewHolder {
		TextView bottomLineTextView;
		TextView topLineTextView;
		TextView indicator;
		CheckBox checkbox;

		ViewHolder(View row) {
			topLineTextView = (TextView) row.findViewById(R.id.contractor_accounts_list__top_line);
			bottomLineTextView = (TextView) row.findViewById(R.id.contractor_accounts_list__bottom_line);
			indicator = (TextView) row.findViewById(R.id.contractor_accounts_list__indicator);
			checkbox = (CheckBox) row.findViewById(R.id.contractor_accounts_list__checkbox);
		}

		void populateFrom(ContractorAccounts item) {
			final String leftToPay = getContext().getString(R.string.contractor_accounts__left_to_pay);
			final String grossValue = getContext().getString(R.string.contractor_accounts__gross_value);
			final String paymentDate = getContext().getString(R.string.contractor_accounts__payment_date);

			if (isSelecting()) {
				checkbox.setVisibility(View.VISIBLE);
				checkbox.setChecked(getCheckedItems().contains(item));
				indicator.setVisibility(View.GONE);
			} else {
				checkbox.setVisibility(View.GONE);
				indicator.setVisibility(View.VISIBLE);

				if (DateTimeHelper.getDate(item.getMaturityDate().toString()).getTime() < new Date().getTime()) {
					indicator.setBackgroundResource(R.drawable.list_item_indicator_red);
				} else {
					indicator.setBackgroundResource(R.drawable.list_item_indicator_green);
				}
			}

			SpannableString spannable = new SpannableString(String.format("%s %s %.2f", item.getRndoSymbol(), leftToPay, item.getLeftToPay()));
			int start = 0;
			int stop = spannable.toString().indexOf(leftToPay);
			spannable.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), start, stop, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

			if (isSelecting()) {
				if (DateTimeHelper.getDate(item.getMaturityDate().toString()).getTime() < new Date().getTime()) {
					spannable.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.red)), start, stop, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
				} else {
					spannable.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.green)), start, stop, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
				}
			}

			start = stop;
			stop = stop + leftToPay.length();
			spannable.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.light_gray)), start, stop, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

			start = stop;
			stop = spannable.length();
			spannable.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), start, stop, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

			if (isSelecting()) {
				if (DateTimeHelper.getDate(item.getMaturityDate().toString()).getTime() < new Date().getTime()) {
					spannable.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.red)), start, stop, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
				} else {
					spannable.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.green)), start, stop, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
				}
			}

			topLineTextView.setText(spannable, BufferType.SPANNABLE);

			spannable = new SpannableString(String.format("%s %s %s %.2f", paymentDate,
					DateTimeHelper.getDateAsString(DateTimeHelper.getDate(item.getMaturityDate().toString()), DateTimeFormat.OracleDateFormat), grossValue, item.getValue()));

			start = spannable.toString().indexOf(paymentDate);
			stop = paymentDate.length();
			spannable.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.light_gray)), start, stop, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

			start = stop;
			stop = spannable.toString().indexOf(grossValue);
			spannable.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), start, stop, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

			start = stop;
			stop = stop + grossValue.length();
			spannable.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.light_gray)), start, stop, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

			start = stop;
			stop = spannable.length();
			spannable.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), start, stop, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

			bottomLineTextView.setText(spannable, BufferType.SPANNABLE);
		}
	}

	private String contractorGuid;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ContractorAccounts.class)
	Dao<ContractorAccounts, Long> contractorAccountsDao;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		try {
			if (convertView == null) {
				final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.contractor_accounts_list_view_item, parent, false);

				holder = new ViewHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.populateFrom(getItem(position));
		} catch (final Exception e) {
			LogHelper.setErrorMessage(ContractorAccountsListAdapter.class.getName(), e);
		}
		return convertView;
	}

	public void init(FragmentBase fragmentBase, String contractorGuid) {
		super.init(fragmentBase, 256L);
		this.contractorGuid = contractorGuid;
	}

	@Override
	public QueryBuilder<ContractorAccounts, Long> prepareQuery() {
		QueryBuilder<ContractorAccounts, Long> queryBuilder = contractorAccountsDao.queryBuilder();

		try {
			queryBuilder.where().notIn(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue()).and().eq(ContractorAccounts.PAYER_GUID__FIELD_NAME, contractorGuid).and()
					.ne(ContractorAccounts.KOR_LEFT_TO_PAY__FIELD_NAME, 0);
		} catch (SQLException e) {
			LogHelper.setErrorMessage(ContractorAccountsListAdapter.class.getName(), e);
		}

		return queryBuilder;
	}
}
