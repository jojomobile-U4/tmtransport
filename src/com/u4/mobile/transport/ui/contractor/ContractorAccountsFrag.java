package com.u4.mobile.transport.ui.contractor;

import java.sql.SQLException;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Intent;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;
import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.model.Contractor;
import com.u4.mobile.transport.model.ContractorAccounts;
import com.u4.mobile.transport.ui.cashbox.CashBoxHelper;
import com.u4.mobile.transport.ui.cashbox.CashDocumentGenerationAct;
import com.u4.mobile.transport.ui.cashbox.CashDocumentGenerationAct_;
import com.u4.mobile.utils.CalculationHelper;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.DateTimeHelper.DateTimeFormat;
import com.u4.mobile.utils.LogHelper;

@OptionsMenu(R.menu.contractor_accounts_menu)
@EFragment(R.layout.contractor_accounts_frag)
public class ContractorAccountsFrag extends FragmentBase {

	private static final int ACCOUNT_LIST_CASH_DOKUMENTS_GENERATION_CODE = 7;
	private Contractor currentObject = null;
	private float receivablesOnTimeSum = 0;
	private float receivablesExpiredSum = 0;
	private ActionMode actionMode;

	private final ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			final List<String> accountsGuids = adapter.getCheckedItemsGuids();

			if (accountsGuids.size() == 0) {
				showDialogInfo(getString(R.string.contractor_accounts_menu__not_selected_accounts__msg));

				return true;
			}

			goToGenerationCashDocuments(accountsGuids.toArray(new String[accountsGuids.size()]));
			mode.finish();

			return true;
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			menu.add(R.string.contractor_accounts_menu__issue_kp_kw);
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			actionMode = null;
			finishSelectMode();
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}
	};

	@OrmLiteDao(helper = DatabaseHelper.class, model = Contractor.class)
	Dao<Contractor, Long> contractorDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ContractorAccounts.class)
	Dao<ContractorAccounts, Long> contractorAccountsDao;

	@FragmentArg(ContractorAccountsAct.CONTRACTOR_ID__PARAM)
	Long contractorId;

	@ViewById(R.id.contractor_accounts__receivables_by)
	TextView receivablesByTextView;

	@ViewById(R.id.contractor_accounts__credit_limit)
	TextView creditLimitTextView;

	@ViewById(R.id.contractor_accounts__receivables_on_time)
	TextView receivablesOnTimeTextView;

	@ViewById(R.id.contractor_accounts__receivables_expired)
	TextView receivablesExpiredTextView;

	@ViewById(R.id.contractor_accounts__receivables_used)
	TextView receivablesUsedTextView;

	@ViewById(R.id.contractor_accounts__accounts_list)
	ListView listView;

	@ViewById(com.u4.mobile.R.id.list_view_empty_view)
	LinearLayout listViewEmptyView;

	@Bean
	ContractorAccountsListAdapter adapter;

	@Bean
	CashBoxHelper cashBoxHelper;

	public void finishSelectMode() {
		adapter.setSelecting(false);
		listView.invalidateViews();

		if (actionMode != null) {
			actionMode.finish();
		}
	}

	@OptionsItem(R.id.contractor_accounts_menu__issue_kp_kw)
	public boolean generateKpKwSelected() {
		if (adapter.getCount() == 0) {
			showDialogInfo(getString(R.string.contractor_accounts_menu__empty_account_list__msg));

			return true;
		}

		adapter.setSelecting(true);
		listView.invalidateViews();

		if (actionMode == null) {
			actionMode = getActivity().startActionMode(actionModeCallback);
		}

		return true;
	}

	@Background
	public void loadData() {
		if (contractorId == null || contractorId < 0) {
			return;
		}

		try {
			currentObject = contractorDao.queryForId(contractorId);

			if (currentObject != null) {
				QueryBuilder<ContractorAccounts, Long> summaryQueryBuilder = contractorAccountsDao.queryBuilder();

				summaryQueryBuilder.selectRaw(String.format("SUM(CASE WHEN Date(%s) < Date(%s) THEN %s ELSE 0 END) AS onTime, SUM(CASE WHEN Date(%s) >= Date(%s) THEN %s ELSE 0 END) AS expired",
						ContractorAccounts.MATURITY_DATE__FIELD_NAME, DateTimeHelper.getCurrentDateAsString(DateTimeFormat.OracleDateFormat), ContractorAccounts.KOR_LEFT_TO_PAY__FIELD_NAME,
						ContractorAccounts.MATURITY_DATE__FIELD_NAME, DateTimeHelper.getCurrentDateAsString(DateTimeFormat.OracleDateFormat), ContractorAccounts.KOR_LEFT_TO_PAY__FIELD_NAME));

				summaryQueryBuilder.where().notIn(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue()).and()
						.eq(ContractorAccounts.PAYER_GUID__FIELD_NAME, currentObject.getGuid()).and().ne(ContractorAccounts.KOR_LEFT_TO_PAY__FIELD_NAME, 0);

				String[] resultData = contractorAccountsDao.queryRaw(summaryQueryBuilder.prepareStatementString()).getResults().get(0);
				receivablesOnTimeSum = CalculationHelper.parseFloat(resultData[0] == null ? "0.00" : resultData[0]);
				receivablesExpiredSum = CalculationHelper.parseFloat(resultData[0] == null ? "0.00" : resultData[1]);
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage(ContractorAccountsFrag.class.getName(), e);
		}

		populateView();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);

		switch (requestCode) {
		case ACCOUNT_LIST_CASH_DOKUMENTS_GENERATION_CODE:
			if (resultCode == Activity.RESULT_OK) {
				if (getActivity().getIntent().getAction().equals(Intent.ACTION_INSERT)) {
					getActivity().setResult(Activity.RESULT_OK, intent);
					getActivity().finish();
				} else {
					loadData();
					listView.invalidate();
				}
			}
			break;
		}
	}

	@AfterViews
	public void onCreateView() {
		creditLimitTextView.setText(String.format("%.2f", 0.0));
		receivablesOnTimeTextView.setText(String.format("%.2f", 0.0));
		receivablesExpiredTextView.setText(String.format("%.2f", 0.0));
		receivablesUsedTextView.setText(String.format("%.2f", 0.0));

		listView.setAdapter(adapter);
		listView.setEmptyView(listViewEmptyView);
		listView.setItemsCanFocus(false);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> listAdapter, View view, int position, long id) {
				if (adapter.isSelecting()) {
					adapter.setItemChecked(position, !adapter.isItemChecked(position));
				}
			}
		});

		loadData();
	}

	@UiThread
	public void populateView() {
		adapter.init(this, currentObject != null ? currentObject.getGuid() : null);

		if (currentObject == null) {

			return;
		}

		adapter.reloadData(true, 0L);
		receivablesByTextView.setText(currentObject.getName());
		creditLimitTextView.setText(String.format("%.2f", currentObject.getCreditLimit()));
		receivablesOnTimeTextView.setText(String.format("%.2f", receivablesOnTimeSum));
		receivablesExpiredTextView.setText(String.format("%.2f", receivablesExpiredSum));
		receivablesUsedTextView.setText(String.format("%.2f", receivablesOnTimeSum + receivablesExpiredSum - currentObject.getCreditLimit()));
	}

	private void goToGenerationCashDocuments(String[] accountGuids) {
		final Intent intent = new Intent(getActivity(), CashDocumentGenerationAct_.class);
		intent.putExtra(CashDocumentGenerationAct.SOURCE_ACCOUNTS_GUID_ARRAY__PARAM, accountGuids);
		intent.putExtra(CashDocumentGenerationAct.CONTRACTOR_ID__PARAM, contractorId);
		intent.setAction(Intent.ACTION_INSERT);

		startActivityForResult(intent, ACCOUNT_LIST_CASH_DOKUMENTS_GENERATION_CODE);
	}
}
