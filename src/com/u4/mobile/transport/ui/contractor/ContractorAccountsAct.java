package com.u4.mobile.transport.ui.contractor;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.u4.mobile.R;
import com.u4.mobile.base.ui.ActivityBase;

@EActivity(R.layout.activity_standard)
public class ContractorAccountsAct extends ActivityBase {

	public static final String CONTRACTOR_ID__PARAM = "contractorId";

	@AfterViews
	public void onCreateView() {

		Long contractorId = -1L;
		Bundle data = getIntent().getExtras();

		if (data != null && data.containsKey(CONTRACTOR_ID__PARAM)) {
			contractorId = data.getLong(CONTRACTOR_ID__PARAM);
		}

		Fragment fragment = ContractorAccountsFrag_.builder().contractorId(contractorId).build();
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.add(R.id.activity_standard__content, fragment).commit();
	}
}
