package com.u4.mobile.transport.utils;

import java.sql.SQLException;
import java.util.List;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.u4.mobile.R;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;
import com.u4.mobile.base.helpers.NetworkHelper;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.transport.app.MainApp;
import com.u4.mobile.transport.model.CashDocument;
import com.u4.mobile.transport.model.CashDocumentImport;
import com.u4.mobile.transport.model.CashDocumentPosition;
import com.u4.mobile.transport.model.CashDocumentPositionImport;
import com.u4.mobile.transport.model.ShippingOrderLine;
import com.u4.mobile.transport.model.ShippingOrderLineImport;
import com.u4.mobile.transport.model.Stop;
import com.u4.mobile.transport.model.StopImport;
import com.u4.mobile.transport.services.RestHelper;
import com.u4.mobile.utils.AppParametersHelper;
import com.u4.mobile.utils.FileHelper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

@EBean(scope = Scope.Singleton)
public class SynchronizationHelper {

	public static boolean existsDataToSend() {
		Long dataToSendCount = ShippingOrderLine.getDataToSendCount() + Stop.getDataToSendCount() + CashDocument.getDataToSendCount() + CashDocumentPosition.getDataToSendCount();

		return dataToSendCount > 0;
	}

	@RootContext
	Context mContext;

	@Bean
	RestHelper mRestHelper;

	@Bean
	DatabaseHelper mDatabaseHelper;

	@Bean
	AppParametersHelper mAppParametersHelper;

	@OrmLiteDao(helper = DatabaseHelper.class, model = Stop.class)
	Dao<Stop, Long> mStopDao;

	@OrmLiteDao(helper = DatabaseHelper.class, model = ShippingOrderLine.class)
	Dao<ShippingOrderLine, Long> mShippingOrderLineDao;

	public boolean getInitDatabase(boolean showAlerts) throws Exception {
		if (!NetworkHelper.checkInternet()) {
			if (showAlerts) {
				showDialogInfo(com.u4.mobile.R.string.network__no_connection_to_internet);
			}

			return false;
		}

		if (mDatabaseHelper.databaseExists() && mDatabaseHelper.isVersionConsistent() && existsDataToSend()) {
			if (showAlerts) {
				showDialogInfo(com.u4.mobile.R.string.sync__init_database_data_to_send_exists__msg);
			}

			return false;
		}

		boolean result = FileHelper.saveNewDatabase(MainApp.getInstance().getApplicationContext(), mRestHelper.getRestService().getDatabaseGeneration());

		if (result) {
			mDatabaseHelper.init();
			MainApp.getInstance().initializeSessionInfo();
			mAppParametersHelper.refresh();
		}

		return result;
	}

	public boolean sendData() throws SQLException {
		return sendData(true);
	}

	public boolean sendData(boolean showAlerts) throws SQLException {
		if (!NetworkHelper.checkInternet()) {
			if (showAlerts) {
				showDialogInfo(com.u4.mobile.R.string.network__no_connection_to_internet);
			}

			return false;
		}

		if (!existsDataToSend()) {
			if (showAlerts) {
				showDialogInfo(com.u4.mobile.R.string.adm_sync__no_data_to_send__msg);
			}

			return false;
		}

		List<ShippingOrderLine> shippingOrderLinesToSend = ShippingOrderLine.getDataToSend();
		List<Stop> stopsToSend = Stop.getDataToSend();
		List<CashDocument> cashDocumentsToSend = CashDocument.getDataToSend();
		List<CashDocumentPosition> cashDocumentPositionsToSend = CashDocumentPosition.getDataToSend();

		// Send stops changes
		if (stopsToSend != null && stopsToSend.size() > 0) {
			for (Stop stopToSend : stopsToSend) {
				mRestHelper.getRestService().Stop(new StopImport(stopToSend));

				// Confirm sent changes in local database
				stopToSend.update(RecordStates.NoChange);
			}
		}

		// Send shipping order lines changes
		if (shippingOrderLinesToSend != null && shippingOrderLinesToSend.size() > 0) {
			for (ShippingOrderLine shippingOrderLineToSend : shippingOrderLinesToSend) {
				mRestHelper.getRestService().ShippingOrderLine(new ShippingOrderLineImport(shippingOrderLineToSend));

				// Confirm sent changes in local database
				shippingOrderLineToSend.update(RecordStates.NoChange);
			}
		}

		// Send cash document changes
		if (cashDocumentsToSend != null && cashDocumentsToSend.size() > 0) {
			for (CashDocument cashDocumentToSend : cashDocumentsToSend) {
				mRestHelper.getRestService().CashDocument(new CashDocumentImport(cashDocumentToSend));

				// Confirm sent changes in local database
				cashDocumentToSend.update(RecordStates.NoChange);
			}
		}

		// Send cash document positions changes
		if (cashDocumentPositionsToSend != null && cashDocumentPositionsToSend.size() > 0) {
			for (CashDocumentPosition cashDocumentPositionToSend : cashDocumentPositionsToSend) {
				mRestHelper.getRestService().CashDocumentPosition(new CashDocumentPositionImport(cashDocumentPositionToSend));

				// Confirm sent changes in local database
				cashDocumentPositionToSend.update(RecordStates.NoChange);
			}
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean sendDataByShorId(Long shorId) throws SQLException {
		QueryBuilder<Stop, Long> stpsQueryBuilder = mStopDao.queryBuilder();
		Where<Stop, Long> stpsWhere = stpsQueryBuilder.where();

		stpsWhere.eq(Stop.SHOR_ID_FIELD_NAME, shorId).and().or(stpsWhere.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Updated.getValue()),
				stpsWhere.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Inserted.getValue()));

		List<Stop> stopsToSend = stpsQueryBuilder.query();

		QueryBuilder<ShippingOrderLine, Long> shoiQueryBuilder = mShippingOrderLineDao.queryBuilder();
		Where<ShippingOrderLine, Long> shoiWhere = shoiQueryBuilder.where();

		shoiWhere.eq(ShippingOrderLine.SHOR_ID_FIELD_NAME, shorId).and().or(shoiWhere.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Updated.getValue()),
				shoiWhere.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Inserted.getValue()));

		List<ShippingOrderLine> shippingOrderLinesToSend = shoiQueryBuilder.query();

		if ((stopsToSend == null || stopsToSend.size() == 0) && (shippingOrderLinesToSend == null || shippingOrderLinesToSend.size() == 0)) {
			showDialogInfo(com.u4.mobile.R.string.adm_sync__no_data_to_send__msg);

			return false;
		}

		if (stopsToSend != null && stopsToSend.size() > 0) {
			// Send stops changes
			for (Stop stopToSend : stopsToSend) {
				mRestHelper.getRestService().Stop(new StopImport(stopToSend));

				// Confirm sent changes in local database
				stopToSend.update(RecordStates.NoChange);
			}
		}

		if (shippingOrderLinesToSend != null && shippingOrderLinesToSend.size() > 0) {
			// Send shipping order lines changes
			for (ShippingOrderLine shippingOrderLineToSend : shippingOrderLinesToSend) {
				mRestHelper.getRestService().ShippingOrderLine(new ShippingOrderLineImport(shippingOrderLineToSend));

				// Confirm sent changes in local database
				shippingOrderLineToSend.update(RecordStates.NoChange);
			}
		}

		return true;
	}

	@UiThread
	public void showDialogInfo(int resId) {
		AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
		alertDialog.setTitle(mContext.getString(R.string.app_name));
		alertDialog.setMessage(mContext.getString(resId));
		alertDialog.setIcon(R.drawable.app_logo);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", (DialogInterface.OnClickListener) null);
		alertDialog.show();
	}

}
