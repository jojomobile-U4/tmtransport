package com.u4.mobile.transport.services;

import org.androidannotations.annotations.rest.Accept;
import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Post;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.MediaType;
import org.androidannotations.api.rest.RestClientRootUrl;
import org.androidannotations.api.rest.RestClientSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.u4.mobile.base.services.GsonHttpMessageConverter;
import com.u4.mobile.model.SessionInfo;
import com.u4.mobile.model.UpdateInfo;
import com.u4.mobile.transport.model.CashDocumentImport;
import com.u4.mobile.transport.model.CashDocumentPositionImport;
import com.u4.mobile.transport.model.ShippingOrderLineImport;
import com.u4.mobile.transport.model.StopImport;

@Rest(converters = { GsonHttpMessageConverter.class, ByteArrayHttpMessageConverter.class })
public interface RestServices extends RestClientRootUrl, RestClientSupport {

	@Post("Finances/CashReports/CashDocumentsImport/")
	public void CashDocument(CashDocumentImport cashDocumentImport);

	@Post("Finances/CashReports/CashDocumentPositionsImport/")
	public void CashDocumentPosition(CashDocumentPositionImport cashDocumentPositionsImport);

	@Accept(MediaType.APPLICATION_OCTET_STREAM)
	@Get("Config/DatabaseGeneration?systems=TRANSPORT")
	public byte[] getDatabaseGeneration();
	
	@Accept(MediaType.APPLICATION_OCTET_STREAM)
	@Get("Config/Updates/download?platform=ANDROID&fileName={fileName}")
	public byte[] getUpdate(String fileName);

	@Override
	public RestTemplate getRestTemplate();

	@Accept(MediaType.APPLICATION_JSON)
	@Get("Config/SessionInfo")
	public ResponseEntity<SessionInfo> getSessionInfo();
	
	@Accept(MediaType.APPLICATION_JSON)
	@Get("Config/Updates?platform=ANDROID")
	public ResponseEntity<UpdateInfo> getUpdateInfo();

	@Override
	public void setRootUrl(String rootUrl);

	@Post("Logistics/Transport/ShippingOrderLinesImport/")
	public void ShippingOrderLine(ShippingOrderLineImport shippingOrderLineImport);

	@Post("Logistics/Transport/StopsImport/")
	public void Stop(StopImport stopImport);
}
