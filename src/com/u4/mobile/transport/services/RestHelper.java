package com.u4.mobile.transport.services;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.rest.RestService;
import org.androidannotations.annotations.sharedpreferences.Pref;

import com.u4.mobile.base.app.AppSettings_;
import com.u4.mobile.base.services.RestHelperBase;
import com.u4.mobile.transport.app.MainApp;

@EBean(scope = Scope.Singleton)
public class RestHelper extends RestHelperBase {

	@RestService
	RestServices restServices;

	@Pref
	public AppSettings_ appSettings;

	public RestServices getRestService() {
		return restServices;
	}

	public void initialize() {
		init(restServices);

		setupRestClient(MainApp.getInstance().getLogin(), MainApp.getInstance().getPassword(), appSettings.serviceUrl().get(), appSettings.proxyUrl().getOr(null), appSettings.serverCertificatePath()
				.get());
	}

	public void reinitialize() {
		init(restServices);
		resetRestClient(MainApp.getInstance().getLogin(), MainApp.getInstance().getPassword(), appSettings.serviceUrl().get(), appSettings.proxyUrl().getOr(null), appSettings.serverCertificatePath()
				.get());
	}

	public void reinitialize(String login, String password) {
		init(restServices);
		resetRestClient(login, password, appSettings.serviceUrl().get(), appSettings.proxyUrl().getOr(null), appSettings.serverCertificatePath().get());
	}

	@AfterViews
	public void startUp() {
		initialize();
	}
}
