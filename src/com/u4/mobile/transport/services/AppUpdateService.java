package com.u4.mobile.transport.services;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.ServiceAction;
import org.androidannotations.annotations.UiThread;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.u4.mobile.base.helpers.NetworkHelper;
import com.u4.mobile.model.UpdateInfo;
import com.u4.mobile.transport.R;
import com.u4.mobile.transport.app.MainApp;
import com.u4.mobile.transport.ui.NotificationAct;
import com.u4.mobile.transport.ui.NotificationAct_;
import com.u4.mobile.utils.LogHelper;

@EIntentService
public class AppUpdateService extends IntentService {

	@Bean
	public RestHelper restHelper;

	public AppUpdateService() {
		super("AppUpdateService");
	}

	@ServiceAction
	void checkForAppUpdate() {
		if (!NetworkHelper.checkInternet()) {
			return;
		}

		try {
			ResponseEntity<UpdateInfo> responseEntity = restHelper.getRestService().getUpdateInfo();

			if (responseEntity != null && responseEntity.hasBody() && responseEntity.getStatusCode() == HttpStatus.OK) {
				UpdateInfo updateInfo = responseEntity.getBody();

				if (updateInfo != null && updateInfo.getVersion().compareTo(MainApp.getInstance().getAppVersion()) > 0) {
					if (updateInfo.isRequired()) {
						goToUpdate(updateInfo);
					} else {
						showNotify(updateInfo);
					}
				}
			}
		} catch (Exception e) {
			LogHelper.setErrorMessage(getClass().getName() + " - checkForUpdateExists", e);
		}
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
	}

	public void goToUpdate(UpdateInfo updateInfo) {
		Intent targetIntent = NotificationAct_.intent(this).get();
		targetIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		targetIntent.putExtra(NotificationAct.APP_UPDATE_IS_AVAILABLE__PARAM, true);
		targetIntent.putExtra(NotificationAct.APP_UPDATE_DATA__PARAM, updateInfo);
		startActivity(targetIntent);
	}
	
	public void showNotify(UpdateInfo updateInfo) {
		// Creates an Intent for the Activity
		Intent targetIntent = new Intent();
		targetIntent.setComponent(new ComponentName(this, NotificationAct_.class));
		// Sets the Activity to start in a new, empty task
		targetIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		targetIntent.putExtra(NotificationAct.APP_UPDATE_IS_AVAILABLE__PARAM, true);
		targetIntent.putExtra(NotificationAct.APP_UPDATE_DATA__PARAM, updateInfo);
		// Creates the PendingIntent
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, targetIntent, Intent.FLAG_ACTIVITY_CLEAR_TOP | PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(MainApp.getInstance())
			.setSmallIcon(R.drawable.ic_launcher)
			.setContentTitle(getString(R.string.app_name))
			.setContentText(getString(R.string.application_update__header_information))
			.setAutoCancel(true)
			.setContentIntent(contentIntent);

		// Builds an anonymous Notification object from the builder, and
		// passes it to the NotificationManager
		NotificationManager nManager = (NotificationManager) MainApp.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
		nManager.notify(1, builder.build());
	}

	@UiThread
	public void showToast() {
		Toast.makeText(MainApp.getInstance(), getString(R.string.application_update__header_information), Toast.LENGTH_LONG).show();
	}
}
