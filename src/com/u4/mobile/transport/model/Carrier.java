package com.u4.mobile.transport.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.u4.mobile.model.DownloadTransferObjectBase;

@DatabaseTable(tableName = "Carrier")
public class Carrier extends DownloadTransferObjectBase {

	public transient static final String NOSN_ID_FIELD_NAME = "NosnId";
	public transient static final String NOSN_SYMBOL_FIELD_NAME = "NosnSymbol";
	public transient static final String SSCC_FIELD_NAME = "Sscc";
	public transient static final String SYMBOL_FIELD_NAME = "Symbol";
	public transient static final String NAME = "Carrier";
	public transient static final String TABLE_PREFIX = "NOSN";

	private static final long serialVersionUID = 1L;
	public static final long PackageSize = 256L;

	@SerializedName(NOSN_ID_FIELD_NAME)
	@DatabaseField(columnName = NOSN_ID_FIELD_NAME, canBeNull = true)
	private Long nosnId;

	@SerializedName(NOSN_SYMBOL_FIELD_NAME)
	@DatabaseField(columnName = NOSN_SYMBOL_FIELD_NAME, canBeNull = true)
	private String nosnSymbol;

	@SerializedName(SSCC_FIELD_NAME)
	@DatabaseField(columnName = SSCC_FIELD_NAME, canBeNull = true)
	private String sscc;

	@SerializedName(SYMBOL_FIELD_NAME)
	@DatabaseField(columnName = SYMBOL_FIELD_NAME, canBeNull = false)
	private String symbol;

	public Carrier() {
		super();
	}

	public Long getNosnId() {
		return nosnId;
	}

	public void setNosnId(Long nosnId) {
		this.nosnId = nosnId;
	}

	public String getNosnSymbol() {
		return nosnSymbol;
	}

	public void setNosnSymbol(String nosnSymbol) {
		this.nosnSymbol = nosnSymbol;
	}

	public String getSscc() {
		return sscc;
	}

	public void setSscc(String sscc) {
		this.sscc = sscc;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
}
