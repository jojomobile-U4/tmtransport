package com.u4.mobile.transport.model;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.DatabaseTable;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;
import com.u4.mobile.model.DaoManager;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.model.UploadTransferObjectBase;
import com.u4.mobile.utils.LogHelper;

@DatabaseTable(tableName = "CashDocumentPosition")
public class CashDocumentPosition extends UploadTransferObjectBase {
	public static final String KSDK_GUID_FIELD_NAME = "KsdkGuid";
	public static final String TITLE_FIELD_NAME = "Title";
	public static final String SINV_GUID_FIELD_NAME = "SinvGuid";
	public static final String INCOME_FIELD_NAME = "Income";
	public static final String EXPENDITURE_FIELD_NAME = "Expenditure";
	public static final String RNWP_GUID_FIELD_NAME = "RnwpGuid";
	public static final String NAME = "CashDocumentPosition";
	public static final String TABLE_PREFIX = "KSPD";

	private static final long serialVersionUID = 1L;
	public transient static final long PackageSize = 256L;

	@SerializedName(KSDK_GUID_FIELD_NAME)
	@DatabaseField(columnName = KSDK_GUID_FIELD_NAME, canBeNull = false, foreign = true, foreignColumnName = GUID__FIELD_NAME, foreignAutoRefresh = true, index = true)
	private CashDocument cashDocument;

	@SerializedName(TITLE_FIELD_NAME)
	@DatabaseField(columnName = TITLE_FIELD_NAME, canBeNull = true)
	private String title;

	@SerializedName(SINV_GUID_FIELD_NAME)
	@DatabaseField(columnName = SINV_GUID_FIELD_NAME, canBeNull = true)
	private UUID sinvGuid;

	@SerializedName(INCOME_FIELD_NAME)
	@DatabaseField(columnName = INCOME_FIELD_NAME, canBeNull = true)
	private double income;

	@SerializedName(EXPENDITURE_FIELD_NAME)
	@DatabaseField(columnName = EXPENDITURE_FIELD_NAME, canBeNull = true)
	private double expenditure;

	@SerializedName(RNWP_GUID_FIELD_NAME)
	@DatabaseField(columnName = RNWP_GUID_FIELD_NAME, canBeNull = true, foreign = true, foreignColumnName = GUID__FIELD_NAME)
	private ContractorAccounts contractorAccounts;

	public CashDocumentPosition() {
		super();
		setGuid(UUID.randomUUID().toString());
	}

	public static List<CashDocumentPosition> getDataToSend() {

		final Dao<CashDocumentPosition, Long> dao = DaoManager.lookupDao(CashDocumentPosition.class);

		if (dao == null) {
			return null;
		}

		QueryBuilder<CashDocumentPosition, Long> queryBuilder = dao.queryBuilder();

		try {
			queryBuilder.where().eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Updated.getValue()).or()
					.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Inserted.getValue());

			List<CashDocumentPosition> dataToSend = queryBuilder.query();

			if (dataToSend != null) {
				return dataToSend;
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage("CashDocument.getDataForSync()", e);
		}

		return null;
	}

	public static Long getDataToSendCount() {

		final Dao<CashDocumentPosition, Long> dao = DaoManager.lookupDao(CashDocumentPosition.class);
		QueryBuilder<CashDocumentPosition, Long> queryBuilder = dao.queryBuilder();

		try {
			queryBuilder.where().eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Updated.getValue()).or()
					.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Inserted.getValue()).or()
					.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.getValue());

			return queryBuilder.countOf();
		} catch (SQLException e) {
			LogHelper.setErrorMessage("CashDocument.getDataForSync()", e);
		}

		return 0L;
	}

	public CashDocument getCashDocument() {
		return cashDocument;
	}

	public ContractorAccounts getContractorAccounts() {
		return contractorAccounts;
	}

	public double getExpenditure() {
		return expenditure;
	}

	public double getIncome() {
		return income;
	}

	public UUID getSinvGuid() {
		return sinvGuid;
	}

	public String getTitle() {
		return title;
	}

	public double getValue() {
		double value = 0.0;

		if (getCashDocument() != null) {
			switch (getCashDocument().getDocumentType()) {
			case KP:
				value = getIncome();
				break;
			case KW:
				value = getExpenditure();
				break;
			default:
				value = 0.0;
			}
		}

		return value;
	}

	public void setCashDocument(CashDocument cashDocument) {
		this.cashDocument = cashDocument;
	}

	public void setContractorAccounts(ContractorAccounts customerAccounts) {
		this.contractorAccounts = customerAccounts;
	}

	public void setExpenditure(double expenditure) {
		this.expenditure = expenditure;
	}

	public void setIncome(double income) {
		this.income = income;
	}

	public void setSinvGuid(UUID sinvGuid) {
		this.sinvGuid = sinvGuid;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setValue(double value) {
		if (getCashDocument() != null) {

			switch (getCashDocument().getDocumentType()) {
			case KP:
				setIncome(value);
			case KW:
				setExpenditure(value);
			default:
				break;
			}
		}
	}
}
