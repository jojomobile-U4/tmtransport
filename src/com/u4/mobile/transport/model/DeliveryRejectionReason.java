package com.u4.mobile.transport.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.u4.mobile.model.DownloadTransferObjectBase;

@DatabaseTable(tableName = "DeliveryRejectionReason")
public class DeliveryRejectionReason extends DownloadTransferObjectBase {
	public static final String DESCRIPTION__FIELD_NAME = "Description";
	public static final String PARTY_RESPONSIBLE__FIELD_NAME = "PartyResponsible";
	public static final String UP_TO_DATE__FIELD_NAME = "UpToDate";
	public static final String NAME = "DeliveryRejectionReason";
	public static final String TABLE_PREFIX = "PROD";

	private static final long serialVersionUID = 1L;
	public transient static final long PackageSize = 256L;

	@SerializedName(DESCRIPTION__FIELD_NAME)
	@DatabaseField(columnName = DESCRIPTION__FIELD_NAME, canBeNull = false)
	private String description;

	@SerializedName(PARTY_RESPONSIBLE__FIELD_NAME)
	@DatabaseField(columnName = PARTY_RESPONSIBLE__FIELD_NAME, canBeNull = true)
	private String partyResponsible;

	@SerializedName(UP_TO_DATE__FIELD_NAME)
	@DatabaseField(columnName = UP_TO_DATE__FIELD_NAME, canBeNull = true, defaultValue = "0")
	private boolean upToDate;

	public DeliveryRejectionReason() {
		super();
	}

	public String getDescription() {
		return description;
	}

	public String getPartyResponsible() {
		return partyResponsible;
	}

	public boolean isUpToDate() {
		return upToDate;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setPartyResponsible(String partyResponsible) {
		this.partyResponsible = partyResponsible;
	}

	public void setUpToDate(boolean upToDate) {
		this.upToDate = upToDate;
	}
	
	@Override
	public String toString() {
		return description;
	}
}
