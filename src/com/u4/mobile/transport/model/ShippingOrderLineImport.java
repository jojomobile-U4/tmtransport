package com.u4.mobile.transport.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.u4.mobile.model.UploadTransferObjectBase;

public class ShippingOrderLineImport extends UploadTransferObjectBase implements Serializable {

	public transient static final String CANCELLATION_DATE_FIELD_NAME = "CancellationDate";
	public transient static final String CANCELED_FIELD_NAME = "Cancelled";
	public transient static final String COMMENTS_FIELD_NAME = "Comments";
	public transient static final String DELIVERED_FIELD_NAME = "Delivered";
	public transient static final String DELIVERY_DATE_FIELD_NAME = "DeliveryDate";
	public transient static final String LOADED_FIELD_NAME = "Loaded";
	public transient static final String LOADING_DATE_FIELD_NAME = "LoadingDate";
	public transient static final String OBJECT_GUID__FIELD_NAME = "ObjectGuid";
	public transient static final String PROD_ID_FIELD_NAME = "ProdId";
	public transient static final String SHOI_GUID__FIELD_NAME = "ShoiGuid";
	public transient static final String SHOI_GUID_PREV__FIELD_NAME = "ShoiGuidPrev";
	public transient static final String SHOR_ID_FIELD_NAME = "ShorId";
	public transient static final String SOURCE_DOC_GUID_FIELD_NAME = "SourceDocGuid";
	public transient static final String SOURCE_DOC_NUMBER_FIELD_NAME = "SourceDocNumber";
	public transient static final String STPS_GUID_LOADING_FIELD_NAME = "StpsGuidLoading";
	public transient static final String STPS_GUID_UNLOADING_FIELD_NAME = "StpsGuidUnloading";

	private static final long serialVersionUID = 1L;

	@SerializedName(CANCELLATION_DATE_FIELD_NAME)
	private String cancellationDate;

	@SerializedName(CANCELED_FIELD_NAME)
	public Boolean canceled;

	@SerializedName(COMMENTS_FIELD_NAME)
	public String comments;

	@SerializedName(DELIVERED_FIELD_NAME)
	public Boolean delivered;

	@SerializedName(DELIVERY_DATE_FIELD_NAME)
	private String deliveryDate;

	@SerializedName(LOADED_FIELD_NAME)
	public Boolean loaded;

	@SerializedName(LOADING_DATE_FIELD_NAME)
	private String loadingDate;

	@SerializedName(OBJECT_GUID__FIELD_NAME)
	private String objectGuid;

	@SerializedName(PROD_ID_FIELD_NAME)
	public Long prodId;

	@SerializedName(SHOI_GUID__FIELD_NAME)
	private String shoiGuid;

	@SerializedName(SHOI_GUID_PREV__FIELD_NAME)
	private String shoiGuidPrev;

	@SerializedName(SHOR_ID_FIELD_NAME)
	public Long shorId;

	@SerializedName(SOURCE_DOC_GUID_FIELD_NAME)
	public String sourceDocGuid;

	@SerializedName(SOURCE_DOC_NUMBER_FIELD_NAME)
	public String sourceDocNumber;

	@SerializedName(STPS_GUID_LOADING_FIELD_NAME)
	public String stpsGuidLoading;

	@SerializedName(STPS_GUID_UNLOADING_FIELD_NAME)
	public String stpsGuidUnloading;

	public ShippingOrderLineImport(ShippingOrderLine transferObject) {
		super(transferObject);

		setObjectGuid(transferObject.getGuid());
		setCancellationDate(transferObject.getCancellationDate());
		setCanceled(transferObject.isCanceled());
		setComments(transferObject.getComments());
		setDelivered(transferObject.isDelivered());
		setDeliveryDate(transferObject.getDeliveryDate());
		setLoaded(transferObject.isLoaded());
		setLoadingDate(transferObject.getLoadingDate());
		setProdId(transferObject.getProdId());
		setShoiGuid(transferObject.getShoiGuid());
		setShoiGuidPrev(transferObject.getShoiGuidPrev());
		setShorId(transferObject.getShorId());
		setSourceDocGuid(transferObject.getSourceDocGuid());
		setSourceDocNumber(transferObject.getSourceDocNumber());

		if (transferObject.getStopLoading() != null) {
			setStpsGuidLoading(transferObject.getStopLoading().getGuid());
		}

		if (transferObject.getStopUnloading() != null) {
			setStpsGuidUnloading(transferObject.getStopUnloading().getGuid());
		}
	}

	public Boolean getCanceled() {
		return canceled;
	}

	public String getCancellationDate() {
		return cancellationDate;
	}

	public String getComments() {
		return comments;
	}

	public Boolean getDelivered() {
		return delivered;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public Boolean getLoaded() {
		return loaded;
	}

	public String getLoadingDate() {
		return loadingDate;
	}

	public String getObjectGuid() {
		return objectGuid;
	}

	public Long getProdId() {
		return prodId;
	}

	public String getShoiGuid() {
		return shoiGuid;
	}

	public String getShoiGuidPrev() {
		return shoiGuidPrev;
	}

	public Long getShorId() {
		return shorId;
	}

	public String getSourceDocGuid() {
		return sourceDocGuid;
	}

	public String getSourceDocNumber() {
		return sourceDocNumber;
	}

	public String getStpsGuidLoading() {
		return stpsGuidLoading;
	}

	public String getStpsGuidUnloading() {
		return stpsGuidUnloading;
	}

	public void setCanceled(Boolean canceled) {
		this.canceled = canceled;
	}

	public void setCancellationDate(String cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public void setDelivered(Boolean delivered) {
		this.delivered = delivered;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public void setLoaded(Boolean loaded) {
		this.loaded = loaded;
	}

	public void setLoadingDate(String loadingDate) {
		this.loadingDate = loadingDate;
	}

	public void setObjectGuid(String objectGuid) {
		this.objectGuid = objectGuid;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public void setShoiGuid(String shoiGuid) {
		this.shoiGuid = shoiGuid;
	}

	public void setShoiGuidPrev(String shoiGuidPrev) {
		this.shoiGuidPrev = shoiGuidPrev;
	}

	public void setShorId(Long shorId) {
		this.shorId = shorId;
	}

	public void setSourceDocGuid(String sourceDocGuid) {
		this.sourceDocGuid = sourceDocGuid;
	}

	public void setSourceDocNumber(String sourceDocNumber) {
		this.sourceDocNumber = sourceDocNumber;
	}

	public void setStpsGuidLoading(String stpsGuidLoading) {
		this.stpsGuidLoading = stpsGuidLoading;
	}

	public void setStpsGuidUnloading(String stpsGuidUnloading) {
		this.stpsGuidUnloading = stpsGuidUnloading;
	}
}
