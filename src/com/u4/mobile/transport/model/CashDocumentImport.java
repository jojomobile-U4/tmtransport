package com.u4.mobile.transport.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.u4.mobile.model.UploadTransferObjectBase;

public class CashDocumentImport extends UploadTransferObjectBase implements Serializable {

	public static final String KSRK_GUID_FIELD_NAME = "KsrkGuid";
	public static final String DOCUMENT_TYPE_FIELD_NAME = "DocumentType";
	public static final String DOCUMENT_NUMBER_FIELD_NAME = "DocumentNumber";
	public static final String DOCUMENT_SYMBOL_FIELD_NAME = "DocumentSymbol";
	public static final String DOCUMENT_DATE_FIELD_NAME = "DocumentDate";
	public static final String KONR_GUID_FIELD_NAME = "KonrGuid";
	public static final String DESCRIPTION_FIELD_NAME = "Description";

	private static final long serialVersionUID = 1L;

	@SerializedName(DESCRIPTION_FIELD_NAME)
	private String description;

	@SerializedName(DOCUMENT_DATE_FIELD_NAME)
	private String documentDate;

	@SerializedName(DOCUMENT_NUMBER_FIELD_NAME)
	private long documentNumber;

	@SerializedName(DOCUMENT_SYMBOL_FIELD_NAME)
	private String documentSymbol;

	@SerializedName(DOCUMENT_TYPE_FIELD_NAME)
	private String documentType;

	@SerializedName(KONR_GUID_FIELD_NAME)
	private String konrGuid;

	@SerializedName(KSRK_GUID_FIELD_NAME)
	private String ksrkGuid;

	public CashDocumentImport(CashDocument transferObject) {
		super(transferObject);

		setGuid(transferObject.getGuid());
		setDescription(transferObject.getDescription());
		setDocumentDate(transferObject.getDocumentDate());
		setDocumentNumber(transferObject.getDocumentNumber());
		setDocumentSymbol(transferObject.getDocumentSymbol());
		setDocumentType(transferObject.getDocumentType().toString());

		if (transferObject.getContractor() != null) {
			setKonrGuid(transferObject.getContractor().getGuid());
		}

		setKsrkGuid(transferObject.getKsrkGuid());
	}

	public String getDescription() {
		return description;
	}

	public String getDocumentDate() {
		return documentDate;
	}

	public long getDocumentNumber() {
		return documentNumber;
	}

	public String getDocumentSymbol() {
		return documentSymbol;
	}

	public String getDocumentType() {
		return documentType;
	}

	public String getKonrGuid() {
		return konrGuid;
	}

	public String getKsrkGuid() {
		return ksrkGuid;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}

	public void setDocumentNumber(long documentNumber) {
		this.documentNumber = documentNumber;
	}

	public void setDocumentSymbol(String documentSymbol) {
		this.documentSymbol = documentSymbol;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public void setKonrGuid(String konrGuid) {
		this.konrGuid = konrGuid;
	}

	public void setKsrkGuid(String ksrkGuid) {
		this.ksrkGuid = ksrkGuid;
	}
}
