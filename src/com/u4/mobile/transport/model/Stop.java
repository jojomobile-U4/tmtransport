package com.u4.mobile.transport.model;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.DatabaseTable;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;
import com.u4.mobile.model.DaoHelper;
import com.u4.mobile.model.DaoManager;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.model.UploadTransferObjectBase;
import com.u4.mobile.transport.model.ShippingOrderLine.TransferState;
import com.u4.mobile.utils.LogHelper;

@DatabaseTable(tableName = "Stop")
public class Stop extends UploadTransferObjectBase {

	public transient static final String ADGE_LATITUDE_FIELD_NAME = "AdgeLatitude";
	public transient static final String ADGE_LONGITUDE_FIELD_NAME = "AdgeLongitude";
	public transient static final String DESCRIPTION_FIELD_NAME = "Description";
	public transient static final String LOCA_ID__FIELD_NAME = "LocaId";
	public transient static final String LOCA_LOCATION_ADDRESS_FIELD_NAME = "LocaLocationAddress";
	public transient static final String LOCA_LOCATION_NAME_FIELD_NAME = "LocaLocationName";
	public transient static final String LOCA_LOCATION_TYPE_FIELD_NAME = "LocaLocationType";
	public transient static final String LOCA_LOCATION_SOURCE_ID__FIELD_NAME = "LocaLocationSourceId";
	public transient static final String SHOR_ID_FIELD_NAME = "ShorId";
	public transient static final String STOP_SEQUENCE_FIELD_NAME = "StopSequence";
	public transient static final String TRANSFER_STATE__FIELD_NAME = "TransferState";
	public transient static final String NAME = "Stop";
	public transient static final String TABLE_PREFIX = "STPS";

	public static final long PackageSize = 256L;
	private static final long serialVersionUID = 1L;

	@SerializedName(ADGE_LATITUDE_FIELD_NAME)
	@DatabaseField(columnName = ADGE_LATITUDE_FIELD_NAME, canBeNull = true)
	private double adgeLatitude;

	@SerializedName(ADGE_LONGITUDE_FIELD_NAME)
	@DatabaseField(columnName = ADGE_LONGITUDE_FIELD_NAME, canBeNull = true)
	private double adgeLongitude;

	@SerializedName(DESCRIPTION_FIELD_NAME)
	@DatabaseField(columnName = DESCRIPTION_FIELD_NAME, canBeNull = true)
	private String description;

	@SerializedName(LOCA_ID__FIELD_NAME)
	@DatabaseField(columnName = LOCA_ID__FIELD_NAME, canBeNull = true)
	private Long locaId;

	@SerializedName(LOCA_LOCATION_ADDRESS_FIELD_NAME)
	@DatabaseField(columnName = LOCA_LOCATION_ADDRESS_FIELD_NAME, canBeNull = true)
	private String locaLocationAddress;

	@SerializedName(LOCA_LOCATION_NAME_FIELD_NAME)
	@DatabaseField(columnName = LOCA_LOCATION_NAME_FIELD_NAME, canBeNull = true)
	private String locaLocationName;

	@SerializedName(LOCA_LOCATION_TYPE_FIELD_NAME)
	@DatabaseField(columnName = LOCA_LOCATION_TYPE_FIELD_NAME, canBeNull = true)
	private String locaLocationType;

	@SerializedName(LOCA_LOCATION_SOURCE_ID__FIELD_NAME)
	@DatabaseField(columnName = LOCA_LOCATION_SOURCE_ID__FIELD_NAME, canBeNull = true)
	private Long locaLocationSourceId;

	@SerializedName(SHOR_ID_FIELD_NAME)
	@DatabaseField(columnName = SHOR_ID_FIELD_NAME, canBeNull = true)
	private Long shorId;

	@SerializedName(STOP_SEQUENCE_FIELD_NAME)
	@DatabaseField(columnName = STOP_SEQUENCE_FIELD_NAME, canBeNull = true)
	public Long stopSequence;

	@DatabaseField(columnName = TRANSFER_STATE__FIELD_NAME, canBeNull = true, defaultValue = "0")
	private int transferState;

	private Boolean done;

	public Stop() {
		super();
	}

	public static List<Stop> getDataToSend() {

		final Dao<Stop, Long> dao = DaoManager.lookupDao(Stop.class);

		if (dao == null) {
			return null;
		}

		QueryBuilder<Stop, Long> queryBuilder = dao.queryBuilder();

		try {
			queryBuilder.where().eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Updated.getValue()).or()
					.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Inserted.getValue());

			List<Stop> dataToSend = queryBuilder.query();

			if (dataToSend != null) {
				return dataToSend;
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage("Stop.getDataForSync()", e);
		}

		return null;
	}

	public static Long getDataToSendCount() {

		final Dao<Stop, Long> dao = DaoManager.lookupDao(Stop.class);
		QueryBuilder<Stop, Long> queryBuilder = dao.queryBuilder();

		try {
			queryBuilder.where().eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Updated.getValue()).or()
					.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Inserted.getValue()).or()
					.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.getValue());

			return queryBuilder.countOf();
		} catch (SQLException e) {
			LogHelper.setErrorMessage("CashDocument.getDataForSync()", e);
		}

		return 0L;
	}

	public double getAdgeLatitude() {
		return adgeLatitude;
	}

	public double getAdgeLongitude() {
		return adgeLongitude;
	}

	public String getDescription() {
		return description;
	}

	public Long getLocaId() {
		return locaId;
	}

	public String getLocaLocationAddress() {
		return locaLocationAddress;
	}

	public String getLocaLocationName() {
		return locaLocationName;
	}

	public Long getLocaLocationSourceId() {
		return locaLocationSourceId;
	}

	public String getLocaLocationType() {
		return locaLocationType;
	}

	public Long getShorId() {
		return shorId;
	}

	public Long getStopSequence() {
		return stopSequence;
	}

	public Boolean isDone() {
		return done;
	}

	public void setAdgeLatitude(double adgeLatitude) {
		this.adgeLatitude = adgeLatitude;
	}

	public void setAdgeLongitude(double adgeLongitude) {
		this.adgeLongitude = adgeLongitude;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDone(Boolean done) {
		this.done = done;
	}

	public void setLocaId(Long locaId) {
		this.locaId = locaId;
	}

	public void setLocaLocationAddress(String locaLocationAddress) {
		this.locaLocationAddress = locaLocationAddress;
	}

	public void setLocaLocationName(String locaLocationName) {
		this.locaLocationName = locaLocationName;
	}

	public void setLocaLocationSourceId(Long locaLocationSourceId) {
		this.locaLocationSourceId = locaLocationSourceId;
	}

	public void setLocaLocationType(String locaLocationType) {
		this.locaLocationType = locaLocationType;
	}

	public void setShorId(Long shorId) {
		this.shorId = shorId;
	}

	public void setStopSequence(Long stopSequence) {
		this.stopSequence = stopSequence;
	}

	@Override
	public int update() throws SQLException {
		setRecordState(RecordStates.Updated);

		return super.update();
	}

	public TransferState getTransferState() {
		return TransferState.values()[transferState];
	}

	public void setTransferState(TransferState transferState) {
		this.transferState = transferState.intValue();
	}

	public void setTransferStateValue(int transferStateValue) {
		this.transferState = transferStateValue;
	}

	public void setTransferingState() throws SQLException {
		this.transferState = TransferState.Transfering.intValue();
		update(RecordStates.NoChange);
	}

	public static void undoTransferingByShorId(Long shorId) throws SQLException {
		final Dao<Stop, Long> stopDao = DaoManager.lookupDao(Stop.class);

		HashMap<String, Object> queryParam = new HashMap<String, Object>();
		queryParam.put("Stop", NAME);
		queryParam.put("TransferState", TRANSFER_STATE__FIELD_NAME);
		queryParam.put("TransferStateValue", TransferState.NotTransfered.getValue());
		queryParam.put("TransferStateFilter", TransferState.Transfering.getValue());
		queryParam.put("ShorId", SHOR_ID_FIELD_NAME);
		queryParam.put("ShorIdValue", shorId);

		StringBuilder query = new StringBuilder();
		query.append("UPDATE %(Stop) ");
		query.append("   SET %(TransferState) = %(TransferStateValue) ");
		query.append(" WHERE %(ShorId) = %(ShorIdValue)");
		query.append("   AND %(TransferState) = '%(TransferStateFilter)'");

		stopDao.updateRaw(DaoHelper.formatQuery(query.toString(), queryParam));
	}

	public static void undoTransfering(String stpsGuid) throws SQLException {
		final Dao<Stop, Long> stopDao = DaoManager.lookupDao(Stop.class);

		Stop stop = DaoManager.queryForGuid(stpsGuid, stopDao);

		if (stop != null) {
			stop.setTransferState(TransferState.NotTransfered);
			stop.update(RecordStates.NoChange);
		}
	}
}
