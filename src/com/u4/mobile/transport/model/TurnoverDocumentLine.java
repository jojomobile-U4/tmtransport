package com.u4.mobile.transport.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.u4.mobile.model.UploadTransferObjectBase;

@DatabaseTable(tableName = "TurnoverDocumentLine")
public class TurnoverDocumentLine extends UploadTransferObjectBase {

	public transient static final String DOOB_ID_FIELD_NAME = "DoobId";
	public transient static final String INMA_ID_FIELD_NAME = "InmaId";
	public transient static final String NO_FIELD_NAME = "No";
	public transient static final String INMA_SYMBOL_FIELD_NAME = "InmaSymbol";
	public transient static final String INMA_NAME_FIELD_NAME = "InmaName";
	public transient static final String INMA_BASE_UNIT_FIELD_NAME = "InmaBaseUnit";
	public transient static final String QUANTITY_FIELD_NAME = "Quantity";
	public transient static final String NAME = "TurnoverDocumentLine";
	public transient static final String TABLE_PREFIX = "DOBI";

	private static final long serialVersionUID = 1L;
	public static final long PackageSize = 256L;
	
	@SerializedName(DOOB_ID_FIELD_NAME)
	@DatabaseField(columnName = DOOB_ID_FIELD_NAME, canBeNull = false)
	public Long doobId;
	
	@SerializedName(NO_FIELD_NAME)
	@DatabaseField(columnName = NO_FIELD_NAME, canBeNull = false)
	public Long no;
	
	@SerializedName(INMA_ID_FIELD_NAME)
	@DatabaseField(columnName = INMA_ID_FIELD_NAME, canBeNull = false)
	public Long inmaId;
	
	@SerializedName(INMA_SYMBOL_FIELD_NAME)
	@DatabaseField(columnName = INMA_SYMBOL_FIELD_NAME, canBeNull = true)
	private String inmaSymbol;
	
	@SerializedName(INMA_NAME_FIELD_NAME)
	@DatabaseField(columnName = INMA_NAME_FIELD_NAME, canBeNull = true)
	private String inmaName;
	
	@SerializedName(INMA_BASE_UNIT_FIELD_NAME)
	@DatabaseField(columnName = INMA_BASE_UNIT_FIELD_NAME, canBeNull = false)
	private String inmaBaseUnit;	
	
	@SerializedName(QUANTITY_FIELD_NAME)
	@DatabaseField(columnName = QUANTITY_FIELD_NAME, canBeNull = false)
	private double quantity;
	
	public TurnoverDocumentLine(){
		super();
	}

	public Long getDoobId() {
		return doobId;
	}

	public void setDoobId(Long doobId) {
		this.doobId = doobId;
	}

	public Long getNo() {
		return no;
	}

	public void setNo(Long no) {
		this.no = no;
	}

	public Long getInmaId() {
		return inmaId;
	}

	public void setInmaId(Long inmaId) {
		this.inmaId = inmaId;
	}

	public String getInmaSymbol() {
		return inmaSymbol;
	}

	public void setInmaSymbol(String inmaSymbol) {
		this.inmaSymbol = inmaSymbol;
	}

	public String getInmaName() {
		return inmaName;
	}

	public void setInmaName(String inmaName) {
		this.inmaName = inmaName;
	}

	public String getInmaBaseUnit() {
		return inmaBaseUnit;
	}

	public void setInmaBaseUnit(String inmaBaseUnit) {
		this.inmaBaseUnit = inmaBaseUnit;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
}
