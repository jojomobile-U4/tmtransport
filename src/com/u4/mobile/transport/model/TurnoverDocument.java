package com.u4.mobile.transport.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.u4.mobile.model.DownloadTransferObjectBase;

@DatabaseTable(tableName = "TurnoverDocument")
public class TurnoverDocument extends DownloadTransferObjectBase {

	public static final String SYMBOL_FIELD_NAME = "Symbol";
	public static final String NAME = "TurnoverDocument";
	public static final String TABLE_PREFIX = "DOOB";
	private static final long serialVersionUID = 1L;
	public static final long PackageSize = 256L;
	
	@SerializedName(SYMBOL_FIELD_NAME)
	@DatabaseField(columnName = SYMBOL_FIELD_NAME, canBeNull = false)
	private String symbol;
	
	public TurnoverDocument() {
		super();
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
}
