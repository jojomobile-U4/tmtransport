package com.u4.mobile.transport.model;

import java.util.UUID;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.u4.mobile.model.DownloadTransferObjectBase;

@DatabaseTable(tableName = "ContractorAccounts")
public class ContractorAccounts extends DownloadTransferObjectBase {

	public transient static final String KOR_LEFT_TO_PAY__FIELD_NAME = "KorLeftToPay";
	public transient static final String MATURITY_DATE__FIELD_NAME = "MaturityDate";
	public transient static final String PAYER_GUID__FIELD_NAME = "PayerGuid";
	public transient static final String RECIPIENT_GUID__FIELD_NAME = "RecipientGuid";
	public transient static final String RNDO_GUID__FIELD_NAME = "RndoGuid";
	public transient static final String RNDO_SYMBOL__FIELD_NAME = "RndoSymbol";
	public transient static final String SALE_DATE__FIELD_NAME = "SaleDate";
	public transient static final String VALUE__FIELD_NAME = "Value";
	public transient static final String SINV_GUID__FIELD_NAME = "SinvGuid";
	public transient static final String NAME = "ContractorAccounts";
	public transient static final String TABLE_PREFIX = "RNWP";

	private static final long serialVersionUID = 1L;
	public transient static final long PackageSize = 256L;

	@SerializedName(KOR_LEFT_TO_PAY__FIELD_NAME)
	@DatabaseField(columnName = KOR_LEFT_TO_PAY__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private float korLeftToPay;

	@SerializedName(MATURITY_DATE__FIELD_NAME)
	@DatabaseField(columnName = MATURITY_DATE__FIELD_NAME, canBeNull = false, defaultValue = "0", index = true)
	private String maturityDate;

	@SerializedName(PAYER_GUID__FIELD_NAME)
	@DatabaseField(columnName = PAYER_GUID__FIELD_NAME, canBeNull = true, foreign = true, foreignColumnName = GUID__FIELD_NAME, index = true)
	private Contractor payer;

	private transient String payerName;

	@SerializedName(RECIPIENT_GUID__FIELD_NAME)
	@DatabaseField(columnName = RECIPIENT_GUID__FIELD_NAME, canBeNull = true, foreign = true, foreignColumnName = GUID__FIELD_NAME, index = true)
	private Contractor recipient;

	@SerializedName(RNDO_GUID__FIELD_NAME)
	@DatabaseField(columnName = RNDO_GUID__FIELD_NAME, canBeNull = true)
	private UUID rndoGuid;

	@SerializedName(RNDO_SYMBOL__FIELD_NAME)
	@DatabaseField(columnName = RNDO_SYMBOL__FIELD_NAME, canBeNull = false)
	private String rndoSymbol;

	@SerializedName(SALE_DATE__FIELD_NAME)
	@DatabaseField(columnName = SALE_DATE__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private long saleDate;

	@SerializedName(SINV_GUID__FIELD_NAME)
	@DatabaseField(columnName = SINV_GUID__FIELD_NAME, canBeNull = true)
	private UUID sinvGuid;

	@SerializedName(VALUE__FIELD_NAME)
	@DatabaseField(columnName = VALUE__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private float value;

	public ContractorAccounts() {
		super();
	}

	public float getLeftToPay() {
		return korLeftToPay;
	}

	public String getMaturityDate() {
		return maturityDate;
	}

	public Contractor getPayer() {
		return payer;
	}

	public String getPayerName() {
		return payerName;
	}

	public Contractor getRecipient() {
		return recipient;
	}

	public UUID getRndoGuid() {
		return rndoGuid;
	}

	public String getRndoSymbol() {
		return rndoSymbol;
	}

	public long getSaleDate() {
		return saleDate;
	}

	public UUID getSinvGuid() {
		return sinvGuid;
	}

	public float getValue() {
		return value;
	}

	public void setLeftToPay(float korLeftToPay) {
		this.korLeftToPay = korLeftToPay;
	}

	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}

	public void setPayer(Contractor payer) {
		this.payer = payer;
	}

	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	public void setRecipient(Contractor recipient) {
		this.recipient = recipient;
	}

	public void setRndoGuid(UUID rndoGuid) {
		this.rndoGuid = rndoGuid;
	}

	public void setRndoSymbol(String rndoSymbol) {
		this.rndoSymbol = rndoSymbol;
	}

	public void setSaleDate(long saleDate) {
		this.saleDate = saleDate;
	}

	public void setSinvGuid(UUID sinvGuid) {
		this.sinvGuid = sinvGuid;
	}

	public void setValue(float value) {
		this.value = value;
	}
}
