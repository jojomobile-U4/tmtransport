package com.u4.mobile.transport.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.u4.mobile.model.DownloadTransferObjectBase;

@DatabaseTable(tableName = "CashReport")
public class CashReport extends DownloadTransferObjectBase {

	public static enum ReportState {
		B, Z, N
	}

	public static final String DATE_FROM_FIELD_NAME = "DateFrom";
	public static final String DATE_TO_FIELD_NAME = "DateTo";
	public static final String STATUS_FIELD_NAME = "Status";
	public static final String OPEN_STATE_FIELD_NAME = "OpenState";
	public static final String CLOSE_STATE_FIELD_NAME = "CloseState";
	public static final String NAME = "CashReport";
	public static final String TABLE_PREFIX = "KSRK";

	private static final long serialVersionUID = 1L;
	public transient static final long PackageSize = 256L;

	@SerializedName("DateFrom")
	@DatabaseField(columnName = DATE_FROM_FIELD_NAME, canBeNull = false)
	private String dateFrom;

	@SerializedName("DateTo")
	@DatabaseField(columnName = DATE_TO_FIELD_NAME, canBeNull = true)
	private String dateTo;

	@SerializedName("Status")
	@DatabaseField(columnName = STATUS_FIELD_NAME, dataType = DataType.ENUM_STRING, canBeNull = true)
	private ReportState status;

	@SerializedName("OpenState")
	@DatabaseField(columnName = OPEN_STATE_FIELD_NAME, canBeNull = false, defaultValue = "0")
	private double openState;

	@SerializedName("CloseState")
	@DatabaseField(columnName = CLOSE_STATE_FIELD_NAME, canBeNull = true)
	private double closeState;

	public CashReport() {
		super();
	}

	public double getCloseState() {
		return closeState;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public double getOpenState() {
		return openState;
	}

	public ReportState getStatus() {
		return status;
	}

	public void setCloseState(double closeState) {
		this.closeState = closeState;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public void setOpenState(double openState) {
		this.openState = openState;
	}

	public void setStatus(ReportState status) {
		this.status = status;
	}
}
