package com.u4.mobile.transport.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.u4.mobile.model.UploadTransferObjectBase;

public class CashDocumentPositionImport extends UploadTransferObjectBase implements Serializable {
	public static final String KSDK_GUID_FIELD_NAME = "KsdkGuid";
	public static final String TITLE_FIELD_NAME = "Title";
	public static final String SINV_GUID_FIELD_NAME = "SinvGuid";
	public static final String INCOME_FIELD_NAME = "Income";
	public static final String EXPENDITURE_FIELD_NAME = "Expenditure";
	public static final String RNWP_GUID_FIELD_NAME = "RnwpGuid";

	private static final long serialVersionUID = 1L;

	@SerializedName(EXPENDITURE_FIELD_NAME)
	private double expenditure;

	@SerializedName(INCOME_FIELD_NAME)
	private double income;

	@SerializedName(KSDK_GUID_FIELD_NAME)
	private String ksdkGuid;

	@SerializedName(RNWP_GUID_FIELD_NAME)
	private String rnwpGuid;

	@SerializedName(SINV_GUID_FIELD_NAME)
	private String sinvGuid;

	@SerializedName(TITLE_FIELD_NAME)
	private String title;

	public CashDocumentPositionImport(CashDocumentPosition transferObject) {
		super(transferObject);

		setGuid(transferObject.getGuid());
		setExpenditure(transferObject.getExpenditure());
		setIncome(transferObject.getIncome());
		setKsdkGuid(transferObject.getCashDocument().getGuid());

		if (transferObject.getContractorAccounts() != null) {
			setRnwpGuid(transferObject.getContractorAccounts().getGuid());
		}

		setSinvGuid(transferObject.getSinvGuid().toString());
		setTitle(transferObject.getTitle());
	}

	public double getExpenditure() {
		return expenditure;
	}

	public double getIncome() {
		return income;
	}

	public String getKsdkGuid() {
		return ksdkGuid;
	}

	public String getRnwpGuid() {
		return rnwpGuid;
	}

	public String getSinvGuid() {
		return sinvGuid;
	}

	public String getTitle() {
		return title;
	}

	public void setExpenditure(double expenditure) {
		this.expenditure = expenditure;
	}

	public void setIncome(double income) {
		this.income = income;
	}

	public void setKsdkGuid(String ksdkGuid) {
		this.ksdkGuid = ksdkGuid;
	}

	public void setRnwpGuid(String rnwpGuid) {
		this.rnwpGuid = rnwpGuid;
	}

	public void setSinvGuid(String sinvGuid) {
		this.sinvGuid = sinvGuid;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
