package com.u4.mobile.transport.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.u4.mobile.model.DownloadTransferObjectBase;

@DatabaseTable(tableName = "ShippingOrder")
public class ShippingOrder extends DownloadTransferObjectBase {

	public static final String ROUT_TYPE_FIELD_NAME = "RoutType";

	public static final String ROUT_ROUTE_NUMBER_FIELD_NAME = "RoutRouteNumber";
	public static final String OSOB_ID_FIELD_NAME = "OsobId";
	public static final String OSOB_FULL_NAME_FIELD_NAME = "OsobFullName";
	public static final String FORWARDING_DATE_FIELD_NAME = "ForwardingDate";
	public static final String DOC_NUMBER_FIELD_NAME = "DocNumber";
	public static final String DOC_DATE_FIELD_NAME = "DocDate";
	public static final String DESCRIPTION_FIELD_NAME = "Description";
	public static final String NAME = "ShippingOrder";
	public static final String TABLE_PREFIX = "SHOR";
	private static final long serialVersionUID = 1L;

	public static final long PackageSize = 256L;
	
	@SerializedName(DESCRIPTION_FIELD_NAME)
	@DatabaseField(columnName = DESCRIPTION_FIELD_NAME, canBeNull = true)
	private String description;

	@SerializedName(DOC_DATE_FIELD_NAME)
	@DatabaseField(columnName = DOC_DATE_FIELD_NAME, canBeNull = false)
	private String docDate;

	@SerializedName(DOC_NUMBER_FIELD_NAME)
	@DatabaseField(columnName = DOC_NUMBER_FIELD_NAME, canBeNull = true)
	private String docNumber;

	@SerializedName(FORWARDING_DATE_FIELD_NAME)
	@DatabaseField(columnName = FORWARDING_DATE_FIELD_NAME, canBeNull = true)
	private String forwardingDate;

	@SerializedName(OSOB_FULL_NAME_FIELD_NAME)
	@DatabaseField(columnName = OSOB_FULL_NAME_FIELD_NAME, canBeNull = true)
	private String osobFullName;

	@SerializedName(OSOB_ID_FIELD_NAME)
	@DatabaseField(columnName = OSOB_ID_FIELD_NAME, canBeNull = true)
	private Long osobId;

	@SerializedName(ROUT_ROUTE_NUMBER_FIELD_NAME)
	@DatabaseField(columnName = ROUT_ROUTE_NUMBER_FIELD_NAME, canBeNull = true)
	private String routRouteNumber;

	@SerializedName("RoutT02")
	@DatabaseField(columnName = ROUT_TYPE_FIELD_NAME, canBeNull = true)
	private String routType;

	public ShippingOrder() {
		super();
	}

	public String getDescription() {
		return description;
	}

	public String getDocDate() {
		return docDate;
	}

	// @ForeignCollectionField
	// private ForeignCollection<ShippingOrderLine> shippingOrderLine;

	public String getDocNumber() {
		return docNumber;
	}

	public String getForwardingDate() {
		return forwardingDate;
	}

	public String getOsobFullName() {
		return osobFullName;
	}

	public Long getOsobId() {
		return osobId;
	}

	public String getRoutRouteNumber() {
		return routRouteNumber;
	}

	public String getRoutType() {
		return routType;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDocDate(String docDate) {
		this.docDate = docDate;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public void setForwardingDate(String forwardingDate) {
		this.forwardingDate = forwardingDate;
	}

	public void setOsobFullName(String osobFullName) {
		this.osobFullName = osobFullName;
	}

	public void setOsobId(Long osobId) {
		this.osobId = osobId;
	}

	public void setRoutRouteNumber(String routRouteNumber) {
		this.routRouteNumber = routRouteNumber;
	}

	public void setRoutType(String routType) {
		this.routType = routType;
	}

	public static enum RoutType {
		line("Liniowa"), courier("Kurierska");

		private final String value;

		RoutType(String value) {

			this.value = value;
		}

		public String getValue() {

			return value;
		}
	}

}
