package com.u4.mobile.transport.model;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.springframework.util.StringUtils;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.table.DatabaseTable;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;
import com.u4.mobile.model.DaoHelper;
import com.u4.mobile.model.DaoManager;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.model.UploadTransferObjectBase;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.DateTimeHelper.DateTimeFormat;
import com.u4.mobile.utils.LogHelper;

@DatabaseTable(tableName = "ShippingOrderLine")
public class ShippingOrderLine extends UploadTransferObjectBase {
	public transient static final String SOURCE_DOC_ID_FIELD_NAME = "SourceDocId";
	public transient static final String SOURCE_DOC_NUMBER_FIELD_NAME = "SourceDocNumber";
	public transient static final String SOURCE_DOC_TYPE_FIELD_NAME = "SourceDocType";
	public transient static final String SOURCE_DOC_ADDITIONAL_DATA_FIELD_NAME = "SourceDocAdditionalData";
	public transient static final String SOURCE_DOC_SUBTYPE_FIELD_NAME = "SourceDocSubtype";
	public transient static final String SOURCE_DOC_GUID_FIELD_NAME = "SourceDocGuid";
	public transient static final String SHOI_GUID__FIELD_NAME = "ShoiGuid";
	public transient static final String SHOI_GUID_PREV__FIELD_NAME = "ShoiGuidPrev";
	public transient static final String SHOR_ID_FIELD_NAME = "ShorId";
	public transient static final String STPS_GUID_LOADING_FIELD_NAME = "StpsGuidLoading";
	public transient static final String STPS_GUID_UNLOADING_FIELD_NAME = "StpsGuidUnloading";
	public transient static final String PRIO_CODE_FIELD_NAME = "PrioCode";
	public transient static final String DESCRIPTION_FIELD_NAME = "Description";
	public transient static final String DELIVERED_FIELD_NAME = "Delivered";
	public transient static final String DELIVERY_DATE_FIELD_NAME = "DeliveryDate";
	public transient static final String CANCELED_FIELD_NAME = "Canceled";
	public transient static final String CANCELLATION_DATE_FIELD_NAME = "CancellationDate";
	public transient static final String LOADED_FIELD_NAME = "Loaded";
	public transient static final String LOADING_DATE_FIELD_NAME = "LoadingDate";
	public transient static final String COMMENTS_FIELD_NAME = "Comments";
	public transient static final String ROZL_TYPE_FIELD_NAME = "RozlType";
	public transient static final String PROD_ID_FIELD_NAME = "ProdId";
	public transient static final String DONE_FIELD_NAME = "Done";
	public transient static final String TRANSFER_STATE__FIELD_NAME = "TransferState";
	public transient static final String DOOB_SYMBOL_FIELD_NAME = "DoobSymbol";
	public transient static final String SINV_SYMBOL_FIELD_NAME = "SinvSymbol";
	public transient static final String SORD_SYMBOL_STATE__FIELD_NAME = "SordSymbol";
	public transient static final String NAME = "ShippingOrderLine";
	public transient static final String TABLE_PREFIX = "SHOI";

	private static final long serialVersionUID = 1L;
	public static final long PackageSize = 256L;

	@SerializedName(CANCELED_FIELD_NAME)
	@DatabaseField(columnName = CANCELED_FIELD_NAME, canBeNull = true, defaultValue = "0")
	public Boolean canceled;

	@SerializedName(CANCELLATION_DATE_FIELD_NAME)
	@DatabaseField(columnName = CANCELLATION_DATE_FIELD_NAME, canBeNull = true)
	private String cancellationDate;

	@SerializedName(COMMENTS_FIELD_NAME)
	@DatabaseField(columnName = COMMENTS_FIELD_NAME, canBeNull = true)
	public String comments;

	@SerializedName(DELIVERED_FIELD_NAME)
	@DatabaseField(columnName = DELIVERED_FIELD_NAME, canBeNull = true, defaultValue = "0")
	public Boolean delivered;

	@SerializedName(DELIVERY_DATE_FIELD_NAME)
	@DatabaseField(columnName = DELIVERY_DATE_FIELD_NAME, canBeNull = true)
	private String deliveryDate;

	@SerializedName(DESCRIPTION_FIELD_NAME)
	@DatabaseField(columnName = DESCRIPTION_FIELD_NAME, canBeNull = true)
	public String description;

	@SerializedName(LOADED_FIELD_NAME)
	@DatabaseField(columnName = LOADED_FIELD_NAME, canBeNull = true, defaultValue = "0")
	public Boolean loaded;

	@SerializedName(LOADING_DATE_FIELD_NAME)
	@DatabaseField(columnName = LOADING_DATE_FIELD_NAME, canBeNull = true)
	private String loadingDate;

	@SerializedName(PRIO_CODE_FIELD_NAME)
	@DatabaseField(columnName = PRIO_CODE_FIELD_NAME, canBeNull = true)
	public String prioCode;

	@SerializedName(PROD_ID_FIELD_NAME)
	@DatabaseField(columnName = PROD_ID_FIELD_NAME, canBeNull = true)
	public Long prodId;

	@SerializedName(ROZL_TYPE_FIELD_NAME)
	@DatabaseField(columnName = ROZL_TYPE_FIELD_NAME, canBeNull = true)
	private String rozlType;

	@SerializedName(SHOI_GUID__FIELD_NAME)
	@DatabaseField(columnName = SHOI_GUID__FIELD_NAME, canBeNull = true)
	private String shoiGuid;

	@DatabaseField(columnName = SHOI_GUID_PREV__FIELD_NAME, canBeNull = true)
	private String shoiGuidPrev;

	@SerializedName(SHOR_ID_FIELD_NAME)
	@DatabaseField(columnName = SHOR_ID_FIELD_NAME, canBeNull = false)
	public Long shorId;

	@SerializedName(STPS_GUID_LOADING_FIELD_NAME)
	@DatabaseField(columnName = STPS_GUID_LOADING_FIELD_NAME, canBeNull = false, foreign = true, foreignColumnName = GUID__FIELD_NAME)
	public Stop stopLoading;

	@SerializedName(STPS_GUID_UNLOADING_FIELD_NAME)
	@DatabaseField(columnName = STPS_GUID_UNLOADING_FIELD_NAME, canBeNull = false, foreign = true, foreignColumnName = GUID__FIELD_NAME)
	public Stop stopUnloading;

	@SerializedName(SOURCE_DOC_ID_FIELD_NAME)
	@DatabaseField(columnName = SOURCE_DOC_ID_FIELD_NAME, canBeNull = false)
	public Long sourceDocId;

	@SerializedName(SOURCE_DOC_ADDITIONAL_DATA_FIELD_NAME)
	@DatabaseField(columnName = SOURCE_DOC_ADDITIONAL_DATA_FIELD_NAME, canBeNull = true)
	public String sourceDocAdditionalData;

	@SerializedName(SOURCE_DOC_SUBTYPE_FIELD_NAME)
	@DatabaseField(columnName = SOURCE_DOC_SUBTYPE_FIELD_NAME, canBeNull = true)
	public String sourceDocSubtype;

	@DatabaseField(columnName = SOURCE_DOC_GUID_FIELD_NAME, canBeNull = true)
	public String sourceDocGuid;

	@SerializedName(SOURCE_DOC_NUMBER_FIELD_NAME)
	@DatabaseField(columnName = SOURCE_DOC_NUMBER_FIELD_NAME, canBeNull = true)
	public String sourceDocNumber;

	@SerializedName(SOURCE_DOC_TYPE_FIELD_NAME)
	@DatabaseField(columnName = SOURCE_DOC_TYPE_FIELD_NAME, canBeNull = true)
	public String sourceDocType;

	@DatabaseField(columnName = DONE_FIELD_NAME, canBeNull = true, defaultValue = "0")
	public Boolean done;

	@DatabaseField(columnName = TRANSFER_STATE__FIELD_NAME, canBeNull = true, defaultValue = "0")
	private int transferState;

	@SerializedName(DOOB_SYMBOL_FIELD_NAME)
	@DatabaseField(columnName = DOOB_SYMBOL_FIELD_NAME, canBeNull = true)
	private String doobSymbol;

	@SerializedName(SINV_SYMBOL_FIELD_NAME)
	@DatabaseField(columnName = SINV_SYMBOL_FIELD_NAME, canBeNull = true)
	private String sinvSymbol;

	@SerializedName(SORD_SYMBOL_STATE__FIELD_NAME)
	@DatabaseField(columnName = SORD_SYMBOL_STATE__FIELD_NAME, canBeNull = true)
	private String sordSymbol;

	private String palletSymbol;

	public ShippingOrderLine() {
		super();
	}

	public static void confirmByCarrier(Long shorId, String stpsGuidUnloading, String stpsGuidLoading, Long nosnId, String confirmColumnName, Boolean confirmValue) {
		try {
			final Dao<ShippingOrderLine, Long> shippingOrderLineDao = DaoManager.lookupDao(ShippingOrderLine.class);
			final Dao<Carrier, Long> carrierDao = DaoManager.lookupDao(Carrier.class);

			HashMap<String, Object> queryParam = new HashMap<String, Object>();
			queryParam.put("ShippingOrderLine", NAME);
			queryParam.put("ShorId", SHOR_ID_FIELD_NAME);
			queryParam.put("ShorIdValue", shorId);
			queryParam.put("SourceDocId", SOURCE_DOC_ID_FIELD_NAME);
			queryParam.put("SourceDocType", SOURCE_DOC_TYPE_FIELD_NAME);
			queryParam.put("ConfirmColumnName", confirmColumnName);
			queryParam.put("ConfirmColumnFilter", confirmValue ? 0 : 1);
			queryParam.put("ConfirmValue", confirmValue ? 1 : 0);
			queryParam.put("ConfirmDateValue", confirmValue ? DateTimeHelper.getCurrentDateAsString(DateTimeFormat.DateTimeFormat) : "NULL");
			queryParam.put("Done", DONE_FIELD_NAME);
			queryParam.put("DoneValue", confirmValue ? 1 : 0);
			queryParam.put("RecordState", RECORD_STATE__FIELD_NAME);
			queryParam.put("RecordStateValue", RecordStates.Updated.getValue());

			if (DELIVERED_FIELD_NAME.equalsIgnoreCase(confirmColumnName)) {
				queryParam.put("ConfirmDateColumnName", DELIVERY_DATE_FIELD_NAME);
			} else if (LOADED_FIELD_NAME.equalsIgnoreCase(confirmColumnName)) {
				queryParam.put("ConfirmDateColumnName", LOADING_DATE_FIELD_NAME);
			} else if (CANCELED_FIELD_NAME.equalsIgnoreCase(confirmColumnName)) {
				queryParam.put("ConfirmDateColumnName", CANCELLATION_DATE_FIELD_NAME);
			}
			StringBuilder queryStopFilter = new StringBuilder();
			StringBuilder query = new StringBuilder();
			query.append("UPDATE %(ShippingOrderLine) ");
			query.append("   SET %(ConfirmColumnName) = %(ConfirmValue), ");
			query.append("       %(ConfirmDateColumnName) = '%(ConfirmDateValue)', ");
			query.append("       %(Done) = %(DoneValue), ");
			query.append("       %(RecordState) = %(RecordStateValue)");
			query.append(" WHERE %(SourceDocType) = 'NOSN'");
			query.append("   AND (SourceDocId) = %(SourceDocIdValue)");
			query.append("   AND %(ShorId) = %(ShorIdValue)");
			query.append("   AND %(ConfirmColumnName) = %(ConfirmColumnFilter)");

			if (StringUtils.hasText(stpsGuidUnloading)) {
				queryParam.put("StpsIdUnloading", STPS_GUID_UNLOADING_FIELD_NAME);
				queryParam.put("StpsIdUnloadingFilter", stpsGuidUnloading);
				queryStopFilter.append("%(StpsIdUnloading) = '%(StpsIdUnloadingFilter)'");
			}

			if (StringUtils.hasText(stpsGuidLoading)) {
				queryParam.put("StpsIdLoading", STPS_GUID_LOADING_FIELD_NAME);
				queryParam.put("StpsIdLoadingFilter", stpsGuidLoading);

				if (queryStopFilter.length() > 0) {
					queryStopFilter.append(" OR ");
				}

				queryStopFilter.append("%(StpsIdLoading) = '%(StpsIdLoadingFilter)'");
			}

			if (queryStopFilter.length() > 0) {
				query.append(String.format(" AND (%s)", queryStopFilter));
			}

			for (Carrier carrier : carrierDao.queryBuilder().where().eq(Carrier.NOSN_ID_FIELD_NAME, nosnId).query()) {
				queryParam.put("SourceDocIdValue", carrier.getId());

				shippingOrderLineDao.updateRaw(DaoHelper.formatQuery(query.toString(), queryParam));

				confirmByCarrier(shorId, stpsGuidUnloading, stpsGuidLoading, carrier.getId(), confirmColumnName, confirmValue);
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage(ShippingOrderLine.class.getName(), e);
		}
	}

	public static void confirmLoadingById(List<Long> ids, Boolean loaded) {
		try {
			final Dao<ShippingOrderLine, Long> dao = DaoManager.lookupDao(ShippingOrderLine.class);
			final QueryBuilder<ShippingOrderLine, Long> qb = dao.queryBuilder();
			qb.where().in(DownloadTransferObjectBase.ID__FIELD_NAME, ids).and().ne(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue());

			for (final ShippingOrderLine line : qb.query()) {
				line.confirmLoading(loaded);
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage(ShippingOrderLine.class.getName(), e);
		}
	}

	@SuppressWarnings("unchecked")
	public static void confirmSubCarrierLines(ShippingOrderLine parentLine, String stpsGuidUnloading, String stpsGuidLoading, String confirmColumnName, Boolean confirmValue) {
		try {
			final Dao<ShippingOrderLine, Long> shippingOrderLineDao = DaoManager.lookupDao(ShippingOrderLine.class);

			QueryBuilder<ShippingOrderLine, Long> queryBuilder = shippingOrderLineDao.queryBuilder();
			Where<ShippingOrderLine, Long> where = queryBuilder.where();
			where.eq(ShippingOrderLine.SHOR_ID_FIELD_NAME, parentLine.getShorId());
			where.and().eq(ShippingOrderLine.SHOI_GUID__FIELD_NAME, parentLine.getGuid());
			where.and().eq(ShippingOrderLine.SOURCE_DOC_TYPE_FIELD_NAME, "NOSN");

			HashMap<String, Object> queryParam = new HashMap<String, Object>();
			queryParam.put("ShippingOrderLine", NAME);
			queryParam.put("ShorId", SHOR_ID_FIELD_NAME);
			queryParam.put("ShorIdValue", parentLine.getShorId());
			queryParam.put("Guid", GUID__FIELD_NAME);
			queryParam.put("SourceDocType", SOURCE_DOC_TYPE_FIELD_NAME);
			queryParam.put("ConfirmColumnName", confirmColumnName);
			queryParam.put("ConfirmColumnFilter", confirmValue ? 0 : 1);
			queryParam.put("ConfirmValue", confirmValue ? 1 : 0);
			queryParam.put("ConfirmDateValue", confirmValue ? DateTimeHelper.getCurrentDateAsString(DateTimeFormat.DateTimeFormat) : "NULL");
			queryParam.put("Done", DONE_FIELD_NAME);
			queryParam.put("DoneValue", confirmValue ? 1 : 0);
			queryParam.put("RecordState", RECORD_STATE__FIELD_NAME);
			queryParam.put("RecordStateValue", RecordStates.Updated.getValue());

			if (DELIVERED_FIELD_NAME.equalsIgnoreCase(confirmColumnName)) {
				queryParam.put("ConfirmDateColumnName", DELIVERY_DATE_FIELD_NAME);
			} else if (LOADED_FIELD_NAME.equalsIgnoreCase(confirmColumnName)) {
				queryParam.put("ConfirmDateColumnName", LOADING_DATE_FIELD_NAME);
			} else if (CANCELED_FIELD_NAME.equalsIgnoreCase(confirmColumnName)) {
				queryParam.put("ConfirmDateColumnName", CANCELLATION_DATE_FIELD_NAME);
			}

			StringBuilder queryStopFilter = new StringBuilder();
			StringBuilder query = new StringBuilder();
			query.append("UPDATE %(ShippingOrderLine) ");
			query.append("   SET %(ConfirmColumnName) = %(ConfirmValue), ");
			query.append("       %(ConfirmDateColumnName) = '%(ConfirmDateValue)', ");
			query.append("       %(Done) = %(DoneValue), ");
			query.append("       %(RecordState) = %(RecordStateValue)");
			query.append(" WHERE %(Guid) = '%(GuidFilter)'");
			query.append("   AND %(ConfirmColumnName) = %(ConfirmColumnFilter)");

			if (StringUtils.hasText(stpsGuidUnloading)) {
				queryParam.put("StpsIdUnloading", STPS_GUID_UNLOADING_FIELD_NAME);
				queryParam.put("StpsIdUnloadingFilter", stpsGuidUnloading);
				queryStopFilter.append("%(StpsIdUnloading) = '%(StpsIdUnloadingFilter)'");
			}

			if (StringUtils.hasText(stpsGuidLoading)) {
				queryParam.put("StpsIdLoading", STPS_GUID_LOADING_FIELD_NAME);
				queryParam.put("StpsIdLoadingFilter", stpsGuidLoading);

				if (queryStopFilter.length() > 0) {
					queryStopFilter.append(" OR ");
				}

				queryStopFilter.append("%(StpsIdLoading) = '%(StpsIdLoadingFilter)'");
			}

			if (queryStopFilter.length() > 0) {
				query.append(String.format(" AND (%s)", queryStopFilter));
			}

			if (StringUtils.hasText(stpsGuidUnloading) && StringUtils.hasText(stpsGuidLoading)) {
				where.and().or(where.eq(ShippingOrderLine.STPS_GUID_LOADING_FIELD_NAME, stpsGuidLoading), where.eq(ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME, stpsGuidUnloading));
			} else if (StringUtils.hasText(stpsGuidUnloading)) {
				where.and().eq(ShippingOrderLine.STPS_GUID_UNLOADING_FIELD_NAME, stpsGuidUnloading);
			} else if (StringUtils.hasText(stpsGuidLoading)) {
				where.and().eq(ShippingOrderLine.STPS_GUID_LOADING_FIELD_NAME, stpsGuidLoading);
			}

			for (ShippingOrderLine line : queryBuilder.query()) {
				queryParam.put("GuidFilter", line.getGuid());

				shippingOrderLineDao.updateRaw(DaoHelper.formatQuery(query.toString(), queryParam));

				confirmSubCarrierLines(line, stpsGuidUnloading, stpsGuidLoading, confirmColumnName, confirmValue);
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage(ShippingOrderLine.class.getName(), e);
		}
	}

	public static void confirmUnloadingById(List<Long> ids, Boolean delivered) {
		try {
			final Dao<ShippingOrderLine, Long> dao = DaoManager.lookupDao(ShippingOrderLine.class);
			final QueryBuilder<ShippingOrderLine, Long> qb = dao.queryBuilder();
			qb.where().in(DownloadTransferObjectBase.ID__FIELD_NAME, ids).and().ne(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue());

			for (final ShippingOrderLine line : qb.query()) {
				line.confirmUnloading(delivered);
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage(ShippingOrderLine.class.getName(), e);
		}
	}

	public static List<ShippingOrderLine> getDataToSend() {

		final Dao<ShippingOrderLine, Long> dao = DaoManager.lookupDao(ShippingOrderLine.class);

		if (dao == null) {
			return null;
		}

		QueryBuilder<ShippingOrderLine, Long> queryBuilder = dao.queryBuilder();

		try {
			queryBuilder.where().eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Updated.getValue()).or()
					.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Inserted.getValue());

			List<ShippingOrderLine> dataToSend = queryBuilder.query();

			if (dataToSend != null) {
				return dataToSend;
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage("ShippingOrderLine.getDataForSync()", e);
		}

		return null;
	}

	public static Long getDataToSendCount() {

		final Dao<ShippingOrderLine, Long> dao = DaoManager.lookupDao(ShippingOrderLine.class);
		QueryBuilder<ShippingOrderLine, Long> queryBuilder = dao.queryBuilder();

		try {
			queryBuilder.where().eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Updated.getValue()).or()
					.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Inserted.getValue()).or()
					.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.getValue());

			return queryBuilder.countOf();
		} catch (SQLException e) {
			LogHelper.setErrorMessage("CashDocument.getDataForSync()", e);
		}

		return 0L;
	}

	public static void reportIncidentById(List<Long> ids, Long reasonId, String reasonDescription) {
		try {
			final Dao<ShippingOrderLine, Long> dao = DaoManager.lookupDao(ShippingOrderLine.class);
			final QueryBuilder<ShippingOrderLine, Long> qb = dao.queryBuilder();
			qb.where().in(DownloadTransferObjectBase.ID__FIELD_NAME, ids).and().ne(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue());

			for (final ShippingOrderLine line : qb.query()) {
				line.setCanceled(true);
				line.setComments(reasonDescription);
				line.setProdId(reasonId);
				line.update();
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage(ShippingOrderLine.class.getName(), e);
		}
	}

	public static String confirmTransfering(String lineSrcGuid, Long shippingOrderDestinationId) throws SQLException {
		final Dao<ShippingOrderLine, Long> shippingOrderLineDao = DaoManager.lookupDao(ShippingOrderLine.class);
		
		ShippingOrderLine lineSrc = DaoManager.queryForGuid(lineSrcGuid, shippingOrderLineDao);
		
		if(lineSrc != null){
			return confirmTransfering(lineSrc, shippingOrderDestinationId);
		}
		
		return null;
	}
	
	public static String confirmTransfering(ShippingOrderLine lineSrc, Long shippingOrderDestinationId) throws SQLException {
		final Dao<ShippingOrderLine, Long> shippingOrderLineDao = DaoManager.lookupDao(ShippingOrderLine.class);
		final Dao<Stop, Long> stopDao = DaoManager.lookupDao(Stop.class);

		// Check if source line is not already take over
		ShippingOrderLine lineDst = shippingOrderLineDao.queryBuilder().where().eq(Stop.SHOR_ID_FIELD_NAME, shippingOrderDestinationId).and()
				.eq(ShippingOrderLine.SHOI_GUID_PREV__FIELD_NAME, lineSrc.getGuid()).queryForFirst();

		if (lineDst != null) {
			return lineDst.getGuid();
		}

		Stop stopSrcUnloading = DaoManager.queryForGuid(lineSrc.getStopUnloading().getGuid(), stopDao);

		// Check if exists on destination shipping order stop with
		// source stop location
		Stop stopDstUnloading = stopDao.queryBuilder().where().eq(Stop.SHOR_ID_FIELD_NAME, shippingOrderDestinationId).and().eq(Stop.LOCA_ID__FIELD_NAME, stopSrcUnloading.getLocaId()).queryForFirst();

		// If not exist then create new
		if (stopDstUnloading == null) {
			stopSrcUnloading.setTransferState(TransferState.Transfered);
			stopSrcUnloading.update(RecordStates.NoChange);

			stopDstUnloading = stopSrcUnloading;
			stopDstUnloading.setId(null);
			stopDstUnloading.setGuid(UUID.randomUUID().toString());
			stopDstUnloading.setRecordState(RecordStates.Inserted);
			stopDstUnloading.setShorId(shippingOrderDestinationId);
			stopDstUnloading.setTransferState(TransferState.NotTransfered);

			stopDao.create(stopDstUnloading);
		}

		Stop stopSrcLoading = DaoManager.queryForGuid(lineSrc.getStopLoading().getGuid(), stopDao);

		// Check if exists on destination shipping order stop with
		// source stop location
		Stop stopDstLoading = stopDao.queryBuilder().where().eq(Stop.SHOR_ID_FIELD_NAME, shippingOrderDestinationId).and().eq(Stop.LOCA_ID__FIELD_NAME, stopSrcLoading.getLocaId()).queryForFirst();

		// If not exist then create new
		if (stopDstLoading == null) {
			stopDstLoading = stopSrcLoading;
			stopDstLoading.setId(null);
			stopDstLoading.setGuid(UUID.randomUUID().toString());
			stopDstLoading.setRecordState(RecordStates.Inserted);
			stopDstLoading.setShorId(shippingOrderDestinationId);
			stopDstLoading.setTransferState(TransferState.NotTransfered);

			stopDao.create(stopDstLoading);
		}

		// Set Transfered state of source line
		lineSrc.setTransferState(TransferState.Transfered);
		lineSrc.update(RecordStates.NoChange);

		// Copy source line to destination shipping order
		lineDst = lineSrc;
		
		// Check if line have parent line
		if (StringUtils.hasText(lineSrc.getShoiGuid())) {
			String parentLineGuid = confirmTransfering(lineSrc.getShoiGuid(), shippingOrderDestinationId);

			if (StringUtils.hasText(parentLineGuid)) {
				lineDst.setShoiGuid(parentLineGuid);
			}
		}
		
		lineDst.setId(null);
		lineDst.setRecordState(RecordStates.Inserted);
		lineDst.setShoiGuidPrev(lineSrc.getGuid());
		lineDst.setGuid(UUID.randomUUID().toString());
		lineDst.setShorId(shippingOrderDestinationId);
		lineDst.setTransferState(TransferState.NotTransfered);
		lineDst.setLoaded(true);
		lineDst.setStopLoading(stopDstLoading);
		lineDst.setStopUnloading(stopDstUnloading);

		shippingOrderLineDao.create(lineDst);

		return lineDst.getGuid();
	}

	public static void confirmTransfering(Long shippingOrderSourceId, Long shippingOrderDestinationId) throws SQLException {
		final Dao<ShippingOrderLine, Long> shippingOrderLineDao = DaoManager.lookupDao(ShippingOrderLine.class);

		List<ShippingOrderLine> linesToTransfer = shippingOrderLineDao.queryBuilder().where().eq(Stop.SHOR_ID_FIELD_NAME, shippingOrderSourceId).and()
				.eq(ShippingOrderLine.TRANSFER_STATE__FIELD_NAME, TransferState.Transfering.getValue()).query();

		for (ShippingOrderLine lineSrc : linesToTransfer) {
			confirmTransfering(lineSrc, shippingOrderDestinationId);
		}
	}

	public void confirmLoading(Boolean loaded) throws SQLException {
		setLoaded(loaded);
		setDone(loaded);
		update();

		// Confirm sub-carriers lines
		if (getSourceDocType().equalsIgnoreCase("NOSN")) {
			ShippingOrderLine.confirmSubCarrierLines(this, null, getStopLoading().getGuid(), LOADED_FIELD_NAME, loaded);
		}
	}

	public void confirmUnloading(Boolean delivered) throws SQLException {
		setDelivered(delivered);
		setDone(delivered);
		update();

		// If unloading location is HUB, then confirm loading this load on line
		// shipping order
		if (getStopUnloading().getLocaLocationType().equalsIgnoreCase("HUB")) {
			// TODO: potwierdzanie rozładowania pozycji na zleceniu liniowym
			// final Dao<ShippingOrder, Long> shippingOrderDao =
			// DaoManager.lookupDao(ShippingOrder.class);
			// ShippingOrder shippingOrder =
			// shippingOrderDao.queryForId(getStopUnloading().getShorId());
			//
			// if(!shippingOrder.getRoutType().equalsIgnoreCase(RoutType.line.getValue()))
			// {
			//
			// }
		}

		// Confirm sub-carriers lines
		if (getSourceDocType().equalsIgnoreCase("NOSN")) {
			ShippingOrderLine.confirmSubCarrierLines(this, getStopUnloading().getGuid(), null, DELIVERED_FIELD_NAME, delivered);
		}
	}

	public String getCancellationDate() {
		return cancellationDate;
	}

	public String getComments() {
		return comments;
	}

	public Boolean getDelivered() {
		return delivered;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public String getDescription() {
		return description;
	}

	public String getLoadingDate() {
		return loadingDate;
	}

	public String getPalletSymbol() {
		return palletSymbol;
	}

	public String getPrioCode() {
		return prioCode;
	}

	public Long getProdId() {
		return prodId;
	}

	public String getRozlType() {
		return rozlType;
	}

	public String getShoiGuid() {
		return shoiGuid;
	}

	public String getShoiGuidPrev() {
		return shoiGuidPrev;
	}

	public Long getShorId() {
		return shorId;
	}

	public String getSourceDocAdditionalData() {
		return sourceDocAdditionalData;
	}

	public String getSourceDocGuid() {
		return sourceDocGuid;
	}

	public Long getSourceDocId() {
		return sourceDocId;
	}

	public String getSourceDocNumber() {
		return sourceDocNumber;
	}

	public String getSourceDocSubtype() {
		return sourceDocSubtype;
	}

	public String getSourceDocType() {
		return sourceDocType;
	}

	public Stop getStopLoading() {
		return stopLoading;
	}

	public Stop getStopUnloading() {
		return stopUnloading;
	}

	public Boolean isCanceled() {
		return canceled;
	}

	public Boolean isDelivered() {
		return delivered;
	}

	public Boolean isDone() {
		return done;
	}

	public Boolean isLoaded() {
		return loaded;
	}

	public void setCanceled(Boolean canceled) {
		this.canceled = canceled;

		if (this.canceled) {
			this.cancellationDate = DateTimeHelper.getCurrentDateAsString(DateTimeFormat.DateTimeFormat);
		} else {
			this.cancellationDate = null;
		}
	}

	public void setCancellationDate(String cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public void setDelivered(Boolean delivered) {
		this.delivered = delivered;

		if (this.delivered) {
			this.deliveryDate = DateTimeHelper.getCurrentDateAsString(DateTimeFormat.SaveDateTimeFormat);
		} else {
			this.deliveryDate = null;
		}
	}

	public void setDeliveryDate(String deliveryDate) {
		if (StringUtils.hasText(deliveryDate)) {
			Date parseDate = DateTimeHelper.getDate(deliveryDate);

			this.deliveryDate = DateTimeHelper.getDateAsString(parseDate, DateTimeFormat.SaveDateTimeFormat);
		} else {
			this.deliveryDate = deliveryDate;
		}
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDone(Boolean done) {
		this.done = done;
	}

	public void setLoaded(Boolean loaded) {
		this.loaded = loaded;

		if (this.loaded) {
			this.loadingDate = DateTimeHelper.getCurrentDateAsString(DateTimeFormat.SaveDateTimeFormat);
		} else {
			this.loadingDate = null;
		}
	}

	public void setLoadingDate(String loadingDate) {
		this.loadingDate = loadingDate;
	}

	public void setPalletSymbol(String palletSymbol) {
		this.palletSymbol = palletSymbol;
	}

	public void setPrioCode(String prioCode) {
		this.prioCode = prioCode;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public void setRozlType(String rozlType) {
		this.rozlType = rozlType;
	}

	public void setShoiGuid(String shoiGuid) {
		this.shoiGuid = shoiGuid;
	}

	public void setShoiGuidPrev(String shoiGuidPrev) {
		this.shoiGuidPrev = shoiGuidPrev;
	}

	public void setShorId(Long shorId) {
		this.shorId = shorId;
	}

	public void setSourceDocAdditionalData(String sourceDocAdditionalData) {
		this.sourceDocAdditionalData = sourceDocAdditionalData;
	}

	public void setSourceDocGuid(String sourceDocGuid) {
		this.sourceDocGuid = sourceDocGuid;
	}

	public void setSourceDocId(Long sourceDocId) {
		this.sourceDocId = sourceDocId;
	}

	public void setSourceDocNumber(String sourceDocNumber) {
		this.sourceDocNumber = sourceDocNumber;
	}

	public void setSourceDocSubtype(String sourceDocSubtype) {
		this.sourceDocSubtype = sourceDocSubtype;
	}

	public void setSourceDocType(String sourceDocType) {
		this.sourceDocType = sourceDocType;
	}

	public void setStopLoading(Stop stopLoading) {
		this.stopLoading = stopLoading;
	}

	public void setStopUnloading(Stop stopUnloading) {
		this.stopUnloading = stopUnloading;
	}

	public String getDoobSymbol() {
		return doobSymbol;
	}

	public void setDoobSymbol(String doobSymbol) {
		this.doobSymbol = doobSymbol;
	}

	public String getSinvSymbol() {
		return sinvSymbol;
	}

	public void setSinvSymbol(String sinvSymbol) {
		this.sinvSymbol = sinvSymbol;
	}

	public String getSordSymbol() {
		return sordSymbol;
	}

	public void setSordSymbol(String sordSymbol) {
		this.sordSymbol = sordSymbol;
	}

	public TransferState getTransferState() {
		return TransferState.values()[transferState];
	}

	public void setTransferState(TransferState transferState) {
		this.transferState = transferState.intValue();
	}

	public void setTransferStateValue(int transferStateValue) {
		this.transferState = transferStateValue;
	}

	public void setTransferingState() throws SQLException {
		this.transferState = TransferState.Transfering.intValue();
		update(RecordStates.NoChange);

		if (stopUnloading != null) {
			stopUnloading.setTransferingState();
		}
	}

	public static void setTransferingNonCarrierLines(String stpsSourceGuid) throws SQLException {
		final Dao<ShippingOrderLine, Long> shippingOrderLineDao = DaoManager.lookupDao(ShippingOrderLine.class);

		HashMap<String, Object> queryParam = new HashMap<String, Object>();
		queryParam.put("ShippingOrderLine", NAME);
		queryParam.put("TransferState", TRANSFER_STATE__FIELD_NAME);
		queryParam.put("TransferStateValue", TransferState.Transfering.getValue());
		queryParam.put("StpsGuidUnloading", STPS_GUID_UNLOADING_FIELD_NAME);
		queryParam.put("StpsGuidLoading", STPS_GUID_LOADING_FIELD_NAME);
		queryParam.put("StpsGuidValue", stpsSourceGuid);
		queryParam.put("SourceDocType", SOURCE_DOC_TYPE_FIELD_NAME);
		queryParam.put("SourceDocTypeNosn", SourceDocType.Carrier.getValue());

		StringBuilder query = new StringBuilder();
		query.append("UPDATE %(ShippingOrderLine) ");
		query.append("   SET %(TransferState) = %(TransferStateValue) ");
		query.append(" WHERE (%(StpsGuidUnloading) = '%(StpsGuidValue)' AND %(SourceDocType) <> '%(SourceDocTypeNosn)')");
		query.append("    OR %(StpsGuidLoading) = '%(StpsGuidValue)'");

		shippingOrderLineDao.updateRaw(DaoHelper.formatQuery(query.toString(), queryParam));
	}

	public static void undoTransferingByShorId(Long shorId) throws SQLException {
		final Dao<ShippingOrderLine, Long> shippingOrderLineDao = DaoManager.lookupDao(ShippingOrderLine.class);

		HashMap<String, Object> queryParam = new HashMap<String, Object>();
		queryParam.put("ShippingOrderLine", NAME);
		queryParam.put("TransferState", TRANSFER_STATE__FIELD_NAME);
		queryParam.put("TransferStateValue", TransferState.NotTransfered.getValue());
		queryParam.put("TransferStateFilter", TransferState.Transfering.getValue());
		queryParam.put("ShorId", SHOR_ID_FIELD_NAME);
		queryParam.put("ShorIdValue", shorId);

		StringBuilder query = new StringBuilder();
		query.append("UPDATE %(ShippingOrderLine) ");
		query.append("   SET %(TransferState) = %(TransferStateValue) ");
		query.append(" WHERE %(ShorId) = %(ShorIdValue)");
		query.append("   AND %(TransferState) = '%(TransferStateFilter)'");

		shippingOrderLineDao.updateRaw(DaoHelper.formatQuery(query.toString(), queryParam));

		Stop.undoTransferingByShorId(shorId);
	}
	
	public static void undoTransferingByStop(String stpsSourceGuid) throws SQLException {
		final Dao<ShippingOrderLine, Long> shippingOrderLineDao = DaoManager.lookupDao(ShippingOrderLine.class);

		HashMap<String, Object> queryParam = new HashMap<String, Object>();
		queryParam.put("ShippingOrderLine", NAME);
		queryParam.put("TransferState", TRANSFER_STATE__FIELD_NAME);
		queryParam.put("TransferStateValue", TransferState.NotTransfered.getValue());
		queryParam.put("StpsGuidUnloading", STPS_GUID_UNLOADING_FIELD_NAME);
		queryParam.put("StpsGuidLoading", STPS_GUID_LOADING_FIELD_NAME);
		queryParam.put("StpsGuidValue", stpsSourceGuid);

		StringBuilder query = new StringBuilder();
		query.append("UPDATE %(ShippingOrderLine) ");
		query.append("   SET %(TransferState) = %(TransferStateValue) ");
		query.append(" WHERE %(StpsGuidUnloading) = '%(StpsGuidValue)'");
		query.append("    OR %(StpsGuidLoading) = '%(StpsGuidValue)'");

		shippingOrderLineDao.updateRaw(DaoHelper.formatQuery(query.toString(), queryParam));

		Stop.undoTransfering(stpsSourceGuid);
	}

	@Override
	public int update() throws SQLException {
		setRecordState(RecordStates.Updated);

		return super.update();
	}

	public enum SourceDocType {
		Carrier("NOSN"), Plug("SEAL"), Payment("PAYM");

		private String value;

		private SourceDocType(String message) {
			value = message;
		}

		public String getValue() {
			return value;
		}
	}

	public enum TransferState {
		NotTransfered(0), Transfering(1), Transfered(2);

		private final int value;

		TransferState(int value) {

			this.value = value;
		}

		public Integer intValue() {

			return value;
		}

		public int getValue() {
			return value;
		}
	}
}
