package com.u4.mobile.transport.model;

import java.util.UUID;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.u4.mobile.model.DownloadTransferObjectBase;

@DatabaseTable(tableName = "Contractor")
public class Contractor extends DownloadTransferObjectBase {

	public transient static final String PAYMENT_DELAY_SALES__FIELD_NAME = "PaymentDelaySales";
	public transient static final String PAYMENT_FORM_GUID__FIELD_NAME = "FozaGuid";
	public transient static final String CONSUMER_GROUP_GUID__FIELD_NAME = "GrodGuid";
	public transient static final String POSTAL_CODE__FIELD_NAME = "PostalCode";
	public transient static final String CITY__FIELD_NAME = "City";
	public transient static final String NAME__FIELD_NAME = "Name";
	public transient static final String NIP__FIELD_NAME = "Nip";
	public transient static final String BUILDING__FIELD_NAME = "Building";
	public transient static final String ANALITYKA__FIELD_NAME = "Analytics";
	public transient static final String APARTMENT__FIELD_NAME = "Apartment";
	public transient static final String IS_RECEIVER__FIELD_NAME = "IsReceiver";
	public transient static final String PLATNIK__FIELD_NAME = "IsPayer";
	public transient static final String RODZAJ_DATY_WAR_HANDL_ZAM__FIELD_NAME = "DateTypeTradCondOrder";
	public transient static final String SHORT_NAME__FIELD_NAME = "ShortName";
	public transient static final String DELIVERY_METHOD_GUID__FIELD_NAME = "SpdoGuid";
	public transient static final String SYMBOL__FIELD_NAME = "Symbol";
	public transient static final String STREET__FIELD_NAME = "Street";
	public transient static final String PAYER_GUID__FIELD_NAME = "KonrGuidPayer";
	public transient static final String CREDIT_LIMIT__FIELD_NAME = "CreditLimit";
	public transient static final String SALES_PRICE_TYPE_GUID__FIELD_NAME = "RcezGuid";
	public transient static final String POINTS__FIELD_NAME = "Points";
	public transient static final String IS_MAIN__FIELD_NAME = "IsMain";
	public transient static final String SYNC_VERSION_FIELD_NAME = "SyncVersion";
	public transient static final String NAME = "Contractor";
	public transient static final String TABLE_PREFIX = "KONR";
	public transient static final String UP_TO_DATE__FIELD_NAME = "UpToDate";

	private static final long serialVersionUID = 1L;
	public transient static final long PackageSize = 256L;

	@SerializedName("PaymentDelaySales")
	@DatabaseField(columnName = PAYMENT_DELAY_SALES__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private short daysToPay;

	@SerializedName("FozaGuid")
	@DatabaseField(columnName = PAYMENT_FORM_GUID__FIELD_NAME, canBeNull = true)
	private UUID fozaGuid;

	@SerializedName("GrodGuid")
	@DatabaseField(columnName = CONSUMER_GROUP_GUID__FIELD_NAME, canBeNull = true)
	private UUID grodGuid;

	@SerializedName("PostalCode")
	@DatabaseField(columnName = POSTAL_CODE__FIELD_NAME, canBeNull = true)
	private String postalCode;

	@SerializedName("City")
	@DatabaseField(columnName = CITY__FIELD_NAME, canBeNull = true)
	private String city;

	@SerializedName("Name")
	@DatabaseField(columnName = NAME__FIELD_NAME, canBeNull = false)
	private String name;

	@SerializedName("NIP")
	@DatabaseField(columnName = NIP__FIELD_NAME, canBeNull = true)
	private String nip;

	@SerializedName("Building")
	@DatabaseField(columnName = BUILDING__FIELD_NAME, canBeNull = true)
	private String building;

	@SerializedName("Analityka")
	@DatabaseField(columnName = ANALITYKA__FIELD_NAME, canBeNull = true)
	private String analytics;

	@SerializedName("Apartment")
	@DatabaseField(columnName = APARTMENT__FIELD_NAME, canBeNull = true)
	private String apartment;

	@SerializedName("Receiver")
	@DatabaseField(columnName = IS_RECEIVER__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private boolean isReceiver;

	@SerializedName("Payer")
	@DatabaseField(columnName = PLATNIK__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private boolean isPayer;

	@SerializedName("DateTypeTradCondOrder")
	@DatabaseField(columnName = RODZAJ_DATY_WAR_HANDL_ZAM__FIELD_NAME, canBeNull = true)
	private String dateTypeTradCondOrder;

	@SerializedName("ShortName")
	@DatabaseField(columnName = SHORT_NAME__FIELD_NAME, canBeNull = true)
	private String shortName;

	@SerializedName("SpdoGuid")
	@DatabaseField(columnName = DELIVERY_METHOD_GUID__FIELD_NAME, canBeNull = true)
	private UUID spdoGuid;

	@SerializedName("Symbol")
	@DatabaseField(columnName = SYMBOL__FIELD_NAME, canBeNull = false, index = true)
	private String symbol;

	@SerializedName("Street")
	@DatabaseField(columnName = STREET__FIELD_NAME, canBeNull = true)
	private String street;

	@SerializedName("KonrGuidPayer")
	@DatabaseField(columnName = PAYER_GUID__FIELD_NAME, canBeNull = true, foreign = true, foreignColumnName = GUID__FIELD_NAME, index = true)
	private Contractor payer;

	@DatabaseField(columnName = CREDIT_LIMIT__FIELD_NAME, canBeNull = true, defaultValue = "0")
	private float creditLimit;

	@SerializedName("RcezGuid")
	@DatabaseField(columnName = SALES_PRICE_TYPE_GUID__FIELD_NAME, canBeNull = true, index = true)
	private UUID rcezGuid;

	@DatabaseField(columnName = POINTS__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private float points;

	@SerializedName("IsMain")
	@DatabaseField(columnName = IS_MAIN__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private boolean isMain;

	@SerializedName("UpToDate")
	@DatabaseField(columnName = UP_TO_DATE__FIELD_NAME, canBeNull = false, defaultValue = "0", index = true)
	private boolean upToDate;

	@SerializedName(SYNC_VERSION_FIELD_NAME)
	@DatabaseField(columnName = SYNC_VERSION_FIELD_NAME, canBeNull = false, defaultValue = "0")
	private String syncVersion;

	public Contractor() {
		super();
	}

	public String getAnalytics() {
		return analytics;
	}

	public String getApartment() {
		return apartment;
	}

	public String getBuilding() {
		return building;
	}

	public String getCity() {
		return city;
	}

	public float getCreditLimit() {
		return creditLimit;
	}

	public String getDateTypeTradCondOrder() {
		return dateTypeTradCondOrder;
	}

	public short getDaysToPay() {
		return daysToPay;
	}

	public UUID getFozaGuid() {
		return fozaGuid;
	}

	public UUID getGrodGuid() {
		return grodGuid;
	}

	public String getName() {
		return name;
	}

	public String getNip() {
		return nip;
	}

	public Contractor getPayer() {
		return payer;
	}

	public float getPoints() {
		return points;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public UUID getRcezGuid() {
		return rcezGuid;
	}

	public String getShortName() {
		return shortName;
	}

	public UUID getSpdoGuid() {
		return spdoGuid;
	}

	public String getStreet() {
		return street;
	}

	public String getSymbol() {
		return symbol;
	}

	public String getSyncVersion() {
		return syncVersion;
	}

	public boolean isMain() {
		return isMain;
	}

	public boolean isPayer() {
		return isPayer;
	}

	public boolean isReceiver() {
		return isReceiver;
	}

	public boolean isUpToDate() {
		return upToDate;
	}

	public void setAnalytics(String analytics) {
		this.analytics = analytics;
	}

	public void setApartment(String apartment) {
		this.apartment = apartment;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCreditLimit(float creditLimit) {
		this.creditLimit = creditLimit;
	}

	public void setDateTypeTradCondOrder(String dateTypeTradCondOrder) {
		this.dateTypeTradCondOrder = dateTypeTradCondOrder;
	}

	public void setDaysToPay(short daysToPay) {
		this.daysToPay = daysToPay;
	}

	public void setFozaGuid(UUID fozaGuid) {
		this.fozaGuid = fozaGuid;
	}

	public void setGrodGuid(UUID grodGuid) {
		this.grodGuid = grodGuid;
	}

	public void setMain(boolean isMain) {
		this.isMain = isMain;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public void setPayer(boolean isPayer) {
		this.isPayer = isPayer;
	}

	public void setPayer(Contractor payer) {
		this.payer = payer;
	}

	public void setPoints(float points) {
		this.points = points;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public void setRcezGuid(UUID rcezGuid) {
		this.rcezGuid = rcezGuid;
	}

	public void setReceiver(boolean isReceiver) {
		this.isReceiver = isReceiver;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public void setSpdoGuid(UUID spdoGuid) {
		this.spdoGuid = spdoGuid;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public void setSyncVersion(String syncVersion) {
		this.syncVersion = syncVersion;
	}

	public void setUpToDate(boolean upToDate) {
		this.upToDate = upToDate;
	}
}
