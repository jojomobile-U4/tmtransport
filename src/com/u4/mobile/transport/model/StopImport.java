package com.u4.mobile.transport.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.u4.mobile.model.UploadTransferObjectBase;

public class StopImport extends UploadTransferObjectBase implements Serializable {

	public transient static final String DESCRIPTION_FIELD_NAME = "Description";
	public transient static final String LOCA_ID__FIELD_NAME = "LocaId";
	public transient static final String OBJECT_GUID__FIELD_NAME = "ObjectGuid";
	public transient static final String SHOR_ID_FIELD_NAME = "ShorId";
	public transient static final String STOP_SEQUENCE_FIELD_NAME = "StopSequence";

	private static final long serialVersionUID = 1L;

	@SerializedName(DESCRIPTION_FIELD_NAME)
	private String description;

	@SerializedName(LOCA_ID__FIELD_NAME)
	private Long locaId;

	@SerializedName(OBJECT_GUID__FIELD_NAME)
	private String objectGuid;

	@SerializedName(SHOR_ID_FIELD_NAME)
	private Long shorId;

	@SerializedName(STOP_SEQUENCE_FIELD_NAME)
	public Long stopSequence;

	public StopImport(Stop transferObject) {
		super(transferObject);

		setObjectGuid(transferObject.getGuid());
		setDescription(transferObject.getDescription());
		setLocaId(transferObject.getLocaId());
		setShorId(transferObject.getShorId());
		setStopSequence(transferObject.getStopSequence());
	}

	public String getDescription() {
		return description;
	}

	public Long getLocaId() {
		return locaId;
	}

	public String getObjectGuid() {
		return objectGuid;
	}

	public Long getShorId() {
		return shorId;
	}

	public Long getStopSequence() {
		return stopSequence;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setLocaId(Long locaId) {
		this.locaId = locaId;
	}

	public void setObjectGuid(String objectGuid) {
		this.objectGuid = objectGuid;
	}

	public void setShorId(Long shorId) {
		this.shorId = shorId;
	}

	public void setStopSequence(Long stopSequence) {
		this.stopSequence = stopSequence;
	}
}
