package com.u4.mobile.transport.model;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.DatabaseTable;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;
import com.u4.mobile.model.DaoManager;
import com.u4.mobile.model.DatabaseHolder;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.model.UploadTransferObjectBase;
import com.u4.mobile.transport.ui.cashbox.CashBoxHelper;
import com.u4.mobile.utils.CalculationHelper;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.LogHelper;

@DatabaseTable(tableName = "CashDocument")
public class CashDocument extends UploadTransferObjectBase {

	public static final String KSRK_GUID_FIELD_NAME = "KsrkGuid";
	public static final String DOCUMENT_TYPE_FIELD_NAME = "DocumentType";
	public static final String DOCUMENT_NUMBER_FIELD_NAME = "DocumentNumber";
	public static final String DOCUMENT_SYMBOL_FIELD_NAME = "DocumentSymbol";
	public static final String DOCUMENT_DATE_FIELD_NAME = "DocumentDate";
	public static final String KONR_GUID_FIELD_NAME = "KonrGuid";
	public static final String DESCRIPTION_FIELD_NAME = "Description";
	public static final String APPROVED_FIELD_NAME = "Approved";
	public static final String NAME = "CashDocument";
	public static final String TABLE_PREFIX = "KSDK";

	private static final long serialVersionUID = 1L;
	public transient static final long PackageSize = 256L;

	@SerializedName(KSRK_GUID_FIELD_NAME)
	@DatabaseField(columnName = KSRK_GUID_FIELD_NAME, canBeNull = true)
	private String ksrkGuid;

	@SerializedName(DESCRIPTION_FIELD_NAME)
	@DatabaseField(columnName = DESCRIPTION_FIELD_NAME, canBeNull = true)
	private String description;

	@SerializedName(DOCUMENT_TYPE_FIELD_NAME)
	@DatabaseField(columnName = DOCUMENT_TYPE_FIELD_NAME, dataType = DataType.ENUM_STRING, canBeNull = true)
	private CashDocumentType documentType;

	@SerializedName(DOCUMENT_NUMBER_FIELD_NAME)
	@DatabaseField(columnName = DOCUMENT_NUMBER_FIELD_NAME, canBeNull = false, defaultValue = "0")
	private long documentNumber;

	@SerializedName(DOCUMENT_SYMBOL_FIELD_NAME)
	@DatabaseField(columnName = DOCUMENT_SYMBOL_FIELD_NAME, canBeNull = true)
	private String documentSymbol;

	@SerializedName(DOCUMENT_DATE_FIELD_NAME)
	@DatabaseField(columnName = DOCUMENT_DATE_FIELD_NAME, canBeNull = false)
	private String documentDate;

	@SerializedName(KONR_GUID_FIELD_NAME)
	@DatabaseField(columnName = KONR_GUID_FIELD_NAME, canBeNull = true, foreign = true, foreignColumnName = GUID__FIELD_NAME, foreignAutoRefresh = true, index = true)
	private Contractor contractor;

	@SerializedName(APPROVED_FIELD_NAME)
	@DatabaseField(columnName = APPROVED_FIELD_NAME, canBeNull = true)
	private boolean approved;

	public CashDocument() {
		super();
		setGuid(UUID.randomUUID().toString());
	}

	public static Long getDataToSendCount() {

		final Dao<CashDocument, Long> dao = DaoManager.lookupDao(CashDocument.class);
		QueryBuilder<CashDocument, Long> queryBuilder = dao.queryBuilder();

		try {
			queryBuilder.where().eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Updated.getValue()).or()
					.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Inserted.getValue()).or()
					.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.getValue());

			return queryBuilder.countOf();
		} catch (SQLException e) {
			LogHelper.setErrorMessage("CashDocument.getDataForSync()", e);
		}

		return 0L;
	}
	
	public static List<CashDocument> getDataToSend() {

		final Dao<CashDocument, Long> dao = DaoManager.lookupDao(CashDocument.class);

		if (dao == null) {
			return null;
		}

		QueryBuilder<CashDocument, Long> queryBuilder = dao.queryBuilder();

		try {
			queryBuilder.where().eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Updated.getValue()).or()
					.eq(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Inserted.getValue());

			List<CashDocument> dataToSend = queryBuilder.query();

			if (dataToSend != null) {
				return dataToSend;
			}
		} catch (SQLException e) {
			LogHelper.setErrorMessage("CashDocument.getDataForSync()", e);
		}

		return null;
	}

	public void approve() {
		try {
			if (getDocumentType() == CashDocumentType.KP || getDocumentType() == CashDocumentType.KW) {
				// Update related customer accounts
				final Dao<CashDocumentPosition, Long> dao = DatabaseHolder.getInstance().getDao(CashDocumentPosition.class);
				final QueryBuilder<CashDocumentPosition, Long> qb = dao.queryBuilder();
				qb.where().eq(CashDocumentPosition.KSDK_GUID_FIELD_NAME, getGuid()).and().notIn(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue());

				for (final CashDocumentPosition position : qb.query()) {
					if (position.getContractorAccounts() != null) {
						final ContractorAccounts account = position.getContractorAccounts();
						if (account != null) {
							account.setLeftToPay((float) CalculationHelper.round((account.getLeftToPay() - (getDocumentType() == CashDocumentType.KP ? position.getIncome() : -position
									.getExpenditure()))));
							account.update();
						}
					}
				}
			}

			// Approve cash document
			setApproved(true);
			setDocumentNumber(CashBoxHelper.getCashDocumentNextNumber(getDocumentType()));
			final Calendar calendar = new GregorianCalendar();
			calendar.setTime(DateTimeHelper.getDate(getDocumentDate()));
			setDocumentSymbol(generateSymbol());
			update();
		} catch (final SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		}
	}

	public double getAmount() {
		double amount = 0.0;

		try {

			final Dao<CashDocumentPosition, Long> dao = DatabaseHolder.getInstance().getDao(CashDocumentPosition.class);
			final QueryBuilder<CashDocumentPosition, Long> qb = dao.queryBuilder();
			qb.selectRaw("SUM(" + (getDocumentType() == CashDocumentType.KP ? CashDocumentPosition.INCOME_FIELD_NAME : CashDocumentPosition.EXPENDITURE_FIELD_NAME) + ")");
			qb.where().eq(CashDocumentPosition.KSDK_GUID_FIELD_NAME, getGuid()).and().notIn(DownloadTransferObjectBase.RECORD_STATE__FIELD_NAME, RecordStates.Deleted.intValue());
			final String[] result = dao.queryRaw(qb.prepareStatementString()).getFirstResult();

			if (result != null) {
				if (result[0] != null) {
					amount = Double.parseDouble(result[0]);
				}
			}

		} catch (final SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		}

		return amount;
	}

	public Contractor getContractor() {
		return contractor;
	}

	public String getDescription() {
		return description;
	}

	public String getDocumentDate() {
		return documentDate;
	}

	public long getDocumentNumber() {
		return documentNumber;
	}

	public String getDocumentSymbol() {
		return documentSymbol;
	}

	public CashDocumentType getDocumentType() {
		return documentType;
	}

	public String getKsrkGuid() {
		return ksrkGuid;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public void setContractor(Contractor contractor) {
		this.contractor = contractor;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}

	public void setDocumentNumber(long documentNumber) {
		this.documentNumber = documentNumber;
	}

	public void setDocumentSymbol(String documentSymbol) {
		this.documentSymbol = documentSymbol;
	}

	public void setDocumentType(CashDocumentType documentType) {
		this.documentType = documentType;
	}

	public void setKsrkGuid(String ksrkGuid) {
		this.ksrkGuid = ksrkGuid;
	}

	private String generateSymbol() {
		final Calendar calendar = new GregorianCalendar();
		calendar.setTime(DateTimeHelper.getDate(getDocumentDate()));

		return getDocumentType() + "/" + getDocumentNumber() + "/" + calendar.get(Calendar.YEAR);
	}

	public static enum CashDocumentType {
		KP, KW, DK, DEL, RZL, ZAL
	}
}
